package main.com.epam.tc.finaltask.bean;

import java.io.Serializable;

/**
 * Created by blooming on 24.2.16.
 */
public class Bill implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int cost;
    private User customer;
    private String description;

    public Bill() {
    }

    public Bill(int id, int cost, User customer, String description) {
        this.id = id;
        this.cost = cost;
        this.customer = customer;
        this.description = description;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (null == object) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Bill bill = (Bill) object;
        if (id != bill.id) {
            return false;
        }
        if (cost != bill.cost) {
            return false;
        }
        if (!customer.equals(bill.customer)) {
            return false;
        }
        if (!description.equals(bill.description)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + cost;
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", cost=" + cost +
                ", customer=" + customer +
                ", description='" + description + '\'' +
                '}';
    }
}
