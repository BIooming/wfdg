package main.com.epam.tc.finaltask.bean;

import java.io.Serializable;

/**
 * Created by blooming on 24.2.16.
 */
public class Project implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String description;
    private String customerName;
    private Bill bill;
    private ProjectStatus status;

    public Project() {
    }

    public Project(int id, String name, String description, String customerName, Bill bill, ProjectStatus status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.customerName = customerName;
        this.bill = bill;
        this.status = status;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (null == object) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Project project = (Project) object;
        if (id != project.id) {
            return false;
        }
        if (!name.equals(project.name)) {
            return false;
        }
        if (!description.equals(project.description)) {
            return false;
        }
        if (!customerName.equals(project.customerName)) {
            return false;
        }
        if (!bill.equals(project.bill)) {
            return false;
        }
        if (!status.equals(project.status)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + customerName.hashCode();
        result = 31 * result + bill.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", customerName='" + customerName + '\'' +
                ", bill=" + bill +
                ", status=" + status +
                '}';
    }
}
