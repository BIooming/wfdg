package main.com.epam.tc.finaltask.bean;

/**
 * Created by blooming on 24.2.16.
 */
public enum ProjectStatus {
    UNDER_CONSIDERATION, IN_PROGRESS, CLOSED
}
