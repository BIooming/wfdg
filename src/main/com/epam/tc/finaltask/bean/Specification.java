package main.com.epam.tc.finaltask.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public class Specification implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String description;
    private User customer;
    private Project project;
    private ArrayList<Work> workList;

    public Specification() {
    }

    public Specification(int id, String name, String description, User customer, Project project, ArrayList<Work> workList) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.customer = customer;
        this.project = project;
        this.workList = workList;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public ArrayList<Work> getWorkList() {
        return workList;
    }

    public void setWorkList(ArrayList<Work> workList) {
        this.workList = workList;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (null == object) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Specification specification = (Specification) object;
        if (id != specification.id) {
            return false;
        }
        if (!name.equals(specification.name)) {
            return false;
        }
        if (!description.equals(specification.description)) {
            return false;
        }
        if (!customer.equals(specification.customer)) {
            return false;
        }
        if (!workList.equals(specification.workList)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + customer.hashCode();
        result = 31 * result + (project != null ? project.hashCode() : 0);
        result = 31 * result + workList.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Specification{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", customer=" + customer +
                ", project=" + project +
                ", workList=" + workList +
                '}';
    }
}
