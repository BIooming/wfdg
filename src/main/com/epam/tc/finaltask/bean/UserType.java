package main.com.epam.tc.finaltask.bean;

import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.command.CommandName;

/**
 * Created by blooming on 28.2.16.
 */
public enum UserType {
    MANAGER, CUSTOMER, DEVELOPER;

    public String getJspPage(CommandName commandName) {

        switch (this) {
            case MANAGER:
                return getManagerPage(commandName);
            case CUSTOMER:
                return getCustomerPage(commandName);
            case DEVELOPER:
                return getDeveloperPage(commandName);
            default:
                return JspPageName.SERVER_ERROR;
        }
    }

    private String getManagerPage(CommandName command) {
        switch (command) {
            case GOTO_MAIN:
                return JspPageName.MANAGER_MAIN;
            case GOTO_ACCOUNT:
                return JspPageName.MANAGER_ACCOUNT;
            case LOGIN:
                return JspPageName.MANAGER_MAIN;
            case SET_LOCALE:
                return JspPageName.MANAGER_SETTINGS;
            case GOTO_SETTINGS:
                return JspPageName.MANAGER_SETTINGS;
            default:
                return JspPageName.SERVER_ERROR;
        }
    }

    private String getCustomerPage(CommandName command) {
        switch (command) {
            case GOTO_MAIN:
                return JspPageName.CUSTOMER_MAIN;
            case GOTO_ACCOUNT:
                return JspPageName.CUSTOMER_ACCOUNT;
            case GOTO_SETTINGS:
                return JspPageName.CUSTOMER_SETTINGS;
            case LOGIN:
                return JspPageName.CUSTOMER_MAIN;
            case SET_LOCALE:
                return JspPageName.CUSTOMER_SETTINGS;
            default:
                return JspPageName.SERVER_ERROR;
        }
    }

    private String getDeveloperPage(CommandName command) {
        switch (command) {
            case GOTO_MAIN:
                return JspPageName.DEVELOPER_MAIN;
            case LOGIN:
                return JspPageName.DEVELOPER_MAIN;
            case SET_LOCALE:
                return JspPageName.DEVELOPER_MAIN;
            default:
                return JspPageName.SERVER_ERROR;
        }
    }
}
