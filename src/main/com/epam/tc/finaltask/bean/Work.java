package main.com.epam.tc.finaltask.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by blooming on 24.2.16.
 */
public class Work implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int specId;
    private int developersNumber;
    private Qualification qualification;
    private String name;
    private String description;
    private ArrayList<User> developers;
    private Map<Integer, Float> spentTime;

    public Work() {
    }

    public Work(String name, String description, int developersNumber) {
        this.name = name;
        this.description = description;
        this.developersNumber = developersNumber;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDevelopersNumber() {
        return developersNumber;
    }

    public void setDevelopersNumber(int developersNumber) {
        this.developersNumber = developersNumber;
    }

    public ArrayList<Integer> getDevelopersId() {
        ArrayList<Integer> devIds = new ArrayList<>();
        for (User user : developers) {
            devIds.add(user.getId());
        }
        return devIds;
    }

    public ArrayList<User> getDevelopers() {
        return developers;
    }

    public void setDevelopers(ArrayList<User> developers) {
        this.developers = developers;
    }

    public Map<Integer, Float> getSpentTime() {
        return spentTime;
    }


    public void setSpentTime(Map<Integer, Float> spentTime) {
        this.spentTime = spentTime;
    }

    public int getSpecId() {
        return specId;
    }

    public void setSpecId(int specId) {
        this.specId = specId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (null == object) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Work work = (Work) object;
        if (id != work.id) {
            return false;
        }
        if (specId != work.specId) {
            return false;
        }
        if (developersNumber != work.developersNumber) {
            return false;
        }
        if (!qualification.equals(work.qualification)) {
            return false;
        }
        if (!name.equals(work.name)) {
            return false;
        }
        if (!description.equals(work.description)) {
            return false;
        }
        if (!developers.equals(work.developers)) {
            return false;
        }
        if (!spentTime.equals(work.spentTime)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + specId;
        result = 31 * result + qualification.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + developersNumber;
        result = 31 * result + (developers != null ? developers.hashCode() : 0);
        result = 31 * result + (spentTime != null ? spentTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Work{" +
                "id=" + id +
                ", specId=" + specId +
                ", qualification=" + qualification +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", developersNumber=" + developersNumber +
                ", developers=" + developers +
                ", spentTime=" + spentTime +
                '}';
    }
}
