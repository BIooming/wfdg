package main.com.epam.tc.finaltask.controller;

import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.CommandHelper;
import main.com.epam.tc.finaltask.controller.command.Executable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by blooming on 24.2.16.
 */
public class Controller extends HttpServlet {
    private static final Logger logger = LogManager.getLogger("root");

    public Controller() {
        super();
        logger.debug("Server started");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = JspPageName.INDEX;

        try {
            String commandName = request.getParameter(ParameterName.COMMAND);
            Executable command = CommandHelper.getInstance().getCommand(commandName);

            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(ParameterName.LOCALE)) {
                        request.getSession().setAttribute(ParameterName.LOCALE, cookie.getValue());
                    }
                }
            }

            page = command.execute(request, response);
        } catch (CommandException e) {
            logger.error(e);
            page = JspPageName.SERVER_ERROR;
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        if (null == dispatcher) {
            response.sendRedirect(JspPageName.SERVER_ERROR);
        } else {
            dispatcher.forward(request, response);
        }
    }
}