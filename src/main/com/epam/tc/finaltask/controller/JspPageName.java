package main.com.epam.tc.finaltask.controller;

/**
 * Created by blooming on 24.2.16.
 */
public final class JspPageName {

    //Common pages
    public static final String SERVER_ERROR = "servererror.jsp";
    public static final String REGISTER_ERROR = "registererror.jsp";
    public static final String LOGIN_ERROR = "loginerror.jsp";
    public static final String INDEX = "index.jsp";

    //Customer pages
    public static final String CUSTOMER_MAIN = "WEB-INF/jsp/customer/main.jsp";
    public static final String VIEW_SPEC_LIST = "WEB-INF/jsp/customer/speclist.jsp";
    public static final String NEW_SPEC = "WEB-INF/jsp/customer/newspec.jsp";
    public static final String CUSTOMER_ACCOUNT = "WEB-INF/jsp/customer/account.jsp";
    public static final String EDIT_SPEC = "WEB-INF/jsp/customer/editspec.jsp";
    public static final String CONTACTS = "WEB-INF/jsp/customer/contacts.jsp";
    public static final String CUSTOMER_SETTINGS = "WEB-INF/jsp/customer/settings.jsp";

    //Manager pages
    public static final String MANAGER_MAIN = "WEB-INF/jsp/manager/main.jsp";
    public static final String MANAGER_ACCOUNT = "WEB-INF/jsp/manager/account.jsp";
    public static final String MANAGER_SETTINGS = "WEB-INF/jsp/manager/settings.jsp";
    public static final String PROJECTS = "WEB-INF/jsp/manager/projects.jsp";
    public static final String QUALIFICATIONS = "WEB-INF/jsp/manager/qualifications.jsp";
    public static final String USERS = "WEB-INF/jsp/manager/users.jsp";
    public static final String CREATE_PROJECT = "WEB-INF/jsp/manager/createproject.jsp";
    public static final String CREATE_QUALIFICATION = "WEB-INF/jsp/manager/createqualification.jsp";
    public static final String CREATE_USER = "WEB-INF/jsp/manager/createuser.jsp";
    public static final String EDIT_PROJECT = "WEB-INF/jsp/manager/editproject.jsp";
    public static final String EDIT_QUALIFICATION = "WEB-INF/jsp/manager/editqualification.jsp";
    public static final String EDIT_USER = "WEB-INF/jsp/manager/edituser.jsp";

    //Developer pages
    public static final String DEVELOPER_MAIN = "WEB-INF/jsp/developer/main.jsp";

    private JspPageName() {
    }
}
