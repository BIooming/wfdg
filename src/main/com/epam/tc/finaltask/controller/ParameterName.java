package main.com.epam.tc.finaltask.controller;

/**
 * Created by blooming on 25.2.16.
 */
public final class ParameterName {

    //Common parameters
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String MESSAGE = "message";
    public static final String LOCALE = "locale";
    public static final String COMMAND = "command";

    //User parameters
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USER = "user";
    public static final String USER_NAME = "username";
    //Project parameters
    public static final String PROJECT_LIST = "projects";
    public static final String PROJECT = "project";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String PROJECT_NAME = "projectName";

    //Qualification parameters
    public static final String QUALIFICATION_LIST = "qualifications";
    public static final String QUALIFICATION = "qualification";
    public static final String QUALIFICATION_MAP = "qualMap";

    //Work parameters
    public static final String WORK_LIST = "works";
    public static final String WORK_NAME = "workName";
    public static final String WORK_DESCRIPTION = "workDescription";
    public static final String CURRENT_WORK = "currentWork";
    public static final String WORK_TO_DELETE = "workToDelete";
    public static final String HOURS = "hours";
    public static final String MINUTES = "minutes";
    public static final String IS_DONE = "isDone";

    //Developer parameters
    public static final String DEVELOPER_LIST = "devs";
    public static final String COUNT_OF_DEVELOPERS = "countOfDevelopers";

    //Specification parameters
    public static final String SPEC_LIST = "specifications";
    public static final String SPECIFICATION = "spec";
    public static final String SPECIFICATION_NAME = "spec_name";
    public static final String SPECIFICATION_DESCRIPTION = "spec_description";

    private ParameterName() {
    }
}
