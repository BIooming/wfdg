package main.com.epam.tc.finaltask.controller.command;

import java.io.Serializable;

/**
 * Created by blooming on 24.2.16.
 */
public class CommandException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;

    public CommandException() {
        super();
    }

    public CommandException(Exception e) {
        super(e);
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Exception e) {
        super(message, e);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
