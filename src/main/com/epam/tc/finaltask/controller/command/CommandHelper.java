package main.com.epam.tc.finaltask.controller.command;

import main.com.epam.tc.finaltask.controller.command.impl.common.*;
import main.com.epam.tc.finaltask.controller.command.impl.customer.*;
import main.com.epam.tc.finaltask.controller.command.impl.developer.SubmitTimeCommand;
import main.com.epam.tc.finaltask.controller.command.impl.manager.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by blooming on 24.2.16.
 */
public final class CommandHelper {
    private static final String NO_SUCH_COMMAND_ERR = "No such command";
    private static final CommandHelper instance = new CommandHelper();

    private Map<CommandName, Executable> commands = new HashMap<>();

    private CommandHelper() {
        //common commands
        commands.put(CommandName.LOGIN, new LoginCommand());
        commands.put(CommandName.LOG_OUT, new LogOutCommand());
        commands.put(CommandName.SET_LOCALE, new SetLocaleCommand());
        commands.put(CommandName.CREATE_USER, new CreateUserCommand());
        commands.put(CommandName.GOTO_ACCOUNT, new GoToAccountCommand());
        commands.put(CommandName.GOTO_MAIN, new GoToMainCommand());
        commands.put(CommandName.GOTO_SETTINGS, new GoToSettingsCommand());

        //customer commands
        commands.put(CommandName.CREATE_SPEC, new CreateSpecCommand());
        commands.put(CommandName.REFUSE_SPEC, new RefuseSpecCommand());
        commands.put(CommandName.EDIT_SPEC, new EditSpecCommand());
        commands.put(CommandName.GOTO_NEWSPEC, new GoToNewSpecFormCommand());
        commands.put(CommandName.GOTO_SPECLIST, new GoToSpecsCommand());
        commands.put(CommandName.GOTO_EDITSPEC, new GoToEditSpecCommand());
        commands.put(CommandName.GOTO_CONTACTS, new GoToContactsCommand());

        //manager commands
        commands.put(CommandName.CREATE_QUALIFICATION, new CreateQualificationCommand());
        commands.put(CommandName.CREATE_PROJECT, new CreateProjectCommand());
        commands.put(CommandName.EDIT_USER, new EditUserCommand());
        commands.put(CommandName.DELETE_USER, new DeleteUserCommand());
        commands.put(CommandName.EDIT_QUALIFICATION, new EditQualificationCommand());
        commands.put(CommandName.DELETE_QUALIFICATION, new DeleteQualificationCommand());
        commands.put(CommandName.EDIT_PROJECT, new EditProjectCommand());
        commands.put(CommandName.DELETE_PROJECT, new DeleteProjectCommand());
        commands.put(CommandName.GOTO_QUALIFICATIONS, new GoToQualificationsCommand());
        commands.put(CommandName.GOTO_EDIT_QUALIFICATION, new GoToEditQualificationCommand());
        commands.put(CommandName.GOTO_CREATE_QUALIFICATION, new GoToCreateQualificationCommand());
        commands.put(CommandName.GOTO_CREATE_PROJECT, new GoToCreateProject());
        commands.put(CommandName.GOTO_EDIT_PROJECT, new GoToEditProjectCommand());
        commands.put(CommandName.GOTO_PROJECTS, new GoToProjectsCommand());
        commands.put(CommandName.GOTO_USERS, new GoToUsersCommand());
        commands.put(CommandName.GOTO_CREATE_USER, new GoToCreateUserCommand());
        commands.put(CommandName.GOTO_EDIT_USER, new GoToEditUserCommand());

        //developer commands
        commands.put(CommandName.SUBMIT_TIME, new SubmitTimeCommand());
    }

    public static CommandHelper getInstance() {
        return instance;
    }

    public Executable getCommand(String commandName) throws CommandException {
        try {
            Executable command;
            CommandName name = CommandName.valueOf(commandName);
            if (null != name) {
                command = commands.get(name);
            } else {
                throw new CommandException(NO_SUCH_COMMAND_ERR);
            }
            return command;
        } catch (NullPointerException e) {
            throw new CommandException(e);
        }
    }
}
