package main.com.epam.tc.finaltask.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 24.2.16.
 */
public interface Executable {

    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
