package main.com.epam.tc.finaltask.controller.command.impl.common;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 19.3.16.
 */
public class CreateUserCommand implements Executable {
    private static final String UPDATE_DEV_LIST_ERROR = "Error updating list of developers";
    private static final String GETTING_QUALIFICATION_ERROR = "Error while getting qualification from DB";
    private static final int NULL = 0;
    private static final int MSG_EMAIL_TAKEN = -1;
    private static final int MSG_INPUT_INVALID = -2;
    private static final int MSG_SUCCESS = 1;

    private UserProcessingService userService = UserProcessingService.getInstance();
    private QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();
    private UserValidationService userValidationService = UserValidationService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.LOGIN_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (userValidationService.isValidUser(user, UserType.MANAGER)) {
            jspPage = createDeveloperAccount(request);
        } else {
            jspPage = createCustomerAccount(request);
        }

        return jspPage;
    }

    private String createCustomerAccount(HttpServletRequest request) throws CommandException {
        String jspPage;
        if (isValidInput(request)) {
            User newUser = getUserFromRequest(request);

            if (create(newUser, UserType.CUSTOMER)) {
                request.getSession().setAttribute(ParameterName.USER, newUser);
                jspPage = JspPageName.INDEX;
            } else {
                request.setAttribute(ParameterName.USER, newUser);
                request.setAttribute(ParameterName.MESSAGE, MSG_EMAIL_TAKEN);
                jspPage = JspPageName.REGISTER_ERROR;
            }
        } else {
            request.setAttribute(ParameterName.MESSAGE, MSG_INPUT_INVALID);
            jspPage = JspPageName.REGISTER_ERROR;
        }
        return jspPage;
    }

    private String createDeveloperAccount(HttpServletRequest request) throws CommandException {
        String jspPage;
        if (isValidInput(request)) {
            User newUser = getUserFromRequest(request);

            if (create(newUser, UserType.DEVELOPER)) {
                ArrayList<User> users = new ArrayList<>();
                try {
                    users = userService.getAllDevs();
                } catch (ServiceException e) {
                    throw new CommandException(UPDATE_DEV_LIST_ERROR, e);
                }
                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
                request.getSession().setAttribute(ParameterName.DEVELOPER_LIST, users);
                jspPage = JspPageName.USERS;
            } else {
                request.setAttribute(ParameterName.MESSAGE, MSG_EMAIL_TAKEN);
                request.setAttribute(ParameterName.USER, newUser);
                jspPage = JspPageName.CREATE_USER;
            }
        } else {
            request.setAttribute(ParameterName.MESSAGE, MSG_INPUT_INVALID);
            jspPage = JspPageName.CREATE_USER;
        }
        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.USER_NAME);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_USERNAME_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.EMAIL);
        if (!valService.isValidEmail(input)) {
            return false;
        }
        input = request.getParameter(ParameterName.PASSWORD);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_PASSWORD_LENGTH)) {
            return false;
        }

        return true;
    }

    private User getUserFromRequest(HttpServletRequest request) throws CommandException {
        User newUser = new User();
        try {
            newUser.setName(request.getParameter(ParameterName.USER_NAME));
            newUser.setEmail(request.getParameter(ParameterName.EMAIL));
            newUser.setPassword(request.getParameter(ParameterName.PASSWORD));

            String qualificationParam = request.getParameter(ParameterName.QUALIFICATION);
            int qualificationId = NULL;
            if (null != qualificationParam) {
                qualificationId = Integer.valueOf(qualificationParam);
            }
            if (NULL != qualificationId) {
                Qualification qualification = qualificationService.getById(qualificationId);
                newUser.setQualification(qualification);
            }
        } catch (ServiceException e) {
            throw new CommandException(GETTING_QUALIFICATION_ERROR, e);
        } catch (NumberFormatException e) {
            throw new CommandException(e);
        }
        return newUser;
    }

    private boolean create(User user, UserType type) throws CommandException {
        boolean created = false;
        try {
            user.setUserType(type);
            if ((NULL == userService.getUserByEmail(user.getEmail()).getId())
                    && (NULL == userService.getUserByName(user.getName()).getId())) {
                userService.add(user);
                created = true;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return created;
    }
}
