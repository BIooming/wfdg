package main.com.epam.tc.finaltask.controller.command.impl.common;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.CommandName;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.WorkProcessingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 25.2.16.
 */
public class GoToMainCommand implements Executable {
    private WorkProcessingService workService = WorkProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;
        User user = null;

        Object attribute = request.getSession().getAttribute(ParameterName.USER);
        if (attribute instanceof User) {
            user = (User) attribute;
        }

        try {
            if (null == user) {
                jspPage = JspPageName.INDEX;
            } else {
                if (user.getUserType().equals(UserType.DEVELOPER)) {
                    request.getSession().setAttribute(ParameterName.WORK_LIST,
                            workService.getWorksByUserID(user.getId()));
                }
                jspPage = user.getUserType().getJspPage(CommandName.GOTO_MAIN);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}