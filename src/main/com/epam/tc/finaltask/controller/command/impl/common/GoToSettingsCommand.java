package main.com.epam.tc.finaltask.controller.command.impl.common;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.CommandName;
import main.com.epam.tc.finaltask.controller.command.Executable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 17.3.16.
 */
public class GoToSettingsCommand implements Executable {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = null;
        User user = null;

        Object attribute = request.getSession().getAttribute(ParameterName.USER);
        if (attribute instanceof User) {
            user = (User) attribute;
        }

        if (null == user) {
            jspPage = JspPageName.INDEX;
        } else {
            jspPage = user.getUserType().getJspPage(CommandName.GOTO_SETTINGS);
        }

        return jspPage;
    }
}
