package main.com.epam.tc.finaltask.controller.command.impl.common;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.CommandName;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.InputValidationService;
import main.com.epam.tc.finaltask.service.LoginService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.WorkProcessingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 25.2.16.
 */
public class LoginCommand implements Executable {
    private static final int MSG_INPUT_INVALID = -1;

    private WorkProcessingService workService = WorkProcessingService.getInstance();
    private LoginService loginService = LoginService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        if (isValidInput(request)) {
            jspPage = login(request);
        } else {
            request.setAttribute(ParameterName.MESSAGE, MSG_INPUT_INVALID);
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.EMAIL);
        if (!valService.isValidEmail(input)) {
            return false;
        }
        input = request.getParameter(ParameterName.PASSWORD);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_PASSWORD_LENGTH)) {
            return false;
        }

        return true;
    }

    private String login(HttpServletRequest request) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;
        try {
            String email = request.getParameter(ParameterName.EMAIL);
            String password = request.getParameter(ParameterName.PASSWORD);
            User user = loginService.login(email, password);
            if (null == user) {
                jspPage = JspPageName.LOGIN_ERROR;
            } else {
                if (user.getUserType().equals(UserType.DEVELOPER)) {
                    request.getSession().setAttribute(ParameterName.CURRENT_WORK,
                            workService.getCurrentWork(user));
                    request.getSession().setAttribute(ParameterName.WORK_LIST,
                            workService.getWorksByUserID(user.getId()));
                }
                request.getSession().setAttribute(ParameterName.USER, user);
                jspPage = user.getUserType().getJspPage(CommandName.LOGIN);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return jspPage;
    }
}
