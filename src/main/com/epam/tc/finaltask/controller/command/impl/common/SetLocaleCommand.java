package main.com.epam.tc.finaltask.controller.command.impl.common;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.CommandName;
import main.com.epam.tc.finaltask.controller.command.Executable;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 25.2.16.
 */
public class SetLocaleCommand implements Executable {
    private static final String INVALID_INPUT_PARAMETERS = "Invalid input parameters";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;
        User user = null;
        Cookie localeCookie = null;

        String locale = request.getParameter(ParameterName.LOCALE);
        if ((null != locale) && (!locale.isEmpty())) {
            request.getSession().setAttribute(ParameterName.LOCALE, locale);
            localeCookie = new Cookie(ParameterName.LOCALE, locale);
            response.addCookie(localeCookie);
        } else {
            throw new CommandException(INVALID_INPUT_PARAMETERS);
        }

        Object attribute = request.getSession().getAttribute(ParameterName.USER);
        if (attribute instanceof User) {
            user = (User) attribute;
        }

        if (null == user) {
            jspPage = JspPageName.INDEX;
        } else {
            jspPage = user.getUserType().getJspPage(CommandName.SET_LOCALE);
        }
        return jspPage;
    }
}
