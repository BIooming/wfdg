package main.com.epam.tc.finaltask.controller.command.impl.customer;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by blooming on 25.2.16.
 */
public class EditSpecCommand implements Executable {
    private static final String WORK_NAME_PATTERN = "(workName)(\\d+)";
    private static final int MSG_SUCCESS = 1;
    private static final int MSG_INVALID_INPUT = -1;

    private SpecProcessingService specService = SpecProcessingService.getInstance();
    private WorkProcessingService workService = WorkProcessingService.getInstance();
    private UserValidationService userValidationService = UserValidationService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (userValidationService.isValidUser(user, UserType.CUSTOMER)) {
            jspPage = updateSpec(request, user);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private String updateSpec(HttpServletRequest request, User user) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;
        if (isValidInput(request)) {
            try {
                Specification specification = (Specification)
                        request.getSession().getAttribute(ParameterName.SPECIFICATION);
                specification.setName(request.getParameter(ParameterName.SPECIFICATION_NAME));
                specification.setDescription(request.getParameter(ParameterName.SPECIFICATION_DESCRIPTION));
                specService.updateInDataBase(specification);

                ArrayList<Work> works = updateWorkList(request);
                request.setAttribute(ParameterName.SPEC_LIST, specService.getSpecList(user));
                for (Work work : works) {
                    if (work.getSpecId() == 0) {
                        work.setSpecId(specification.getId());
                    }
                }
                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
                request.getSession().setAttribute(ParameterName.SPECIFICATION, null);
                jspPage = JspPageName.VIEW_SPEC_LIST;
                workService.updateWorks(works);
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        } else {
            request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
            jspPage = JspPageName.NEW_SPEC;
        }
        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.SPECIFICATION_NAME);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_NAME_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.SPECIFICATION_DESCRIPTION);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_DESCRIPTION_LENGTH)) {
            return false;
        }

        return true;
    }

    private ArrayList<Work> updateWorkList(HttpServletRequest request) throws CommandException {
        ArrayList<Work> works = (ArrayList<Work>)
                request.getSession().getAttribute(ParameterName.WORK_LIST);
        try {
            ArrayList<Work> newWorks = getWorksFromRequest(request);
            ArrayList<Integer> worksToDelete = getWorksToDelete(request);
            works.addAll(newWorks);
            for (Integer id : worksToDelete) {
                works = workService.removeWork(works, id);
            }
            works = newWorks;
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }
        return works;
    }

    private ArrayList<Integer> getWorksToDelete(HttpServletRequest request) throws NumberFormatException {
        ArrayList<Integer> works = new ArrayList<>();
        String[] workToDeleteIds = request.getParameterValues(ParameterName.WORK_TO_DELETE);
        if (workToDeleteIds != null) {
            for (String id : workToDeleteIds) {
                works.add(Integer.parseInt(id));
            }
        }
        return works;
    }

    private ArrayList<Work> getWorksFromRequest(HttpServletRequest request) throws NumberFormatException {
        ArrayList<Work> works = new ArrayList<>();
        Pattern workName = Pattern.compile(WORK_NAME_PATTERN);
        Enumeration<String> paramNames = request.getParameterNames();

        while (paramNames.hasMoreElements()) {
            String param = paramNames.nextElement();
            Matcher matcher = workName.matcher(param);
            if (matcher.find()) {
                int parameterNumber = Integer.parseInt(matcher.group(2));

                Work work = new Work();
                work.setDescription(request.getParameter(ParameterName.WORK_DESCRIPTION + parameterNumber));
                work.setName(request.getParameter(ParameterName.WORK_NAME + parameterNumber));
                work.setDevelopersNumber(Integer.parseInt(
                        request.getParameter(ParameterName.COUNT_OF_DEVELOPERS + parameterNumber)));

                ArrayList<Qualification> qualifications = (ArrayList<Qualification>)
                        request.getSession().getAttribute(ParameterName.QUALIFICATION_LIST);

                for (Qualification qualification : qualifications) {
                    String qualificationParameter = request.getParameter(ParameterName.QUALIFICATION + parameterNumber);
                    if (qualification.getName().equals(qualificationParameter)) {
                        work.setQualification(qualification);
                    }
                }
                works.add(work);
            }
        }
        return works;
    }
}
