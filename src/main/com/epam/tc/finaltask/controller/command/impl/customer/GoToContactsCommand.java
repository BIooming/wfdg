package main.com.epam.tc.finaltask.controller.command.impl.customer;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 12.3.16.
 */
public class GoToContactsCommand implements Executable {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.CUSTOMER)) {
            jspPage = JspPageName.CONTACTS;
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }
        return jspPage;
    }
}
