package main.com.epam.tc.finaltask.controller.command.impl.customer;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 3.3.16.
 */
public class GoToEditSpecCommand implements Executable {
    private SpecProcessingService specProcessingService = SpecProcessingService.getInstance();
    private QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;
        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        Specification specification = null;

        try {
            if (UserValidationService.getInstance().isValidUser(user, UserType.CUSTOMER)) {

                ArrayList<Qualification> qualifications = qualificationService.getAll();
                request.getSession().setAttribute(ParameterName.QUALIFICATION_LIST, qualifications);

                Integer specificationId = Integer.parseInt(request.getParameter(ParameterName.ID));
                specification = specProcessingService.getSpecById(specificationId);
                request.getSession().setAttribute(ParameterName.SPECIFICATION, specification);

                ArrayList<Work> workList = WorkProcessingService.getInstance().getWorksBySpecId(specification.getId());
                request.getSession().setAttribute(ParameterName.WORK_LIST, workList);

                jspPage = JspPageName.EDIT_SPEC;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}
