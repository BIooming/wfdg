package main.com.epam.tc.finaltask.controller.command.impl.customer;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.QualificationProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 25.2.16.
 */
public class GoToNewSpecFormCommand implements Executable {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();
        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.CUSTOMER)) {
                ArrayList<Qualification> qualifications = qualificationService.getAll();
                request.getSession().setAttribute(ParameterName.QUALIFICATION_LIST, qualifications);

                Specification specification = new Specification();
                ArrayList<Work> works = new ArrayList<>();
                request.getSession().setAttribute(ParameterName.WORK_LIST, works);
                request.getSession().setAttribute(ParameterName.SPECIFICATION, specification);

                jspPage = JspPageName.NEW_SPEC;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return jspPage;
    }
}
