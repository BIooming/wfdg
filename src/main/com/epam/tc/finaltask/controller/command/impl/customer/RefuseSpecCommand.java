package main.com.epam.tc.finaltask.controller.command.impl.customer;

import main.com.epam.tc.finaltask.bean.Specification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.SpecProcessingService;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 25.2.16.
 */
public class RefuseSpecCommand implements Executable {
    private SpecProcessingService specService = SpecProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.CUSTOMER)) {
                int id = Integer.parseInt(request.getParameter(ParameterName.ID));
                Specification specification = specService.getSpecById(id);
                specService.deleteFromDataBase(specification);

                User customer = (User) request.getSession().getAttribute(ParameterName.USER);
                request.setAttribute(ParameterName.SPEC_LIST, specService.getSpecList(customer));
                jspPage = JspPageName.VIEW_SPEC_LIST;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }
        return jspPage;
    }
}
