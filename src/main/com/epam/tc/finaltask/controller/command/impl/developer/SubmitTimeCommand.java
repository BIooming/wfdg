package main.com.epam.tc.finaltask.controller.command.impl.developer;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.CommandName;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserValidationService;
import main.com.epam.tc.finaltask.service.WorkProcessingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 25.2.16.
 */
public class SubmitTimeCommand implements Executable {
    private static final int MSG_SUCCESS = 1;
    private static final int MSG_INVALID_INPUT = -1;

    private WorkProcessingService workService = WorkProcessingService.getInstance();
    private UserValidationService userValidationService = UserValidationService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (userValidationService.isValidUser(user, UserType.DEVELOPER)) {
            jspPage = submitTime(request, user);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private String submitTime(HttpServletRequest request, User user) throws CommandException {
        String jspPage = UserType.DEVELOPER.getJspPage(CommandName.GOTO_MAIN);

        if (isValidInput(request)) {
            try {
                int idWork = Integer.parseInt(request.getParameter(ParameterName.ID));
                int idUser = user.getId();
                float hours = Float.parseFloat(request.getParameter(ParameterName.HOURS));
                float minutes = Float.parseFloat(request.getParameter(ParameterName.MINUTES));

                float totalSpentTime = hours + minutes / 60;
                workService.submitTime(idWork, idUser, totalSpentTime);

                boolean isDone = Boolean.parseBoolean(request.getParameter(ParameterName.IS_DONE));
                if (isDone) {
                    workService.setFinished(idWork, idUser, true);
                }

                request.getSession().setAttribute(ParameterName.CURRENT_WORK, workService.getCurrentWork(user));
                request.getSession().setAttribute(ParameterName.WORK_LIST, workService.getWorksByUserID(user.getId()));
                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
            } catch (ServiceException e) {
                throw new CommandException(e);
            } catch (NumberFormatException e) {
                request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
            }
        } else {
            request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
        }

        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        try {
            Float input = Float.parseFloat(request.getParameter(ParameterName.HOURS));
            if ((input < 0) || (input > 100)) {
                return false;
            }
            input = Float.parseFloat(request.getParameter(ParameterName.MINUTES));
            if ((input < 0) || (input > 60)) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
