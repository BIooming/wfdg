package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by blooming on 25.2.16.
 */
public class CreateProjectCommand implements Executable {
    private static final WorkProcessingService workService = WorkProcessingService.getInstance();
    private static final UserProcessingService userService = UserProcessingService.getInstance();
    private static final ProjectProcessingService projectService = ProjectProcessingService.getInstance();
    private static final BillProcessingService billService = BillProcessingService.getInstance();

    private static final String WORK_PARAM_PATTERN = "work\\d+:";
    private static final int MSG_SUCCESS = 1;
    private static final int MSG_INVALID_INPUT = -1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
            jspPage = createProject(request);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private String createProject(HttpServletRequest request) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            if (isValidInput(request)) {
                Specification spec = (Specification)
                        request.getSession().getAttribute(ParameterName.SPECIFICATION);
                ArrayList<Work> works = (ArrayList<Work>)
                        request.getSession().getAttribute(ParameterName.WORK_LIST);
                Bill bill = billService.calculate(spec);
                bill = billService.add(bill);
                Project project = new Project();
                project.setBill(bill);
                project.setName(request.getParameter(ParameterName.PROJECT_NAME));
                project.setDescription(request.getParameter(ParameterName.PROJECT_DESCRIPTION));
                project.setStatus(ProjectStatus.IN_PROGRESS);
                project.setId(spec.getId());

                for (Work work : works) {
                    work = getWorkDataFromRequest(work, request);
                }
                workService.updateWorks(works);
                projectService.saveProjectToDatabase(project);

                updateSessionAttributes(request);
                clearProjectAttributes(request);
                jspPage = JspPageName.PROJECTS;
            } else {
                request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
                jspPage = JspPageName.CREATE_PROJECT;
            }
        } catch (ServiceException | ClassCastException | NumberFormatException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.PROJECT_NAME);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_NAME_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.PROJECT_DESCRIPTION);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_DESCRIPTION_LENGTH)) {
            return false;
        }

        return true;
    }

    private void updateSessionAttributes(HttpServletRequest request) throws ServiceException {
        ArrayList<Specification> specifications = SpecProcessingService.getInstance().getFullSpecList();
        ArrayList<Project> projects = new ArrayList<>();
        ArrayList<Specification> unprocessedSpec = new ArrayList<>();

        for (Specification spec : specifications) {
            if (spec.getProject() != null) {
                Project project = projectService.getProjectById(spec.getId());
                project.setCustomerName(spec.getCustomer().getName());
                projects.add(project);
            } else {
                unprocessedSpec.add(spec);
            }
        }

        request.getSession().setAttribute(ParameterName.PROJECT_LIST, projects);
        request.getSession().setAttribute(ParameterName.SPEC_LIST, unprocessedSpec);
    }

    private Work getWorkDataFromRequest(Work work, HttpServletRequest request) throws ServiceException {
        Enumeration<String> paramNames = request.getParameterNames();

        while (paramNames.hasMoreElements()) {
            String param = paramNames.nextElement();

            Pattern workName = Pattern.compile(WORK_PARAM_PATTERN + work.getId());
            Matcher matcher = workName.matcher(param);
            if (matcher.find()) {
                String userName = request.getParameter(param);
                User user = userService.getUserByName(userName);
                if (work.getDevelopers() == null) {
                    work.setDevelopers(new ArrayList<User>());
                }
                work.getDevelopers().add(user);
            }
        }

        return work;
    }

    private void clearProjectAttributes(HttpServletRequest request) {
        request.getSession().removeAttribute(ParameterName.SPECIFICATION);
        request.getSession().removeAttribute(ParameterName.WORK_LIST);
        request.getSession().removeAttribute(ParameterName.DEVELOPER_LIST);
        request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
    }
}