package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.InputValidationService;
import main.com.epam.tc.finaltask.service.QualificationProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 25.2.16.
 */
public class CreateQualificationCommand implements Executable {
    private static final QualificationProcessingService qualificationService =
            QualificationProcessingService.getInstance();

    private static final int MSG_SUCCESS = 1;
    private static final int MSG_INVALID_INPUT = -1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
            jspPage = createQualification(request);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private String createQualification(HttpServletRequest request) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            if (isValidInput(request)) {
                Qualification qualification = new Qualification();
                qualification.setName(request.getParameter(ParameterName.NAME));
                qualification.setDescription(request.getParameter(ParameterName.DESCRIPTION));

                qualificationService.save(qualification);
                ArrayList<Qualification> qualifications = qualificationService.getAll();
                request.getSession().setAttribute(ParameterName.QUALIFICATION_LIST, qualifications);

                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
                jspPage = JspPageName.QUALIFICATIONS;
            } else {
                request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
                jspPage = JspPageName.CREATE_QUALIFICATION;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.NAME);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_SECONDARY_INPUT_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.DESCRIPTION);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_NAME_LENGTH)) {
            return false;
        }

        return true;
    }
}
