package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.bean.Work;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.QualificationProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserValidationService;
import main.com.epam.tc.finaltask.service.WorkProcessingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 5.3.16.
 */
public class DeleteQualificationCommand implements Executable {
    private QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();
    private WorkProcessingService workProcessingService = WorkProcessingService.getInstance();

    private static final int MSG_SUCCESS = 1;
    private static final int MSG_CANT_DELETE = -1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
            jspPage = deleteQualification(request);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }
        return jspPage;
    }

    private String deleteQualification(HttpServletRequest request) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            int qualificationId = Integer.parseInt(request.getParameter(ParameterName.ID));

            ArrayList<Work> works = workProcessingService.getWorksByQualificationId(qualificationId);
            if (works.isEmpty()) {
                qualificationService.delete(qualificationId);
                ArrayList<Qualification> qualifications = qualificationService.getAll();
                request.getSession().setAttribute(ParameterName.QUALIFICATION_LIST, qualifications);

                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
                jspPage = JspPageName.QUALIFICATIONS;
            } else {
                request.setAttribute(ParameterName.MESSAGE, MSG_CANT_DELETE);
                jspPage = JspPageName.EDIT_QUALIFICATION;
            }
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}
