package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by blooming on 5.3.16.
 */
public class EditProjectCommand implements Executable {
    private static final ProjectProcessingService projectService = ProjectProcessingService.getInstance();
    private static final SpecProcessingService specService = SpecProcessingService.getInstance();
    private static final WorkProcessingService workService = WorkProcessingService.getInstance();
    private static final UserProcessingService userService = UserProcessingService.getInstance();
    private static final BillProcessingService billService = BillProcessingService.getInstance();

    private static final String WORK_PARAM_PATTERN = "work\\d+:";
    private static final int MSG_SUCCESS = 1;
    private static final int MSG_INVALID_INPUT = -1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
            jspPage = editProject(request);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }
        return jspPage;
    }

    private String editProject(HttpServletRequest request) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            if (isValidInput(request)) {
                Project project = (Project) request.getSession().getAttribute(ParameterName.PROJECT);
                project.setName(request.getParameter(ParameterName.PROJECT_NAME));
                project.setDescription(request.getParameter(ParameterName.PROJECT_DESCRIPTION));

                Specification spec = specService.getSpecById(project.getId());
                Bill bill = billService.calculate(spec);
                billService.update(bill);
                ArrayList<Work> works = (ArrayList<Work>)
                        request.getSession().getAttribute(ParameterName.WORK_LIST);

                for (Work work : works) {
                    work = getWorkDataFromRequest(work, request);
                }
                workService.updateWorks(works);
                projectService.updateProjectInDataBase(project);

                updateSessionAttributes(request);
                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
                jspPage = JspPageName.PROJECTS;
            } else {
                request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
                jspPage = JspPageName.EDIT_PROJECT;
            }
        } catch (ServiceException | ClassCastException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.PROJECT_NAME);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_NAME_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.PROJECT_DESCRIPTION);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_DESCRIPTION_LENGTH)) {
            return false;
        }

        return true;
    }

    private void updateSessionAttributes(HttpServletRequest request) throws ServiceException {
        ArrayList<Specification> specifications = specService.getFullSpecList();
        ArrayList<Project> projects = new ArrayList<>();
        ArrayList<Specification> unprocessedSpec = new ArrayList<>();

        for (Specification spec : specifications) {
            if (null != spec.getProject()) {
                Project project = projectService.getProjectById(spec.getId());
                project.setCustomerName(spec.getCustomer().getName());
                projects.add(project);
            } else {
                unprocessedSpec.add(spec);
            }
        }

        request.getSession().setAttribute(ParameterName.PROJECT_LIST, projects);
        request.getSession().setAttribute(ParameterName.SPEC_LIST, unprocessedSpec);
    }

    private Work getWorkDataFromRequest(Work work, HttpServletRequest request) throws ServiceException {
        Enumeration<String> paramNames = request.getParameterNames();
        work.setDevelopers(new ArrayList<User>());
        while (paramNames.hasMoreElements()) {
            String param = paramNames.nextElement();

            Pattern workName = Pattern.compile(WORK_PARAM_PATTERN + work.getId());
            Matcher matcher = workName.matcher(param);
            if (matcher.find()) {
                String userName = request.getParameter(param);
                User user = userService.getUserByName(userName);
                work.getDevelopers().add(user);
            }
        }

        return work;
    }
}
