package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 5.3.16.
 */
public class EditUserCommand implements Executable {
    private static final UserProcessingService userService = UserProcessingService.getInstance();
    private static final QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();

    private static final int NULL = 0;
    private static final int MSG_SUCCESS = 1;
    private static final int MSG_INVALID_INPUT = -1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
            jspPage = editUser(request);
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private String editUser(HttpServletRequest request) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;
        try {
            if (isValidInput(request)) {
                User userToEdit = getUserFromRequest(request);
                userService.update(userToEdit);
                ArrayList<User> users = userService.getAllDevs();
                request.getSession().setAttribute(ParameterName.DEVELOPER_LIST, users);
                request.setAttribute(ParameterName.MESSAGE, MSG_SUCCESS);
                jspPage = JspPageName.USERS;
            } else {
                request.setAttribute(ParameterName.MESSAGE, MSG_INVALID_INPUT);
                jspPage = JspPageName.EDIT_USER;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return jspPage;
    }

    private boolean isValidInput(HttpServletRequest request) {
        InputValidationService valService = InputValidationService.getInstance();
        String input = request.getParameter(ParameterName.NAME);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_USERNAME_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.EMAIL);
        if (!valService.isValidEmail(input)) {
            return false;
        }
        input = request.getParameter(ParameterName.PASSWORD);
        if (!valService.isValid(input,
                InputValidationService.MIN_INPUT_LENGTH,
                InputValidationService.MAX_PASSWORD_LENGTH)) {
            return false;
        }
        input = request.getParameter(ParameterName.QUALIFICATION);
        if (null == input) {
            return false;
        }

        return true;
    }

    private User getUserFromRequest(HttpServletRequest request) throws CommandException {
        User newUser = new User();
        try {
            newUser.setName(request.getParameter(ParameterName.NAME));
            newUser.setEmail(request.getParameter(ParameterName.EMAIL));
            newUser.setPassword(request.getParameter(ParameterName.PASSWORD));

            String qualificationParam = request.getParameter(ParameterName.QUALIFICATION);
            int qualificationId = NULL;
            if (null != qualificationParam) {
                qualificationId = Integer.valueOf(qualificationParam);
            }
            if (NULL != qualificationId) {
                Qualification qualification = qualificationService.getById(qualificationId);
                newUser.setQualification(qualification);
            }
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }
        return newUser;
    }
}
