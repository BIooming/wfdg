package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by blooming on 5.3.16.
 */
public class GoToEditProjectCommand implements Executable {
    private SpecProcessingService specificationService = SpecProcessingService.getInstance();
    private ProjectProcessingService projectService = ProjectProcessingService.getInstance();
    private WorkProcessingService workService = WorkProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        User user = (User) request.getSession().getAttribute(ParameterName.USER);
        if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
            setSessionAttributes(request);
            jspPage = JspPageName.EDIT_PROJECT;
        } else {
            jspPage = JspPageName.LOGIN_ERROR;
        }

        return jspPage;
    }

    private void setSessionAttributes(HttpServletRequest request) throws CommandException {
        try {
            int specificationId = Integer.parseInt(request.getParameter(ParameterName.ID));
            Specification specification = specificationService.getSpecById(specificationId);
            request.getSession().setAttribute(ParameterName.SPECIFICATION, specification);

            Project projectToEdit = projectService.getProjectById(specification.getId());
            request.getSession().setAttribute(ParameterName.PROJECT, projectToEdit);

            ArrayList<Work> works = workService.getWorksBySpecId(specification.getId());
            request.getSession().setAttribute(ParameterName.WORK_LIST, works);
            UserProcessingService userService = UserProcessingService.getInstance();

            HashMap<Integer, ArrayList<User>> qualificationDevelopersMap = new HashMap<>();
            for (Work work : works) {
                Qualification qualification = work.getQualification();
                ArrayList<User> developers = new ArrayList<>();
                developers = userService.getByQualification(qualification);
                developers = userService.getAvailableDevelopers(developers);
                qualificationDevelopersMap.put(qualification.getId(), developers);
            }
            request.getSession().setAttribute(ParameterName.QUALIFICATION_MAP, qualificationDevelopersMap);
        } catch (NumberFormatException | ServiceException e) {
            throw new CommandException(e);
        }
    }
}
