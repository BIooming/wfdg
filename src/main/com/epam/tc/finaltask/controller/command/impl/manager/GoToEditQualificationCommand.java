package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.QualificationProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by blooming on 5.3.16.
 */
public class GoToEditQualificationCommand implements Executable {
    QualificationProcessingService qualificationProcessingService = QualificationProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
                int qualificationId = Integer.parseInt(request.getParameter(ParameterName.ID));
                Qualification qualification = qualificationProcessingService.getById(qualificationId);
                request.setAttribute(ParameterName.QUALIFICATION, qualification);
                jspPage = JspPageName.EDIT_QUALIFICATION;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (NumberFormatException | ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}
