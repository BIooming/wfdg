package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.QualificationProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserProcessingService;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 5.3.16.
 */
public class GoToEditUserCommand implements Executable {
    private static final UserProcessingService userService = UserProcessingService.getInstance();
    private static final QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {

                int id = Integer.parseInt(request.getParameter(ParameterName.ID));
                ArrayList<Qualification> qualifications = qualificationService.getAll();
                request.getSession().setAttribute(ParameterName.QUALIFICATION_LIST, qualifications);

                User accountToEdit = userService.getUserById(id);
                request.setAttribute(ParameterName.USER, accountToEdit);

                jspPage = JspPageName.EDIT_USER;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (NumberFormatException | ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}
