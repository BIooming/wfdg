package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Project;
import main.com.epam.tc.finaltask.bean.Specification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.ProjectProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.SpecProcessingService;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 26.2.16.
 */
public class GoToProjectsCommand implements Executable {
    private static final SpecProcessingService specService = SpecProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
                setRequestAttributes(request);
                jspPage = JspPageName.PROJECTS;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }

    private void setRequestAttributes(HttpServletRequest request) throws ServiceException {
        ArrayList<Project> projects = new ArrayList<>();
        ArrayList<Specification> unprocessedSpec = new ArrayList<>();

        ArrayList<Specification> specifications = specService.getFullSpecList();
        for (Specification spec : specifications) {
            if (null != spec.getProject()) {
                Project project = ProjectProcessingService.getInstance().getProjectById(spec.getId());
                project.setCustomerName(spec.getCustomer().getName());
                projects.add(project);
            } else {
                unprocessedSpec.add(spec);
            }
        }

        request.getSession().setAttribute(ParameterName.PROJECT_LIST, projects);
        request.getSession().setAttribute(ParameterName.SPEC_LIST, unprocessedSpec);
    }
}
