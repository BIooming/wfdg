package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.QualificationProcessingService;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 26.2.16.
 */
public class GoToQualificationsCommand implements Executable {
    private QualificationProcessingService qualificationService = QualificationProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
                ArrayList<Qualification> qualifications = qualificationService.getAll();
                request.getSession().setAttribute(ParameterName.QUALIFICATION_LIST, qualifications);
                jspPage = JspPageName.QUALIFICATIONS;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}
