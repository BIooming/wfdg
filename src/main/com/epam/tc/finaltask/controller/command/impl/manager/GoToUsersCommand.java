package main.com.epam.tc.finaltask.controller.command.impl.manager;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.controller.JspPageName;
import main.com.epam.tc.finaltask.controller.ParameterName;
import main.com.epam.tc.finaltask.controller.command.CommandException;
import main.com.epam.tc.finaltask.controller.command.Executable;
import main.com.epam.tc.finaltask.service.ServiceException;
import main.com.epam.tc.finaltask.service.UserProcessingService;
import main.com.epam.tc.finaltask.service.UserValidationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by blooming on 26.2.16.
 */
public class GoToUsersCommand implements Executable {
    private static final UserProcessingService userService = UserProcessingService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String jspPage = JspPageName.SERVER_ERROR;

        try {
            User user = (User) request.getSession().getAttribute(ParameterName.USER);
            if (UserValidationService.getInstance().isValidUser(user, UserType.MANAGER)) {
                ArrayList<User> users = userService.getAllDevs();
                request.getSession().setAttribute(ParameterName.DEVELOPER_LIST, users);
                jspPage = JspPageName.USERS;
            } else {
                jspPage = JspPageName.LOGIN_ERROR;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return jspPage;
    }
}
