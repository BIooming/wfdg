package main.com.epam.tc.finaltask.customtag;

/**
 * Created by blooming on 31.3.16.
 */

import main.com.epam.tc.finaltask.bean.Work;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

public class WorkReportTag extends BodyTagSupport {
    private Work work;

    public void setWork(Work work) {
        this.work = work;
    }

    public int doStartTag() throws JspTagException {
        try {
            pageContext.getOut().write("<div class=\"panel panel-default\">");
            pageContext.getOut().write("<div class=\"panel-heading\">");
            pageContext.getOut().write(work.getName());
            pageContext.getOut().write("</div>");
            pageContext.getOut().write("<div class=\"panel-body\">");
            pageContext.getOut().write(work.getDescription());
            pageContext.getOut().write("</div>");
            pageContext.getOut().write("<div class=\"panel-footer\">");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doAfterBody() throws JspTagException {
        try {
            pageContext.getOut().write("</div>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }

    public int doEndTag() throws JspTagException {
        try {
            pageContext.getOut().write("</div>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
