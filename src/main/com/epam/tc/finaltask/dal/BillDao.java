package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.Bill;

import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public interface BillDao {

    Bill searchByID(int id) throws DalException;

    Bill searchByCustomerId(int customerId) throws DalException;

    ArrayList<Bill> searchAll() throws DalException;

    ArrayList<Bill> searchAll(int customerId) throws DalException;

    Bill add(Bill bill) throws DalException;

    boolean update(Bill bill) throws DalException;

    boolean delete(int id) throws DalException;
}
