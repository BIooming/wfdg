package main.com.epam.tc.finaltask.dal;

import java.io.Serializable;

/**
 * Created by blooming on 24.2.16.
 */
public class DalException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;

    public DalException() {
        super();
    }

    public DalException(Exception e) {
        super(e);
    }

    public DalException(String message) {
        super(message);
    }

    public DalException(String message, Exception e) {
        super(message, e);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}