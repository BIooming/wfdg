package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.dal.factoryimpl.MySqlDaoFactory;

/**
 * Created by blooming on 24.2.16.
 */
public abstract class DaoFactory {

    public static DaoFactory getDaoFactory() {
        return MySqlDaoFactory.getInstance();
    }

    public abstract WorkTimeDao getWorkTimeDao();

    public abstract ProjectDao getProjectDao();

    public abstract QualificationsDao getQualificationsDao();

    public abstract SpecDao getSpecDao();

    public abstract UserDao getUserDao();

    public abstract UserQualificationDao getUserQualificationDao();

    public abstract UserTypeDao getUserTypeDao();

    public abstract WorkDao getWorkDao();

    public abstract BillDao getBillDao();
}
