package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.Project;

import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public interface ProjectDao {

    Project searchBySpecId(int specId) throws DalException;

    Project searchByBillId(int billId) throws DalException;

    ArrayList<Project> searchAll() throws DalException;

    Project add(Project project) throws DalException;

    boolean update(Project project) throws DalException;

    boolean delete(int id) throws DalException;
}
