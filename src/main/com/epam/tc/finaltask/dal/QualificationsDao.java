package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.Qualification;

import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public interface QualificationsDao {

    Qualification searchById(int qualificationId) throws DalException;

    ArrayList<Qualification> searchAll() throws DalException;

    Qualification add(Qualification qualification) throws DalException;

    boolean update(Qualification qualification) throws DalException;

    boolean delete(int id) throws DalException;
}
