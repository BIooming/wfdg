package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.Specification;

import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public interface SpecDao {

    Specification searchById(int specId) throws DalException;

    ArrayList<Specification> searchAll() throws DalException;

    ArrayList<Specification> searchByCustomerId(int customerId) throws DalException;

    Specification add(Specification specification) throws DalException;

    boolean update(Specification specification) throws DalException;

    boolean delete(int id) throws DalException;
}
