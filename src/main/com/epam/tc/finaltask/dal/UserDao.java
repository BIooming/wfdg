package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.User;

import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public interface UserDao {

    User searchById(int id) throws DalException;

    User searchByEmail(String email) throws DalException;

    User searchByName(String name) throws DalException;

    ArrayList<User> searchAll() throws DalException;

    User add(User user) throws DalException;

    boolean update(User user) throws DalException;

    boolean delete(int id) throws DalException;
}
