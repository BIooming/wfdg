package main.com.epam.tc.finaltask.dal;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by blooming on 24.2.16.
 */
public interface UserQualificationDao {

    ArrayList<Integer> searchUsersByQualificationId(int qualificationId) throws DalException;

    ArrayList<Integer> searchQualificationsByUserId(int userId) throws DalException;

    Hashtable<Integer, Integer> searchAll() throws DalException;

    void add(int userId, int qualificationId) throws DalException;

    void deleteByUserId(int userId) throws DalException;

    void deleteByQualificationId(int qualificationId) throws DalException;
}
