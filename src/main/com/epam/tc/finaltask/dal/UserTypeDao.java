package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.UserType;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by blooming on 24.2.16.
 */
public interface UserTypeDao {

    UserType searchByUserId(int userId) throws DalException;

    ArrayList<Integer> searchUserIdByType(UserType type) throws DalException;

    Hashtable<Integer, UserType> searchAll() throws DalException;

    void add(int userId, UserType type) throws DalException;

    boolean delete(int userId) throws DalException;
}
