package main.com.epam.tc.finaltask.dal;

import main.com.epam.tc.finaltask.bean.Work;

import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public interface WorkDao {

    Work searchById(int id) throws DalException;

    ArrayList<Work> searchByQualificationId(int qualificationId) throws DalException;

    ArrayList<Work> searchBySpecId(int specId) throws DalException;

    Work add(Work work) throws DalException;

    boolean update(Work work) throws DalException;

    boolean delete(int workId) throws DalException;
}
