package main.com.epam.tc.finaltask.dal;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by blooming on 24.2.16.
 */
public interface WorkTimeDao {

    ArrayList<Integer> searchWorkByUserId(int userId) throws DalException;

    ArrayList<Integer> searchUserByWorkId(int workId) throws DalException;

    Float searchSpentTime(int workId, int userId) throws DalException;

    Hashtable<Integer, Integer> searchAll() throws DalException;

    void add(int workId, int userId) throws DalException;

    void addTime(int userId, int workId, Float time) throws DalException;

    void setFinished(int workId, int userId) throws DalException;

    void setUnfinished(int workId, int userId) throws DalException;

    void delete(int userId, int workId) throws DalException;

    void deleteByUser(int userId) throws DalException;

    void deleteByWork(int workId) throws DalException;

    boolean isFinished(int workId, int userId) throws DalException;
}
