package main.com.epam.tc.finaltask.dal.factoryimpl;

import main.com.epam.tc.finaltask.dal.*;
import main.com.epam.tc.finaltask.dal.mysqldao.*;

/**
 * Created by blooming on 24.2.16.
 */
public final class MySqlDaoFactory extends DaoFactory {
    private static MySqlDaoFactory instance = new MySqlDaoFactory();

    public static MySqlDaoFactory getInstance() {
        return instance;
    }

    private MySqlDaoFactory() {
    }

    @Override
    public WorkTimeDao getWorkTimeDao() {
        return MySqlWorkTimeDao.getInstance();
    }

    @Override
    public ProjectDao getProjectDao() {
        return MySqlProjectDao.getInstance();
    }

    @Override
    public QualificationsDao getQualificationsDao() {
        return MySqlQualificationDao.getInstance();
    }

    @Override
    public SpecDao getSpecDao() {
        return MySqlSpecDao.getInstance();
    }

    @Override
    public UserDao getUserDao() {
        return MySqlUserDao.getInstance();
    }

    @Override
    public UserQualificationDao getUserQualificationDao() {
        return MySqlUserQualificationDao.getInstance();
    }

    @Override
    public UserTypeDao getUserTypeDao() {
        return MySqlUserTypeDao.getInstance();
    }

    @Override
    public WorkDao getWorkDao() {
        return MySqlWorkDao.getInstance();
    }

    @Override
    public BillDao getBillDao() {
        return MySqlBillDao.getInstance();
    }
}
