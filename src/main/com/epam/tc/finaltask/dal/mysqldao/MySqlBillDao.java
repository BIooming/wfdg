package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Bill;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.dal.BillDao;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public class MySqlBillDao implements BillDao {
    private static final String SEARCH_BASE = "SELECT * FROM bill ";
    private static final String BY_ID = "WHERE id_bill=?;";
    private static final String BY_CUSTOMER_ID = "WHERE id_customer=?;";
    private static final String INSERT = "INSERT INTO bill(id_customer,descript_bill,cost) VALUES(?,?,?);";
    private static final String DELETE_BASE = "DELETE FROM bill ";
    private static final String UPDATE_BASE = "UPDATE bill SET id_customer=?,descript_bill=?,cost=? ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;
    private static final int FOURTHS = 4;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlBillDao instance = new MySqlBillDao();

    public static MySqlBillDao getInstance() {
        return instance;
    }

    private MySqlBillDao() {
    }

    @Override
    public Bill searchByID(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Bill bill = null;
        try {
            String sql = SEARCH_BASE + BY_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, id);
            resultSet = statement.executeQuery();
            ArrayList<Bill> result = getBillFromSet(resultSet);
            if (!result.isEmpty()) {
                bill = result.get(0);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return bill;
    }

    @Override
    public Bill searchByCustomerId(int customerId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Bill bill = null;
        try {
            String sql = SEARCH_BASE + BY_CUSTOMER_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, customerId);
            resultSet = statement.executeQuery();
            bill = getBillFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return bill;
    }

    @Override
    public ArrayList<Bill> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Bill> billList = null;
        try {
            String sql = SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            billList = getBillFromSet(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return billList;
    }

    @Override
    public ArrayList<Bill> searchAll(int customerId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Bill> billList = null;
        try {
            String sql = SEARCH_BASE + BY_CUSTOMER_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, customerId);
            resultSet = statement.executeQuery();
            billList = getBillFromSet(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return billList;
    }

    @Override
    public Bill add(Bill bill) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(INSERT,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, bill.getCustomer().getId());
            statement.setString(SECOND, bill.getDescription());
            statement.setInt(THIRD, bill.getCost());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                bill.setId(resultSet.getInt(FIRST));
            }
        } catch (SQLException | ConnectionPoolException e) {
            e.printStackTrace();
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return bill;
    }

    @Override
    public boolean update(Bill bill) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(UPDATE_BASE + BY_ID,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, bill.getCustomer().getId());
            statement.setString(SECOND, bill.getDescription());
            statement.setInt(THIRD, bill.getCost());
            statement.setInt(FOURTHS, bill.getId());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            e.printStackTrace();
            return false;
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_ID,
                    Statement.RETURN_GENERATED_KEYS);
            statement.setInt(FIRST, id);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    private ArrayList<Bill> getBillFromSet(ResultSet resultSet) throws DalException {
        ArrayList<Bill> billList = new ArrayList<>();

        try {
            while (resultSet.next()) {
                Bill bill = new Bill();
                bill.setId(resultSet.getInt("id_bill"));
                bill.setDescription(resultSet.getString("descript_bill"));
                bill.setCost(resultSet.getInt("cost"));

                User user = new User();
                user.setId(resultSet.getInt("id_customer"));
                bill.setCustomer(user);

                billList.add(bill);
            }
        } catch (SQLException e) {
            throw new DalException(e);
        }

        return billList;
    }
}
