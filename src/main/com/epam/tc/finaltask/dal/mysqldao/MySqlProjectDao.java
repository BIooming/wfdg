package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Bill;
import main.com.epam.tc.finaltask.bean.Project;
import main.com.epam.tc.finaltask.bean.ProjectStatus;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.ProjectDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public class MySqlProjectDao implements ProjectDao {
    private static final String SEARCH_BASE = "SELECT * FROM project ";

    private static final String BY_SPEC = "WHERE id_spec=?;";
    private static final String BY_BILL = "WHERE id_bill=?";
    private static final String INSERT =
            "INSERT INTO project(id_spec,id_bill,name_project,descript_project,status) " +
                    "VALUES(?,?,?,?,?);";
    private static final String UPDATE_BASE = "UPDATE project " +
            "SET name_project=?,descript_project=?,status=? ";
    private static final String DELETE_BASE = "DELETE FROM project ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;
    private static final int FOURTH = 4;
    private static final int FIFTH = 5;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlProjectDao instance = new MySqlProjectDao();

    public static MySqlProjectDao getInstance() {
        return instance;
    }

    private MySqlProjectDao() {
    }

    @Override
    public Project searchBySpecId(int specId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Project project = null;
        try {
            String sql = SEARCH_BASE + BY_SPEC;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, specId);
            resultSet = statement.executeQuery();
            project = getProjectFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            project = null;
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return project;
    }

    @Override
    public Project searchByBillId(int billId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Project project = null;
        try {
            String sql = SEARCH_BASE + BY_BILL;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, billId);
            resultSet = statement.executeQuery();
            project = getProjectFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return project;
    }

    @Override
    public ArrayList<Project> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Project> projects = null;
        try {
            String sql = SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            projects = getProjectFromSet(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return projects;
    }

    @Override
    public Project add(Project project) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(INSERT,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, project.getId());
            if (null == project.getBill()) {
                statement.setNull(SECOND, 0);
            } else {
                statement.setInt(SECOND, project.getBill().getId());
            }
            statement.setString(THIRD, project.getName());
            statement.setString(FOURTH, project.getDescription());
            statement.setString(FIFTH, project.getStatus().name());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return project;
    }

    @Override
    public boolean update(Project project) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(UPDATE_BASE + BY_SPEC,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(FIRST, project.getName());
            statement.setString(SECOND, project.getDescription());
            statement.setString(THIRD, project.getStatus().name());
            statement.setInt(FOURTH, project.getId());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_SPEC,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, id);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    private ArrayList<Project> getProjectFromSet(ResultSet resultSet) throws DalException {
        ArrayList<Project> projects = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getInt("id_spec"));
                project.setName(resultSet.getString("name_project"));
                project.setStatus(ProjectStatus.valueOf(resultSet.getString("status")));
                project.setDescription(resultSet.getString("descript_project"));
                Bill bill = new Bill();
                bill.setId(resultSet.getInt("id_bill"));
                project.setBill(bill);
                projects.add(project);
            }
        } catch (SQLException e) {
            throw new DalException(e);
        }
        return projects;
    }
}