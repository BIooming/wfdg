package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.QualificationsDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public class MySqlQualificationDao implements QualificationsDao {
    private static final String SEARCH_BASE = "SELECT * FROM qualification ";
    private static final String BY_ID = "WHERE id_qualification=?;";
    private static final String INSERT =
            "INSERT INTO qualification(name_qualification,descript_qualification) VALUES(?,?);";
    private static final String DELETE_BASE = "DELETE FROM qualification ";
    private static final String UPDATE_BASE = "UPDATE qualification " +
            "SET name_qualification=?,descript_qualification=? ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;

    private final static ConnectionPool connectionPool = ConnectionPool.getInstance();
    private static MySqlQualificationDao instance = new MySqlQualificationDao();

    public static MySqlQualificationDao getInstance() {
        return instance;
    }

    private MySqlQualificationDao() {
    }

    @Override
    public Qualification searchById(int qualificationId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Qualification qualification = null;
        try {
            String sql = SEARCH_BASE + BY_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, qualificationId);
            resultSet = statement.executeQuery();
            qualification = getQualificationFromResult(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            qualification = null;
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return qualification;
    }

    @Override
    public ArrayList<Qualification> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Qualification> qualifications = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SEARCH_BASE + SEMICOLON,
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            qualifications = getQualificationFromResult(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return qualifications;
    }

    @Override
    public Qualification add(Qualification qualification) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            statement.setString(FIRST, qualification.getName());
            String description = qualification.getDescription();
            if (null != description) {
                statement.setString(SECOND, qualification.getDescription());
            }
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                qualification.setId(resultSet.getInt(FIRST));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return qualification;
    }

    @Override
    public boolean update(Qualification qualification) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(UPDATE_BASE + BY_ID);
            statement.setString(FIRST, qualification.getName());
            String description = qualification.getDescription();
            if (null != description) {
                statement.setString(SECOND, qualification.getDescription());
            } else {
                statement.setString(SECOND, "");
            }
            statement.setInt(THIRD, qualification.getId());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_ID, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(FIRST, id);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    private ArrayList<Qualification> getQualificationFromResult(ResultSet resultSet) throws DalException {
        ArrayList<Qualification> qualifications = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Qualification qualification = new Qualification();
                qualification.setId(resultSet.getInt("id_qualification"));
                qualification.setName(resultSet.getString("name_qualification"));
                String description = resultSet.getString("descript_qualification");
                if (null != description) {
                    qualification.setDescription(description);
                }
                qualifications.add(qualification);
            }
        } catch (SQLException e) {
            throw new DalException(e);
        }
        return qualifications;
    }
}
