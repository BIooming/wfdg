package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Specification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.SpecDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public class MySqlSpecDao implements SpecDao {
    private static final String SEARCH_BASE = "SELECT * FROM spec ";
    private static final String BY_ID = "WHERE id_spec=?;";
    private static final String BY_CUSTOMER = "WHERE id_customer=?;";
    private static final String INSERT =
            "INSERT INTO spec(id_customer,name_spec,descript_spec) VALUES(?,?,?);";
    private static final String UPDATE_BASE =
            "UPDATE spec SET id_customer=?,name_spec=?,descript_spec=? ";
    private static final String DELETE_BASE = "DELETE FROM spec ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;
    private static final int FOURTH = 4;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlSpecDao instance = new MySqlSpecDao();

    public static MySqlSpecDao getInstance() {
        return instance;
    }

    private MySqlSpecDao() {
    }

    @Override
    public Specification searchById(int specId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Specification specification = null;
        try {
            String sql = SEARCH_BASE + BY_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, specId);
            resultSet = statement.executeQuery();
            specification = getSpecificationFromResult(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            specification = null;
        } finally {
            connectionPool.closeConnection(connection, statement);
        }

        return specification;
    }

    @Override
    public ArrayList<Specification> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Specification> specifications = null;
        try {
            String sql = SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            specifications = getSpecificationFromResult(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return specifications;
    }

    @Override
    public ArrayList<Specification> searchByCustomerId(int customerId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Specification> specifications = null;
        try {
            String sql = SEARCH_BASE + BY_CUSTOMER;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, customerId);
            resultSet = statement.executeQuery();
            specifications = getSpecificationFromResult(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            e.printStackTrace();
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return specifications;
    }

    @Override
    public Specification add(Specification specification) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(INSERT,
                    Statement.RETURN_GENERATED_KEYS);
            statement.setInt(FIRST, specification.getCustomer().getId());
            statement.setString(SECOND, specification.getName());
            statement.setString(THIRD, specification.getDescription());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                specification.setId(resultSet.getInt(FIRST));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return specification;
    }

    @Override
    public boolean update(Specification specification) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(UPDATE_BASE + BY_ID,
                    ResultSet.TYPE_FORWARD_ONLY);
            statement.setInt(FIRST, specification.getCustomer().getId());
            statement.setString(SECOND, specification.getName());
            statement.setString(THIRD, specification.getDescription());
            statement.setInt(FOURTH, specification.getId());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_ID,
                    ResultSet.TYPE_FORWARD_ONLY);
            statement.setInt(FIRST, id);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    private ArrayList<Specification> getSpecificationFromResult(ResultSet resultSet) throws DalException {
        ArrayList<Specification> specifications = new ArrayList<>();
        Specification spec = null;
        try {
            while (resultSet.next()) {
                spec = new Specification();
                spec.setId(resultSet.getInt("id_spec"));
                spec.setDescription(resultSet.getString("descript_spec"));
                spec.setName(resultSet.getString("name_spec"));
                User user = new User();
                user.setId(resultSet.getInt("id_customer"));
                spec.setCustomer(user);
                specifications.add(spec);
            }
        } catch (SQLException e) {
            throw new DalException(e);
        }
        return specifications;
    }
}