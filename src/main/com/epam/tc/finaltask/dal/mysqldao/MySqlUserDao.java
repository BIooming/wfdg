package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.UserDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public class MySqlUserDao implements UserDao {
    private static final String SQL_INSERT = "INSERT INTO user(name_user,email,password) VALUES (?,?,?);";
    private static final String SQL_UPDATE_BASE = "UPDATE user SET name_user=?,email=?,password=? ";
    private static final String SQL_DELETE_BASE = "DELETE FROM user ";
    private static final String SQL_SEARCH_BASE = "SELECT * FROM user ";
    private static final String BY_ID = "WHERE id_user=?;";
    private static final String BY_EMAIL = "WHERE email=?;";
    private static final String BY_NAME = "WHERE name_user=?;";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;
    private static final int FOURTH = 4;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlUserDao instance = new MySqlUserDao();

    public static MySqlUserDao getInstance() {
        return instance;
    }

    private MySqlUserDao() {
    }

    @Override
    public User searchById(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            String sql = SQL_SEARCH_BASE + BY_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, id);
            resultSet = statement.executeQuery();
            user = getUserFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            user = new User();
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return user;
    }

    @Override
    public User searchByEmail(String email) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            String sql = SQL_SEARCH_BASE + BY_EMAIL;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setString(FIRST, email);
            resultSet = statement.executeQuery();
            user = getUserFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            user = new User();
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return user;
    }

    @Override
    public User searchByName(String name) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            String sql = SQL_SEARCH_BASE + BY_NAME;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setString(FIRST, name);
            resultSet = statement.executeQuery();
            user = getUserFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            user = new User();
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return user;
    }

    @Override
    public ArrayList<User> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<User> users = null;
        try {
            String sql = SQL_SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            users = getUserFromSet(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return users;
    }

    @Override
    public User add(User user) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(
                    SQL_INSERT, Statement.RETURN_GENERATED_KEYS);

            statement.setString(FIRST, user.getName());
            statement.setString(SECOND, user.getEmail());
            statement.setString(THIRD, user.getPassword());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                user.setId(resultSet.getInt(FIRST));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return user;
    }

    @Override
    public boolean update(User user) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(
                    SQL_UPDATE_BASE + BY_ID, Statement.RETURN_GENERATED_KEYS);

            statement.setString(FIRST, user.getName());
            statement.setString(SECOND, user.getEmail());
            statement.setString(THIRD, user.getPassword());
            statement.setInt(FOURTH, user.getId());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(
                    SQL_DELETE_BASE + BY_ID, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, id);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    private ArrayList<User> getUserFromSet(ResultSet resultSet) throws DalException {
        ArrayList<User> userArrayList = new ArrayList<>();
        try {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id_user"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name_user"));
                user.setEmail(resultSet.getString("email"));
                userArrayList.add(user);
            }
        } catch (SQLException e) {
            throw new DalException(e);
        }
        return userArrayList;
    }
}
