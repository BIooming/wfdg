package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.UserQualificationDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by blooming on 24.2.16.
 */
public class MySqlUserQualificationDao implements UserQualificationDao {
    private static final String SEARCH_BASE = "SELECT * FROM userqualification ";
    private static final String BY_QUALIFICATION = "WHERE id_qualification=?;";
    private static final String BY_USER = "WHERE id_user=?;";
    private static final String INSERT =
            "INSERT INTO userqualification(id_user,id_qualification) VALUES(?,?);";
    private static final String DELETE_BASE = "DELETE FROM userqualification ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlUserQualificationDao instance = new MySqlUserQualificationDao();

    public static MySqlUserQualificationDao getInstance() {
        return instance;
    }

    private MySqlUserQualificationDao() {
    }

    @Override
    public ArrayList<Integer> searchUsersByQualificationId(int qualificationId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> userIdList = new ArrayList<>();
        try {
            String sql = SEARCH_BASE + BY_QUALIFICATION;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, qualificationId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userIdList.add(resultSet.getInt("id_user"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return userIdList;
    }

    @Override
    public ArrayList<Integer> searchQualificationsByUserId(int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> qualificationIdList = new ArrayList<>();
        try {
            String sql = SEARCH_BASE + BY_USER;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                qualificationIdList.add(resultSet.getInt("id_qualification"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return qualificationIdList;
    }

    @Override
    public Hashtable<Integer, Integer> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Hashtable<Integer, Integer> resultTable = new Hashtable<>();
        try {
            String sql = SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                resultTable.put(resultSet.getInt("id_user"), resultSet.getInt("id_qualification"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return resultTable;
    }

    @Override
    public void add(int userId, int qualificationId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(INSERT);
            statement.setInt(FIRST, userId);
            statement.setInt(SECOND, qualificationId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void deleteByUserId(int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_USER);
            statement.setInt(FIRST, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void deleteByQualificationId(int qualificationId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_QUALIFICATION);
            statement.setInt(FIRST, qualificationId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }
}
