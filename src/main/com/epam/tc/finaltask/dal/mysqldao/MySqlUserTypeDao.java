package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.UserTypeDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by blooming on 24.2.16.
 */
public final class MySqlUserTypeDao implements UserTypeDao {
    private static final String SEARCH_BASE = "SELECT * FROM usertype ";
    private static final String BY_ID = "WHERE id_user=?;";
    private static final String BY_TYPE = "WHERE type=?;";
    private static final String INSERT = "INSERT INTO usertype(id_user,type) VALUES(?,?);";
    private static final String DELETE = "DELETE FROM usertype ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();
    private static MySqlUserTypeDao instance = new MySqlUserTypeDao();

    public static MySqlUserTypeDao getInstance() {
        return instance;
    }

    private MySqlUserTypeDao() {
    }

    @Override
    public UserType searchByUserId(int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        UserType userType = null;
        try {
            String sql = SEARCH_BASE + BY_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userType = UserType.valueOf(resultSet.getString("type"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return userType;
    }

    @Override
    public ArrayList<Integer> searchUserIdByType(UserType type) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> userIdList = new ArrayList<>();
        try {
            String sql = SEARCH_BASE + BY_TYPE;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setString(FIRST, type.name());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userIdList.add(resultSet.getInt("id_user"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return userIdList;
    }

    @Override
    public Hashtable<Integer, UserType> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Hashtable<Integer, UserType> result = null;
        try {
            String sql = SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            result = new Hashtable<>();
            while (resultSet.next()) {
                result.put(resultSet.getInt("id_user"),
                        UserType.valueOf(resultSet.getString("type")));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return result;
    }

    @Override
    public void add(int userId, UserType type) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(INSERT);
            statement.setInt(FIRST, userId);
            statement.setString(SECOND, type.name());
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public boolean delete(int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE + BY_ID);
            statement.setInt(FIRST, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }
}