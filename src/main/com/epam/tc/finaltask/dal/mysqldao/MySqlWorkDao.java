package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.Work;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.WorkDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by blooming on 24.2.16.
 */
public final class MySqlWorkDao implements WorkDao {
    private static final String SEARCH_BASE = "SELECT * FROM work ";
    private static final String BY_ID = "WHERE id_work=?;";
    private static final String BY_QUALIFICATION = "WHERE id_qualification=?;";
    private static final String BY_SPEC = "WHERE id_spec=?;";
    private static final String INSERT =
            "INSERT INTO work(id_qualification,id_spec,name_work,descript_work,number_devs) VALUES(?,?,?,?,?);";
    private static final String UPDATE = "UPDATE work " +
            "SET id_qualification=?,name_work=?,descript_work=?,number_devs=? ";
    private static final String DELETE = "DELETE from work ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;
    private static final int FOURTH = 4;
    private static final int FIFTH = 5;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlWorkDao instance = new MySqlWorkDao();

    public static MySqlWorkDao getInstance() {
        return instance;
    }

    private MySqlWorkDao() {
    }

    @Override
    public Work searchById(int id) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Work work = null;
        try {
            String sql = SEARCH_BASE + BY_ID;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, id);
            resultSet = statement.executeQuery();
            work = getWorkListFromSet(resultSet).get(0);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } catch (IndexOutOfBoundsException e) {
            work = null;
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return work;
    }

    @Override
    public ArrayList<Work> searchByQualificationId(int qualificationId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Work> works = null;
        try {
            String sql = SEARCH_BASE + BY_QUALIFICATION;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, qualificationId);
            resultSet = statement.executeQuery();
            works = getWorkListFromSet(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return works;
    }

    @Override
    public ArrayList<Work> searchBySpecId(int specId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Work> works = null;
        try {
            String sql = SEARCH_BASE + BY_SPEC;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, specId);
            resultSet = statement.executeQuery();
            works = getWorkListFromSet(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return works;
    }

    @Override
    public Work add(Work work) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(
                    INSERT, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, work.getQualification().getId());
            statement.setInt(SECOND, work.getSpecId());
            statement.setString(THIRD, work.getName());
            statement.setString(FOURTH, work.getDescription());
            statement.setInt(FIFTH, work.getDevelopersNumber());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                work.setId(resultSet.getInt(FIRST));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return work;
    }

    @Override
    public boolean update(Work work) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(
                    UPDATE + BY_ID, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, work.getQualification().getId());
            statement.setString(SECOND, work.getName());
            statement.setString(THIRD, work.getDescription());
            statement.setInt(FOURTH, work.getDevelopersNumber());
            statement.setInt(FIFTH, work.getId());
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    @Override
    public boolean delete(int workId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(
                    DELETE + BY_ID, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(FIRST, workId);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return true;
    }

    private ArrayList<Work> getWorkListFromSet(ResultSet resultSet) throws DalException {
        ArrayList<Work> works = new ArrayList<>();
        Work work = null;
        try {
            while (resultSet.next()) {
                work = new Work();
                work.setId(resultSet.getInt("id_work"));
                work.setSpecId(resultSet.getInt("id_spec"));
                work.setName(resultSet.getString("name_work"));
                work.setDescription(resultSet.getString("descript_work"));
                work.setDevelopersNumber(resultSet.getInt("number_devs"));
                Qualification qualification = new Qualification();
                qualification.setId(resultSet.getInt("id_qualification"));
                work.setQualification(qualification);
                works.add(work);
            }
        } catch (SQLException e) {
            throw new DalException(e);
        }
        return works;
    }
}
