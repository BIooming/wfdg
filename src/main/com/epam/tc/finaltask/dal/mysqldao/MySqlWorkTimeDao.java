package main.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.WorkTimeDao;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPool;
import main.com.epam.tc.finaltask.dal.mysqldao.connectionpool.ConnectionPoolException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by blooming on 24.2.16.
 */
public final class MySqlWorkTimeDao implements WorkTimeDao {
    private static final String SQL_SEARCH_BASE = "SELECT * FROM worktime ";
    private static final String BY_WORK = "WHERE id_work=?;";
    private static final String BY_USER = "WHERE id_dev=?;";
    private static final String BY_WORK_AND_USER = "WHERE id_work=? AND id_dev=?;";
    private static final String SQL_SET_FINISHED = "UPDATE worktime SET is_finished=? ";
    private static final String SQL_ADD_TIME =
            "UPDATE worktime SET timespent=? WHERE id_work=? AND id_dev=?;";
    private static final String SQL_INSERT =
            "INSERT INTO worktime(id_work,id_dev) VALUES(?,?);";
    private static final String DELETE_BASE = "DELETE FROM worktime ";
    private static final String SEMICOLON = ";";

    private static final int FIRST = 1;
    private static final int SECOND = 2;
    private static final int THIRD = 3;

    private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private static MySqlWorkTimeDao instance = new MySqlWorkTimeDao();

    public static MySqlWorkTimeDao getInstance() {
        return instance;
    }

    private MySqlWorkTimeDao() {
    }

    @Override
    public ArrayList<Integer> searchWorkByUserId(int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> workIdList = new ArrayList<>();
        try {
            String sql = SQL_SEARCH_BASE + BY_USER;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                workIdList.add(resultSet.getInt("id_work"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return workIdList;
    }

    @Override
    public ArrayList<Integer> searchUserByWorkId(int workId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> usersId = new ArrayList<>();//TODO rename
        try {
            String sql = SQL_SEARCH_BASE + BY_WORK;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, workId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                usersId.add(resultSet.getInt("id_dev"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return usersId;
    }

    @Override
    public Float searchSpentTime(int workId, int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Float spentTime = 0F;
        try {
            String sql = SQL_SEARCH_BASE + BY_WORK_AND_USER;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, workId);
            statement.setInt(SECOND, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                spentTime = resultSet.getFloat("timespent");
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return spentTime;
    }

    @Override
    public Hashtable<Integer, Integer> searchAll() throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Hashtable<Integer, Integer> result = null;
        try {
            String sql = SQL_SEARCH_BASE + SEMICOLON;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery();
            result = new Hashtable<>();
            while (resultSet.next()) {
                result.put(resultSet.getInt("id_work"), resultSet.getInt("id_dev"));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return result;
    }

    @Override
    public void add(int workId, int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SQL_INSERT);
            statement.setInt(FIRST, workId);
            statement.setInt(SECOND, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void addTime(int userId, int workId, Float time) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SQL_ADD_TIME);
            statement.setFloat(FIRST, time);
            statement.setInt(SECOND, workId);
            statement.setInt(THIRD, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void setFinished(int workId, int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SQL_SET_FINISHED + BY_WORK_AND_USER);
            statement.setBoolean(FIRST, true);
            statement.setInt(SECOND, workId);
            statement.setInt(THIRD, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void setUnfinished(int workId, int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(SQL_SET_FINISHED + BY_WORK_AND_USER);
            statement.setBoolean(FIRST, false);
            statement.setInt(SECOND, workId);
            statement.setInt(THIRD, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void delete(int userId, int workId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_WORK_AND_USER);
            statement.setInt(FIRST, workId);
            statement.setInt(SECOND, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void deleteByUser(int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_USER);
            statement.setInt(FIRST, userId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public void deleteByWork(int workId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(DELETE_BASE + BY_WORK);
            statement.setInt(FIRST, workId);
            statement.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
    }

    @Override
    public boolean isFinished(int workId, int userId) throws DalException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Boolean isFinished = false;
        try {
            String sql = SQL_SEARCH_BASE + BY_WORK_AND_USER;
            connection = connectionPool.takeConnection();
            statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setInt(FIRST, workId);
            statement.setInt(SECOND, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                isFinished = resultSet.getBoolean("is_finished");
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DalException(e);
        } finally {
            connectionPool.closeConnection(connection, statement);
        }
        return isFinished;
    }
}
