package main.com.epam.tc.finaltask.dal.mysqldao.connectionpool;

import main.com.epam.tc.finaltask.dal.mysqldao.dbproperties.DBProperties;
import main.com.epam.tc.finaltask.dal.mysqldao.dbproperties.DBPropertiesManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

/**
 * Created by blooming on 9.2.16.
 */
public class ConnectionPool {
    private static final Logger logger = Logger.getLogger("root");
    private static ConnectionPool instance = new ConnectionPool();

    private BlockingQueue<Connection> connections;
    private BlockingQueue<Connection> givenAwayConnections;

    private String driverName;
    private String url;
    private String user;
    private String password;
    private String encoding;
    private String useUnicode;
    private int poolSize;

    private ConnectionPool() {
        DBPropertiesManager dbPropertiesManager = DBPropertiesManager.getInstance();
        this.driverName = dbPropertiesManager.getProperty(DBProperties.DB_DRIVER);
        this.url = dbPropertiesManager.getProperty(DBProperties.DB_URL);

        this.user = dbPropertiesManager.getProperty(DBProperties.DB_USER);
        this.password = dbPropertiesManager.getProperty(DBProperties.DB_PASSWORD);
        this.encoding = dbPropertiesManager.getProperty(DBProperties.DB_ENCODING);
        this.useUnicode = dbPropertiesManager.getProperty(DBProperties.DB_USE_UNICODE);

        try {
            this.poolSize = Integer.parseInt(
                    dbPropertiesManager.getProperty(DBProperties.DB_POOL_SIZE));
        } catch (NumberFormatException e) {
            this.poolSize = 5;
        }

        initPoolData();
    }

    public static ConnectionPool getInstance() {
        return instance;
    }

    public void initPoolData() {
        try {
            Class.forName(driverName);
            givenAwayConnections = new ArrayBlockingQueue<Connection>(poolSize);
            connections = new ArrayBlockingQueue<Connection>(poolSize);
            Properties properties = new Properties();
            properties.setProperty("user", user);
            properties.setProperty("password", password);
            properties.setProperty("useUnicode", useUnicode);
            properties.setProperty("characterEncoding", encoding);
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(url, properties);
                PooledConnection pooledConnection = new PooledConnection(connection);
                connections.add(pooledConnection);
            }
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.SQL_EXCEPTION, e);
        } catch (ClassNotFoundException e) {
            logger.error(ConnectionPoolErrorDescription.DB_DRIVER_NOT_FOUND, e);
        }
    }

    public void dispose() {
        clearConnectionQueue();
    }

    private void clearConnectionQueue() {
        try {
            closeConnectionsQueue(givenAwayConnections);
            closeConnectionsQueue(connections);
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.CLOSE_CONNECTION_ERROR, e);
        }
    }

    public Connection takeConnection() throws ConnectionPoolException {
        Connection connection = null;
        try {
            connection = connections.take();
            givenAwayConnections.add(connection);
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(ConnectionPoolErrorDescription.DATA_SOURCE_CONNECT_ERROR, e);
        }
        return connection;
    }

    public void closeConnection(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.RETURN_CONNECTION_ERROR);
        }

        try {
            resultSet.close();
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.CLOSE_SET_ERROR);
        }

        try {
            statement.close();
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.CLOSE_STATMENT_ERROR);
        }
    }

    public void closeConnection(Connection connection, Statement statement) {
        try {
            connection.setAutoCommit(true);
            connection.close();
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.RETURN_CONNECTION_ERROR);
        }

        try {
            statement.close();
        } catch (SQLException e) {
            logger.error(ConnectionPoolErrorDescription.CLOSE_STATMENT_ERROR);
        }
    }

    public void closeConnectionsQueue(BlockingQueue<Connection> queue)
            throws SQLException {
        Connection connection;
        while (null != (connection = queue.poll())) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            ((PooledConnection) connection).reallyClose();
        }
    }

    private class PooledConnection implements Connection {
        private Connection connection;

        public PooledConnection(Connection connection) throws SQLException {
            this.connection = connection;
            this.connection.setAutoCommit(true);
        }

        public void reallyClose() throws SQLException {
            this.connection.close();
        }

        @Override
        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        @Override
        public PreparedStatement prepareStatement(String s) throws SQLException {
            return connection.prepareStatement(s);
        }

        @Override
        public CallableStatement prepareCall(String s) throws SQLException {
            return connection.prepareCall(s);
        }

        @Override
        public String nativeSQL(String s) throws SQLException {
            return connection.nativeSQL(s);
        }

        @Override
        public void setAutoCommit(boolean b) throws SQLException {
            connection.setAutoCommit(b);
        }

        @Override
        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        @Override
        public void commit() throws SQLException {
            connection.commit();
        }

        @Override
        public void rollback() throws SQLException {
            connection.rollback();
        }

        @Override
        public void close() throws SQLException {
            if (connection.isClosed()) {
                throw new SQLException(ConnectionPoolErrorDescription.CLOSE_CLOSED_CONNECTION_ERROR);
            }
            if (connection.isReadOnly()) {
                connection.setReadOnly(false);
            }
            if (!givenAwayConnections.remove(this)) {
                throw new SQLException(ConnectionPoolErrorDescription.DELETE_FROM_GIVENAWAY_ERROR);
            }
            if (!connections.offer(this)) {
                throw new SQLException(ConnectionPoolErrorDescription.ALLOCATING_ERROR);
            }
        }

        @Override
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        @Override
        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        @Override
        public void setReadOnly(boolean b) throws SQLException {
            connection.setReadOnly(b);
        }

        @Override
        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        @Override
        public void setCatalog(String s) throws SQLException {
            connection.setCatalog(s);
        }

        @Override
        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        @Override
        public void setTransactionIsolation(int i) throws SQLException {
            connection.setTransactionIsolation(i);
        }

        @Override
        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        @Override
        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        @Override
        public Statement createStatement(int i, int i1) throws SQLException {
            return connection.createStatement(i, i1);
        }

        @Override
        public PreparedStatement prepareStatement(String s, int i, int i1) throws SQLException {
            return connection.prepareStatement(s, i, i1);
        }

        @Override
        public CallableStatement prepareCall(String s, int i, int i1) throws SQLException {
            return connection.prepareCall(s, i, i1);
        }

        @Override
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        @Override
        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
            connection.setTypeMap(map);
        }

        @Override
        public void setHoldability(int i) throws SQLException {
            connection.setHoldability(i);
        }

        @Override
        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        @Override
        public Savepoint setSavepoint(String s) throws SQLException {
            return connection.setSavepoint(s);
        }

        @Override
        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback(savepoint);
        }

        @Override
        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        @Override
        public Statement createStatement(int i, int i1, int i2) throws SQLException {
            return connection.createStatement(i, i1, i2);
        }

        @Override
        public PreparedStatement prepareStatement(String s, int i, int i1, int i2) throws SQLException {
            return connection.prepareStatement(s, i, i1, i2);
        }

        @Override
        public CallableStatement prepareCall(String s, int i, int i1, int i2) throws SQLException {
            return connection.prepareCall(s, i, i1, i2);
        }

        @Override
        public PreparedStatement prepareStatement(String s, int i) throws SQLException {
            return connection.prepareStatement(s, i);
        }

        @Override
        public PreparedStatement prepareStatement(String s, int[] ints) throws SQLException {
            return connection.prepareStatement(s, ints);
        }

        @Override
        public PreparedStatement prepareStatement(String s, String[] strings) throws SQLException {
            return connection.prepareStatement(s, strings);
        }

        @Override
        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        @Override
        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        @Override
        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        @Override
        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        @Override
        public boolean isValid(int i) throws SQLException {
            return connection.isValid(i);
        }

        @Override
        public void setClientInfo(String s, String s1) throws SQLClientInfoException {
            connection.setClientInfo(s, s1);
        }

        @Override
        public void setClientInfo(Properties properties) throws SQLClientInfoException {
            connection.setClientInfo(properties);
        }

        @Override
        public String getClientInfo(String s) throws SQLException {
            return connection.getClientInfo(s);
        }

        @Override
        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        @Override
        public Array createArrayOf(String s, Object[] objects) throws SQLException {
            return connection.createArrayOf(s, objects);
        }

        @Override
        public Struct createStruct(String s, Object[] objects) throws SQLException {
            return connection.createStruct(s, objects);
        }

        @Override
        public void setSchema(String s) throws SQLException {
            connection.setSchema(s);
        }

        @Override
        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        @Override
        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        @Override
        public void setNetworkTimeout(Executor executor, int i) throws SQLException {
            connection.setNetworkTimeout(executor, i);
        }

        @Override
        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        @Override
        public <T> T unwrap(Class<T> aClass) throws SQLException {
            return connection.unwrap(aClass);
        }

        @Override
        public boolean isWrapperFor(Class<?> aClass) throws SQLException {
            return connection.isWrapperFor(aClass);
        }
    }
}


