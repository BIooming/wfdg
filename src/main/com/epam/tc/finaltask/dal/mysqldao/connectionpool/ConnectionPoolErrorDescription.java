package main.com.epam.tc.finaltask.dal.mysqldao.connectionpool;

/**
 * Created by blooming on 9.2.16.
 */
public final class ConnectionPoolErrorDescription {
    public static final String SQL_EXCEPTION = "SQL Exception in the connection pool";
    public static final String DB_DRIVER_NOT_FOUND = "Can't find database driver class";
    public static final String DATA_SOURCE_CONNECT_ERROR = "Error connecting  to data source";
    public static final String CLOSE_CONNECTION_ERROR = "Error closing database connection";

    public static final String RETURN_CONNECTION_ERROR = "Connection isn't return to the pool";
    public static final String CLOSE_SET_ERROR = "Result set isn't closed";
    public static final String CLOSE_STATMENT_ERROR = "Statement isn't closed";
    public static final String CLOSE_CLOSED_CONNECTION_ERROR = "Attempting to close closed connection";

    public static final String DELETE_FROM_GIVENAWAY_ERROR = "Error deleting connection from given away connections pool";
    public static final String ALLOCATING_ERROR = "Error allocating connections in the pool";

    private ConnectionPoolErrorDescription() {
    }
}
