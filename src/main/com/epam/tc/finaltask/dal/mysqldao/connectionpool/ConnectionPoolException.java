package main.com.epam.tc.finaltask.dal.mysqldao.connectionpool;

/**
 * Created by blooming on 9.2.16.
 */
public class ConnectionPoolException extends Exception {
    private static final long SerialVersionUID = 1L;

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Exception e) {
        super(message, e);
    }
}
