package main.com.epam.tc.finaltask.dal.mysqldao.dbproperties;

/**
 * Created by blooming on 9.2.16.
 */
public final class DBProperties {
    public static final String DB_CONFIG_FILE = "main.com.epam.tc.finaltask.dal.mysqldao.dbproperties.dbconfig";

    public static final String DB_DRIVER = "db.driver";
    public static final String DB_URL = "db.url";
    public static final String DB_USER = "db.user";
    public static final String DB_PASSWORD = "db.password";
    public static final String DB_POOL_SIZE = "db.poolsize";
    public static final String DB_USE_UNICODE = "db.useUnicode";
    public static final String DB_ENCODING = "db.encoding";

    private DBProperties() {
    }
}
