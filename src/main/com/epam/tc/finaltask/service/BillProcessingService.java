package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.Bill;
import main.com.epam.tc.finaltask.bean.Specification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.Work;
import main.com.epam.tc.finaltask.dal.BillDao;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.UserDao;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 26.2.16.
 */
public final class BillProcessingService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final BillDao BILL_DAO = DAO_FACTORY.getBillDao();
    private static final UserDao USER_DAO = DAO_FACTORY.getUserDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final BillProcessingService instance = new BillProcessingService();

    private static final int NULL = 0;
    private static final int DEVELOPER_COST = 1500;
    private static final int WORK_COST = 4000;

    private BillProcessingService() {
    }

    public static BillProcessingService getInstance() {
        return instance;
    }

    public Bill getById(int id) throws ServiceException {
        LOCK.lock();
        try {
            Bill bill = null;

            bill = BILL_DAO.searchByID(id);
            User user = USER_DAO.searchById(bill.getCustomer().getId());
            bill.setCustomer(user);

            return bill;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Bill getByCustomer(User user) throws ServiceException {
        LOCK.lock();
        try {
            Bill bill = null;

            bill = BILL_DAO.searchByCustomerId(user.getId());
            user = USER_DAO.searchById(user.getId());
            bill.setCustomer(user);

            return bill;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Bill> searchAll() throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Bill> bills = null;
            bills = BILL_DAO.searchAll();

            return bills;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Bill> searchAll(User user) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Bill> bills = null;
            bills = BILL_DAO.searchAll(user.getId());

            return bills;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void update(Bill bill) throws ServiceException {
        LOCK.lock();
        try {
            BILL_DAO.update(bill);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void delete(Bill bill) throws ServiceException {
        LOCK.lock();
        try {
            BILL_DAO.delete(bill.getId());
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Bill add(Bill bill) throws ServiceException {
        LOCK.lock();
        try {
            bill = BILL_DAO.add(bill);

            return bill;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Bill calculate(Specification spec) throws ServiceException {
        LOCK.lock();
        try {
            int cost = NULL;
            Bill bill = new Bill();

            bill.setCustomer(spec.getCustomer());

            for (Work work : spec.getWorkList()) {
                cost = work.getDevelopersNumber() * DEVELOPER_COST;
            }

            cost = cost + (spec.getWorkList().size() * WORK_COST);
            bill.setCost(cost);

            return bill;
        } finally {
            LOCK.unlock();
        }
    }
}