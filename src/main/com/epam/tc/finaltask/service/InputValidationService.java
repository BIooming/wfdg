package main.com.epam.tc.finaltask.service;

import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by blooming on 4.4.16.
 */
public class InputValidationService {
    private static final InputValidationService instance = new InputValidationService();
    private static final ReentrantLock LOCK = new ReentrantLock();

    private static final String SCRIPT_START_TAG = "<script";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final int MIN_INPUT_LENGTH = 6;
    public static final int MAX_USERNAME_LENGTH = 16;
    public static final int MAX_NAME_LENGTH = 255;
    public static final int MAX_DESCRIPTION_LENGTH = 510;
    public static final int MAX_PASSWORD_LENGTH = 60;
    public static final int MAX_SECONDARY_INPUT_LENGTH = 45;

    private InputValidationService() {
    }

    public static InputValidationService getInstance() {
        return instance;
    }

    public boolean isValid(String input, int minLength, int maxLength) {
        LOCK.lock();
        try {
            String string = input.trim();
            if (string.isEmpty()) {
                return false;
            }
            if ((string.length() < minLength) && (string.length() > maxLength)) {
                return false;
            }
            if (string.contains(SCRIPT_START_TAG)) {
                return false;
            }
            return true;
        } finally {
            LOCK.unlock();
        }
    }

    public boolean isValidEmail(String string) {
        LOCK.lock();
        try {
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(string);
            return matcher.matches();
        } finally {
            LOCK.unlock();
        }
    }
}
