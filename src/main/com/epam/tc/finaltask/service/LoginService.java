package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.UserDao;
import main.com.epam.tc.finaltask.dal.UserTypeDao;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 25.2.16.
 */
public final class LoginService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final UserDao USER_DAO = DAO_FACTORY.getUserDao();
    private static final UserTypeDao USER_TYPE_DAO = DAO_FACTORY.getUserTypeDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final LoginService instance = new LoginService();
    private static final int NULL = 0;

    private LoginService() {
    }

    public static LoginService getInstance() {
        return instance;
    }

    public User login(String email, String password) throws ServiceException {
        LOCK.lock();
        try {
            User user = null;

            user = USER_DAO.searchByEmail(email);
            if (isValid(user, password)) {
                user.setUserType(USER_TYPE_DAO.searchByUserId(user.getId()));
            } else {
                user = null;
            }

            return user;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    private boolean isValid(User user, String password) {
        boolean isValid = true;
        if (user.getId() < NULL) {
            isValid = false;
        }
        if (isWrongPassword(user, password)) {
            isValid = false;
        }
        return isValid;
    }

    private boolean isWrongPassword(User user, String password) {
        boolean isWrong = true;
        if (password.equals(user.getPassword())) {
            isWrong = false;
        }
        return isWrong;
    }
}