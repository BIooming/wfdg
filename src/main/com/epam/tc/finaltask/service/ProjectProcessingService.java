package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.Bill;
import main.com.epam.tc.finaltask.bean.Project;
import main.com.epam.tc.finaltask.dal.BillDao;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.ProjectDao;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 25.2.16.
 */
public final class ProjectProcessingService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final ProjectDao PROJECT_DAO = DAO_FACTORY.getProjectDao();
    private static final BillDao BILL_DAO = DAO_FACTORY.getBillDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final ProjectProcessingService instance = new ProjectProcessingService();

    private ProjectProcessingService() {
    }

    public static ProjectProcessingService getInstance() {
        return instance;
    }

    public Project getProjectById(int id) throws ServiceException {
        LOCK.lock();
        try {
            Project project = null;

            project = PROJECT_DAO.searchBySpecId(id);

            Bill bill = BILL_DAO.searchByID(project.getBill().getId());
            project.setBill(bill);

            return project;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void saveProjectToDatabase(Project project) throws ServiceException {
        LOCK.lock();
        try {
            PROJECT_DAO.add(project);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void updateProjectInDataBase(Project project) throws ServiceException {
        LOCK.lock();
        try {
            PROJECT_DAO.update(project);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }
}