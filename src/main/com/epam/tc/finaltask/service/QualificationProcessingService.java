package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.QualificationsDao;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 25.2.16.
 */
public final class QualificationProcessingService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final QualificationsDao QUALIFICATIONS_DAO = DAO_FACTORY.getQualificationsDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final QualificationProcessingService instance = new QualificationProcessingService();

    private QualificationProcessingService() {
    }

    public static QualificationProcessingService getInstance() {
        return instance;
    }

    public ArrayList<Qualification> getAll() throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Qualification> qualifications = new ArrayList<>();
            qualifications = QUALIFICATIONS_DAO.searchAll();
            return qualifications;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Qualification getById(int id) throws ServiceException {
        LOCK.lock();
        try {
            Qualification qualification = null;
            qualification = QUALIFICATIONS_DAO.searchById(id);
            return qualification;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void save(Qualification qualification) throws ServiceException {
        LOCK.lock();
        try {
            QUALIFICATIONS_DAO.add(qualification);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void update(Qualification qualification) throws ServiceException {
        LOCK.lock();
        try {
            QUALIFICATIONS_DAO.update(qualification);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void delete(Qualification qualification) throws ServiceException {
        LOCK.lock();
        try {
            QUALIFICATIONS_DAO.delete(qualification.getId());
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void delete(Integer id) throws ServiceException {
        LOCK.lock();
        try {
            QUALIFICATIONS_DAO.delete(id);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }
}