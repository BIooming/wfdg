package main.com.epam.tc.finaltask.service;

import java.io.Serializable;

/**
 * Created by blooming on 24.2.16.
 */
public class ServiceException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;

    public ServiceException() {
        super();
    }

    public ServiceException(Exception e) {
        super(e);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Exception e) {
        super(message, e);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
