package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.dal.*;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 25.2.16.
 */
public final class SpecProcessingService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final SpecDao SPEC_DAO = DAO_FACTORY.getSpecDao();
    private static final ProjectDao PROJECT_DAO = DAO_FACTORY.getProjectDao();
    private static final BillDao BILL_DAO = DAO_FACTORY.getBillDao();
    private static final WorkDao WORK_DAO = DAO_FACTORY.getWorkDao();
    private static final UserDao USER_DAO = DAO_FACTORY.getUserDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final SpecProcessingService instance = new SpecProcessingService();

    private SpecProcessingService() {
    }

    public static SpecProcessingService getInstance() {
        return instance;
    }

    public ArrayList<Specification> getSpecList(User user) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Specification> specificationList = new ArrayList<>();
            specificationList = SPEC_DAO.searchByCustomerId(user.getId());
            for (Specification spec : specificationList) {
                spec.setCustomer(user);
                spec = fillSpecData(spec);
            }
            return specificationList;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Specification> getFullSpecList() throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Specification> specificationList = new ArrayList<>();
            specificationList = SPEC_DAO.searchAll();
            for (Specification spec : specificationList) {
                spec = fillSpecData(spec);
            }
            return specificationList;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Specification addToDataBase(Specification specification) throws ServiceException {
        LOCK.lock();
        try {
            specification = SPEC_DAO.add(specification);
            return specification;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Specification getSpecById(int id) throws ServiceException {
        LOCK.lock();
        try {
            Specification specification = null;
            specification = SPEC_DAO.searchById(id);
            specification = fillSpecData(specification);
            return specification;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void updateInDataBase(Specification specification) throws ServiceException {
        LOCK.lock();
        try {
            SPEC_DAO.update(specification);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void deleteFromDataBase(Specification specification) throws ServiceException {
        LOCK.lock();
        try {
            SPEC_DAO.delete(specification.getId());
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    private Specification fillSpecData(Specification specification) throws DalException {
        Project project = PROJECT_DAO.searchBySpecId(specification.getId());
        if (null != project) {
            Bill bill = BILL_DAO.searchByID(project.getBill().getId());
            if (null != bill) {
                bill.setCustomer(specification.getCustomer());
                project.setBill(bill);
            }
            specification.setProject(project);
        }

        User user = USER_DAO.searchById(specification.getCustomer().getId());
        specification.setCustomer(user);

        ArrayList<Work> workList = WORK_DAO.searchBySpecId(specification.getId());
        specification.setWorkList(workList);

        return specification;
    }
}
