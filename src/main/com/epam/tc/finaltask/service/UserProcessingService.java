package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.dal.*;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 25.2.16.
 */
public final class UserProcessingService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final UserQualificationDao USER_QUALIFICATION_DAO = DAO_FACTORY.getUserQualificationDao();
    private static final UserDao USER_DAO = DAO_FACTORY.getUserDao();
    private static final UserTypeDao TYPE_DAO = DAO_FACTORY.getUserTypeDao();
    private static final QualificationsDao QUALIFICATIONS_DAO = DAO_FACTORY.getQualificationsDao();
    private static final WorkTimeDao WORK_TIME_DAO = DAO_FACTORY.getWorkTimeDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final UserProcessingService instance = new UserProcessingService();
    private static final int NULL = 0;

    private UserProcessingService() {
    }

    public static UserProcessingService getInstance() {
        return instance;
    }

    public User getUserByEmail(String email) throws ServiceException {
        LOCK.lock();
        try {
            User user = null;

            user = USER_DAO.searchByEmail(email);
            UserType type = TYPE_DAO.searchByUserId(user.getId());
            user = setQualification(user);
            user.setUserType(type);

            return user;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public User getUserByName(String name) throws ServiceException {
        LOCK.lock();
        try {
            User user = null;

            user = USER_DAO.searchByName(name);
            UserType type = TYPE_DAO.searchByUserId(user.getId());
            user.setUserType(type);
            user = setQualification(user);

            return user;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public User add(User user) throws ServiceException {
        LOCK.lock();
        try {
            user = USER_DAO.add(user);
            TYPE_DAO.add(user.getId(), user.getUserType());

            if (null != user.getQualification()) {
                Qualification qualification = user.getQualification();
                USER_QUALIFICATION_DAO.add(user.getId(), qualification.getId());
            }

            return user;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public User getUserById(int id) throws ServiceException {
        LOCK.lock();
        try {
            User user = null;

            user = USER_DAO.searchById(id);
            UserType type = TYPE_DAO.searchByUserId(user.getId());
            user.setUserType(type);
            user = setQualification(user);

            return user;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<User> getAllDevs() throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<User> users = new ArrayList<>();

            for (User user : USER_DAO.searchAll()) {
                UserType type = TYPE_DAO.searchByUserId(user.getId());
                user.setUserType(type);
                user = setQualification(user);
                if (type == UserType.DEVELOPER) {
                    users.add(user);
                }
            }

            return users;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<User> getByQualification(Qualification qualification) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<User> userList = new ArrayList<>();

            for (int id : USER_QUALIFICATION_DAO.searchUsersByQualificationId(qualification.getId())) {
                User user = new User();
                user = USER_DAO.searchById(id);
                user = setQualification(user);
                userList.add(user);
            }

            return userList;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<User> getAvailableDevelopers(ArrayList<User> developers) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<User> availableDevelopers = new ArrayList<>();

            for (User developer : developers) {
                boolean isWorksFinished = true;

                ArrayList<Integer> workIdList = WORK_TIME_DAO.searchWorkByUserId(developer.getId());
                for (Integer id : workIdList) {
                    if (!WORK_TIME_DAO.isFinished(id, developer.getId())) {
                        isWorksFinished = false;
                    }
                }
                if (isWorksFinished) {
                    availableDevelopers.add(developer);
                }
            }

            return availableDevelopers;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void update(User user) throws ServiceException {
        LOCK.lock();
        try {
            USER_DAO.update(user);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void delete(User user) throws ServiceException {
        LOCK.lock();
        try {
            USER_DAO.delete(user.getId());
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void delete(int userId) throws ServiceException {
        LOCK.lock();
        try {
            User user = USER_DAO.searchById(userId);
            USER_DAO.delete(user.getId());
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    private User setQualification(User user) throws ServiceException {
        if (user.getId() != NULL) {
            try {
                ArrayList<Integer> qualificationIdList = USER_QUALIFICATION_DAO.searchQualificationsByUserId(user.getId());
                if (!qualificationIdList.isEmpty()) {
                    for (Integer id : qualificationIdList) {
                        Qualification qualification = QUALIFICATIONS_DAO.searchById(id);
                        user.setQualification(qualification);
                    }
                }
            } catch (DalException e) {
                throw new ServiceException(e);
            }
        }
        return user;
    }
}
