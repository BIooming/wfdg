package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 19.3.16.
 */
public final class UserValidationService {
    private static final UserValidationService instance = new UserValidationService();
    private static final ReentrantLock LOCK = new ReentrantLock();

    public static UserValidationService getInstance() {
        return instance;
    }

    private UserValidationService() {
    }

    public boolean isValidUser(User user, UserType type) {
        LOCK.lock();
        try {
            boolean isValid = true;
            if ((null == user) || (!user.getUserType().equals(type))) {
                isValid = false;
            }
            return isValid;
        } finally {
            LOCK.unlock();
        }
    }
}
