package main.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.Work;
import main.com.epam.tc.finaltask.dal.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 26.2.16.
 */
public final class WorkProcessingService {
    private static final DaoFactory DAO_FACTORY = DaoFactory.getDaoFactory();
    private static final WorkDao WORK_DAO = DAO_FACTORY.getWorkDao();
    private static final QualificationsDao QUALIFICATIONS_DAO = DAO_FACTORY.getQualificationsDao();
    private static final WorkTimeDao WORK_TIME_DAO = DAO_FACTORY.getWorkTimeDao();
    private static final UserDao USER_DAO = DAO_FACTORY.getUserDao();

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final WorkProcessingService instance = new WorkProcessingService();
    private static final int NULL = 0;

    private WorkProcessingService() {
    }

    public static WorkProcessingService getInstance() {
        return instance;
    }

    public Work getWorkById(int id) throws ServiceException {
        LOCK.lock();
        try {
            Work work = new Work();

            work = WORK_DAO.searchById(id);
            Qualification qualification = work.getQualification();
            qualification = QUALIFICATIONS_DAO.searchById(qualification.getId());
            work.setQualification(qualification);

            return work;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public Work getCurrentWork(User user) throws ServiceException {
        LOCK.lock();
        try {
            Work work = new Work();

            for (Integer workId : WORK_TIME_DAO.searchWorkByUserId(user.getId())) {
                if (!WORK_TIME_DAO.isFinished(workId, user.getId())) {
                    work = getWorkById(workId);
                }
            }

            return work;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Work> saveInDB(ArrayList<Work> works) throws ServiceException {
        LOCK.lock();
        try {
            for (Work work : works) {
                work = WORK_DAO.add(work);
            }

            return works;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Work> getWorksBySpecId(int id) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Work> works = null;
            works = WORK_DAO.searchBySpecId(id);

            for (Work work : works) {
                Qualification qualification = work.getQualification();
                qualification = QUALIFICATIONS_DAO.searchById(qualification.getId());
                work.setQualification(qualification);

                ArrayList<User> developers = getWorkDevelopers(work.getId());
                work.setDevelopers(developers);
            }

            return works;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Work> getWorksByQualificationId(int id) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Work> works = null;
            works = WORK_DAO.searchByQualificationId(id);

            for (Work work : works) {
                Qualification qualification = work.getQualification();
                qualification = QUALIFICATIONS_DAO.searchById(qualification.getId());
                work.setQualification(qualification);

                ArrayList<User> developers = getWorkDevelopers(id);
                work.setDevelopers(developers);
            }

            return works;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Work> getWorksByUserID(int userId) throws ServiceException {
        LOCK.lock();
        try {
            ArrayList<Work> works = new ArrayList<>();
            HashMap<Integer, Float> timeSpent = new HashMap<>();

            for (Integer workId : WORK_TIME_DAO.searchWorkByUserId(userId)) {
                Work work = WORK_DAO.searchById(workId);

                Qualification qualification = work.getQualification();
                qualification = QUALIFICATIONS_DAO.searchById(qualification.getId());
                work.setQualification(qualification);

                ArrayList<User> developers = getWorkDevelopers(workId);
                work.setDevelopers(developers);
                for (User developer : developers) {
                    timeSpent.put(developer.getId(), WORK_TIME_DAO.searchSpentTime(workId, userId));
                }
                work.setSpentTime(timeSpent);

                works.add(work);
            }

            return works;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public ArrayList<Work> removeWork(ArrayList<Work> works, int id) throws ServiceException {
        LOCK.lock();
        try {
            int index = -1;
            for (Work work : works) {
                if (id == work.getId()) {
                    index = works.indexOf(work);
                }
            }

            if (NULL <= index) {
                Work work = works.get(index);
                WORK_DAO.delete(work.getId());
                works.remove(index);
            }

            return works;
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void submitTime(int idWork, int idUser, Float time) throws ServiceException {
        LOCK.lock();
        try {
            Float currentTime = WORK_TIME_DAO.searchSpentTime(idWork, idUser);
            currentTime = currentTime + time;
            WORK_TIME_DAO.addTime(idUser, idWork, currentTime);
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void updateWorks(ArrayList<Work> works) throws ServiceException {
        LOCK.lock();
        try {
            for (Work work : works) {
                if (NULL == work.getId()) {
                    WORK_DAO.add(work);
                } else {
                    WORK_DAO.update(work);
                    updateRelated(work);
                }
            }
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    public void setFinished(int workId, int userId, boolean isFinished) throws ServiceException {
        LOCK.lock();
        try {
            if (isFinished) {
                WORK_TIME_DAO.setFinished(workId, userId);
            } else {
                WORK_TIME_DAO.setUnfinished(workId, userId);
            }
        } catch (DalException e) {
            throw new ServiceException(e);
        } finally {
            LOCK.unlock();
        }
    }

    private ArrayList<User> getWorkDevelopers(int id) throws ServiceException {
        ArrayList<User> developers = new ArrayList<>();
        try {
            for (Integer devId : WORK_TIME_DAO.searchUserByWorkId(id)) {
                User developer = USER_DAO.searchById(devId);
                developers.add(developer);
            }
        } catch (DalException e) {
            throw new ServiceException(e);
        }
        return developers;
    }

    private void updateRelated(Work work) throws ServiceException {
        try {
            ArrayList<Integer> developers = WORK_TIME_DAO.searchUserByWorkId(work.getId());
            if (developers.isEmpty()) {
                for (User user : work.getDevelopers()) {
                    WORK_TIME_DAO.add(work.getId(), user.getId());
                }
            } else {
                for (Integer id : developers) {
                    if (!work.getDevelopersId().contains(id)) {
                        WORK_TIME_DAO.delete(id, work.getId());
                    }
                }
                for (Integer id : work.getDevelopersId()) {
                    if (!developers.contains(id)) {
                        WORK_TIME_DAO.add(work.getId(), id);
                    }
                }
            }
        } catch (DalException e) {
            throw new ServiceException(e);
        }
    }
}
