package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Bill;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.dal.BillDao;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.UserDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlBillDaoTest {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final BillDao billDao = daoFactory.getBillDao();
    private static final UserDao userDao = daoFactory.getUserDao();

    private static final int NULL = 0;
    private static final int TEST_COST = 1000;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";
    private static final String TEST_UPDATED_STRING = "test update";

    private static User tempUser;

    @Before
    public void createTempData() {
        try {
            tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser.setUserType(UserType.CUSTOMER);
            tempUser = userDao.add(tempUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByID() {
        try {
            Bill expectedBill = addTempBillToDataBase();
            Bill actualBill = billDao.searchByID(expectedBill.getId());
            actualBill.setCustomer(userDao.searchById(actualBill.getCustomer().getId()));

            assertEquals(expectedBill, actualBill);

            clearTempDataFromDataBase(expectedBill);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByCustomerId() {
        try {
            Bill expectedBill = addTempBillToDataBase();
            Bill actualBill = billDao.searchByCustomerId(expectedBill.getCustomer().getId());
            actualBill.setCustomer(userDao.searchById(actualBill.getCustomer().getId()));

            assertEquals(expectedBill, actualBill);

            clearTempDataFromDataBase(expectedBill);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchAll() {
        try {
            ArrayList<Bill> savedBills = addTempBillListToDataBase();
            ArrayList<Bill> actualBills = billDao.searchAll();
            for (Bill bill : actualBills) {
                bill.setCustomer(userDao.searchById(bill.getCustomer().getId()));
            }
            boolean isActualContainSaved = actualBills.containsAll(savedBills);

            assertEquals(true, isActualContainSaved);

            clearTempDataFromDataBase(savedBills);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            Bill bill = addTempBillToDataBase();
            assertNotEquals(NULL, bill.getId());
            clearTempDataFromDataBase(bill);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            Bill expectedBill = addTempBillToDataBase();
            expectedBill.setCost(NULL);
            expectedBill.setDescription(TEST_UPDATED_STRING);
            billDao.update(expectedBill);

            Bill actualBill = billDao.searchByID(expectedBill.getId());
            actualBill.setCustomer(userDao.searchById(actualBill.getCustomer().getId()));

            assertEquals(expectedBill, actualBill);

            clearTempDataFromDataBase(expectedBill);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            Bill expectedBill = addTempBillToDataBase();
            billDao.delete(expectedBill.getId());
            Bill actualBill = billDao.searchByID(expectedBill.getId());

            assertNull(actualBill);

            clearTempDataFromDataBase(expectedBill);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private Bill addTempBillToDataBase() throws DalException {
        Bill tempBill = new Bill();
        tempBill.setCost(TEST_COST);
        tempBill.setDescription(TEST_DESCRIPTION);
        tempBill.setCustomer(tempUser);
        tempBill = billDao.add(tempBill);
        return tempBill;
    }

    private ArrayList<Bill> addTempBillListToDataBase() throws DalException {
        ArrayList<Bill> tempBillList = new ArrayList<>();
        for (int i = NULL; i < 5; i++) {
            Bill tempBill = new Bill();
            tempBill.setCost(TEST_COST);
            tempBill.setDescription(TEST_DESCRIPTION);
            tempBill.setCustomer(tempUser);
            tempBill = billDao.add(tempBill);
            tempBillList.add(tempBill);
        }
        return tempBillList;
    }

    private void clearTempDataFromDataBase(Bill savedBill) throws DalException {
        billDao.delete(savedBill.getId());
    }

    private void clearTempDataFromDataBase(ArrayList<Bill> savedBills) throws DalException {
        for (Bill bill : savedBills) {
            billDao.delete(bill.getId());
        }
    }

    @After
    public void deleteTempUser() {
        try {
            userDao.delete(tempUser.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }
}