package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.dal.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlProjectDaoTest {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final ProjectDao projectDao = daoFactory.getProjectDao();
    private static final BillDao billDao = daoFactory.getBillDao();
    private static final SpecDao specDao = daoFactory.getSpecDao();
    private static final UserDao userDao = daoFactory.getUserDao();

    private static final int NULL = 0;
    private static final int TEST_COST = 1000;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";
    private static final String TEST_UPDATED_STRING = "test update";

    private static Specification tempSpec;
    private static User tempUser;
    private static Bill tempBill;

    @Before
    public void createTempExternalData() {
        try {
            tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser.setUserType(UserType.CUSTOMER);
            tempUser = userDao.add(tempUser);

            tempBill = new Bill();
            tempBill.setCost(TEST_COST);
            tempBill.setDescription(TEST_DESCRIPTION);
            tempBill.setCustomer(tempUser);
            tempBill = billDao.add(tempBill);

            tempSpec = new Specification();
            tempSpec.setCustomer(tempUser);
            tempSpec.setName(TEST_NAME);
            tempSpec.setDescription(TEST_DESCRIPTION);
            specDao.add(tempSpec);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchBySpecId() {
        try {
            Project expectedProject = addTempProjectToDataBase();
            Project actualProject = projectDao.searchBySpecId(tempSpec.getId());
            actualProject.setCustomerName(tempUser.getName());
            actualProject.setBill(tempBill);

            assertEquals(expectedProject, actualProject);

            clearTempDataFromDataBase(expectedProject);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByBillId() {
        try {
            Project expectedProject = addTempProjectToDataBase();
            Project actualProject = projectDao.searchByBillId(tempBill.getId());
            actualProject.setCustomerName(tempUser.getName());
            actualProject.setBill(tempBill);

            assertEquals(expectedProject, actualProject);

            clearTempDataFromDataBase(expectedProject);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchAll() {
        try {
            ArrayList<Project> expectedProjectList = addTempProjectListToDataBase();
            ArrayList<Project> actualProjectList = projectDao.searchAll();
            for (Project project : actualProjectList) {
                project.setCustomerName(tempUser.getName());
                project.setBill(tempBill);
            }
            boolean isActualContainSaved = actualProjectList.containsAll(expectedProjectList);

            assertEquals(true, isActualContainSaved);

            clearTempDataFromDataBase(expectedProjectList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            Project project = addTempProjectToDataBase();
            assertNotEquals(NULL, project.getId());
            clearTempDataFromDataBase(project);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            Project expectedProject = addTempProjectToDataBase();
            expectedProject.setDescription(TEST_UPDATED_STRING);
            expectedProject.setName(TEST_UPDATED_STRING);
            projectDao.update(expectedProject);

            Project actualProject = projectDao.searchBySpecId(tempSpec.getId());
            actualProject.setCustomerName(tempUser.getName());
            actualProject.setBill(tempBill);
            assertEquals(expectedProject, actualProject);

            clearTempDataFromDataBase(expectedProject);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            Project expectedProject = addTempProjectToDataBase();
            projectDao.delete(expectedProject.getId());

            Project actualProject = projectDao.searchBySpecId(tempSpec.getId());

            assertNull(actualProject);

            clearTempDataFromDataBase(expectedProject);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private Project addTempProjectToDataBase() throws DalException {
        Project tempProject = new Project();

        tempProject.setId(tempSpec.getId());
        tempProject.setName(TEST_NAME);
        tempProject.setDescription(TEST_DESCRIPTION);
        tempProject.setBill(tempBill);
        tempProject.setStatus(ProjectStatus.IN_PROGRESS);
        tempProject.setCustomerName(tempUser.getName());
        tempProject = projectDao.add(tempProject);

        return tempProject;
    }

    private ArrayList<Project> addTempProjectListToDataBase() throws DalException {
        ArrayList<Project> tempProjectList = new ArrayList<>();
        Project tempProject;

        for (int i = 0; i < 5; i++) {
            tempProject = new Project();

            Specification tempSpec = new Specification();
            tempSpec.setCustomer(tempUser);
            tempSpec.setName(TEST_NAME);
            tempSpec.setDescription(TEST_DESCRIPTION);
            specDao.add(tempSpec);

            tempProject.setId(tempSpec.getId());
            tempProject.setName(TEST_NAME);
            tempProject.setDescription(TEST_DESCRIPTION);
            tempProject.setBill(tempBill);
            tempProject.setStatus(ProjectStatus.IN_PROGRESS);
            tempProject.setCustomerName(tempUser.getName());
            tempProject = projectDao.add(tempProject);

            tempProjectList.add(tempProject);
        }

        return tempProjectList;
    }

    private void clearTempDataFromDataBase(Project tempProject) throws DalException {
        projectDao.delete(tempProject.getId());
    }

    private void clearTempDataFromDataBase(ArrayList<Project> expectedProjectList) throws DalException {
        for (Project project : expectedProjectList) {
            projectDao.delete(project.getId());
            specDao.delete(project.getId());
        }
    }

    @After
    public void deleteTempExternalData() {
        try {
            specDao.delete(tempSpec.getId());
            userDao.delete(tempSpec.getCustomer().getId());
            billDao.delete(tempBill.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }
}