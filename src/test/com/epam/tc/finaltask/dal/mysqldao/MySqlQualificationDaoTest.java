package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.QualificationsDao;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlQualificationDaoTest {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final QualificationsDao qualificationDao = daoFactory.getQualificationsDao();

    private static final int NULL = 0;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_UPDATED_STRING = "test update";

    @Test
    public void testSearchById() {
        try {
            Qualification expectedQualification = addTempQualificationToDataBase();
            Qualification actualQualification = qualificationDao.searchById(expectedQualification.getId());

            assertEquals(expectedQualification, actualQualification);

            clearTempDataFromDataBase(expectedQualification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetAll() {
        try {
            ArrayList<Qualification> expectedQualifications = addTempQualificationsToDataBase();
            ArrayList<Qualification> actualQualifications = qualificationDao.searchAll();

            boolean isActualContainSaved = actualQualifications.containsAll(expectedQualifications);

            assertEquals(true, isActualContainSaved);

            clearTempDataFromDataBase(expectedQualifications);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            Qualification qualification = addTempQualificationToDataBase();

            assertNotEquals(NULL, qualification.getId());

            clearTempDataFromDataBase(qualification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            Qualification expectedQualification = addTempQualificationToDataBase();
            expectedQualification.setName(TEST_UPDATED_STRING);
            expectedQualification.setDescription(TEST_UPDATED_STRING);
            qualificationDao.update(expectedQualification);

            Qualification actualQualification = qualificationDao.searchById(expectedQualification.getId());

            assertEquals(expectedQualification, actualQualification);

            clearTempDataFromDataBase(expectedQualification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            Qualification expectedQualification = addTempQualificationToDataBase();
            qualificationDao.delete(expectedQualification.getId());
            Qualification actualQualification = qualificationDao.searchById(expectedQualification.getId());

            assertNull(actualQualification);

            clearTempDataFromDataBase(expectedQualification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private Qualification addTempQualificationToDataBase() throws DalException {
        Qualification tempQualification = new Qualification();

        tempQualification.setName(TEST_NAME);
        tempQualification.setDescription(TEST_DESCRIPTION);
        tempQualification = qualificationDao.add(tempQualification);

        return tempQualification;
    }

    private ArrayList<Qualification> addTempQualificationsToDataBase() throws DalException {
        ArrayList<Qualification> tempQualificationList = new ArrayList<>();
        Qualification tempQualification = null;
        for (int i = NULL; i < 5; i++) {
            tempQualification = new Qualification();
            tempQualification.setName(TEST_NAME);
            tempQualification.setDescription(TEST_DESCRIPTION);

            tempQualification = qualificationDao.add(tempQualification);
            tempQualificationList.add(tempQualification);
        }

        return tempQualificationList;
    }

    private void clearTempDataFromDataBase(Qualification tempQualification) throws DalException {
        qualificationDao.delete(tempQualification.getId());
    }

    private void clearTempDataFromDataBase(ArrayList<Qualification> tempQualificationList) throws DalException {
        for (Qualification qualification : tempQualificationList) {
            qualificationDao.delete(qualification.getId());
        }
    }
}