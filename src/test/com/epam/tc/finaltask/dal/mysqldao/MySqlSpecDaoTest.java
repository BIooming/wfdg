package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Specification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.bean.Work;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.SpecDao;
import main.com.epam.tc.finaltask.dal.UserDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlSpecDaoTest {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final SpecDao specDao = daoFactory.getSpecDao();
    private static final UserDao userDao = daoFactory.getUserDao();

    private static final int NULL = 0;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";
    private static final String TEST_UPDATED_STRING = "test update";

    private static User tempUser;

    @Before
    public void createTempData() {
        try {
            tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser.setUserType(UserType.CUSTOMER);
            tempUser = userDao.add(tempUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchById() {
        try {
            Specification expectedSpec = addTempSpecToDataBase();
            Specification actualSpec = specDao.searchById(expectedSpec.getId());
            actualSpec.setCustomer(tempUser);
            actualSpec.setWorkList(new ArrayList<Work>());

            assertEquals(expectedSpec, actualSpec);

            clearTempDataFromDataBase(expectedSpec);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testSearchAll() {
        try {
            ArrayList<Specification> expectedSpecList = addTempSpecListToDataBase();
            ArrayList<Specification> actualSpecList = specDao.searchAll();
            for (Specification specification : actualSpecList) {
                specification.setCustomer(tempUser);
                specification.setWorkList(new ArrayList<Work>());
            }
            boolean isActualContainSaved = actualSpecList.containsAll(expectedSpecList);

            assertEquals(true, isActualContainSaved);

            clearTempDataFromDataBase(expectedSpecList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByCustomerId() {
        try {
            ArrayList<Specification> expectedSpecList = addTempSpecListToDataBase();
            ArrayList<Specification> actualSpecList = specDao.searchByCustomerId(tempUser.getId());
            for (Specification specification : actualSpecList) {
                specification.setCustomer(tempUser);
                specification.setWorkList(new ArrayList<Work>());
            }
            boolean isActualContainSaved = actualSpecList.containsAll(expectedSpecList);

            assertEquals(true, isActualContainSaved);

            clearTempDataFromDataBase(expectedSpecList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            Specification specification = addTempSpecToDataBase();
            assertNotEquals(NULL, specification.getId());
            clearTempDataFromDataBase(specification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            Specification expectedSpec = addTempSpecToDataBase();
            expectedSpec.setName(TEST_UPDATED_STRING);
            expectedSpec.setDescription(TEST_UPDATED_STRING);
            specDao.update(expectedSpec);
            Specification actualSpec = specDao.searchById(expectedSpec.getId());
            actualSpec.setCustomer(tempUser);
            actualSpec.setWorkList(new ArrayList<Work>());

            assertEquals(expectedSpec, actualSpec);

            clearTempDataFromDataBase(expectedSpec);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            Specification expectedSpec = addTempSpecToDataBase();
            specDao.delete(expectedSpec.getId());
            Specification actualSpec = specDao.searchById(expectedSpec.getId());

            assertNull(actualSpec);

            clearTempDataFromDataBase(expectedSpec);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }


    private Specification addTempSpecToDataBase() throws DalException {
        Specification tempSpec = new Specification();
        tempSpec.setName(TEST_NAME);
        tempSpec.setWorkList(new ArrayList<Work>());
        tempSpec.setDescription(TEST_DESCRIPTION);
        tempSpec.setCustomer(tempUser);
        tempSpec = specDao.add(tempSpec);
        return tempSpec;
    }

    private ArrayList<Specification> addTempSpecListToDataBase() throws DalException {
        ArrayList<Specification> tempSpecList = new ArrayList<>();

        for (int i = NULL; i < 5; i++) {
            Specification tempSpec = new Specification();
            tempSpec.setName(TEST_NAME);
            tempSpec.setDescription(TEST_DESCRIPTION);
            tempSpec.setWorkList(new ArrayList<Work>());
            tempSpec.setCustomer(tempUser);
            tempSpec = specDao.add(tempSpec);
            tempSpecList.add(tempSpec);
        }

        return tempSpecList;
    }

    private void clearTempDataFromDataBase(Specification tempSpec) throws DalException {
        specDao.delete(tempSpec.getId());
    }

    private void clearTempDataFromDataBase(ArrayList<Specification> tempSpecList) throws DalException {
        for (Specification specification : tempSpecList) {
            specDao.delete(specification.getId());
        }
    }

    @After
    public void deleteTempUser() {
        try {
            userDao.delete(tempUser.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }
}