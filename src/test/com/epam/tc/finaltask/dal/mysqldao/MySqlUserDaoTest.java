package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.UserDao;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlUserDaoTest {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final UserDao userDao = daoFactory.getUserDao();

    private static final int NULL = 0;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";
    private static final String TEST_UPDATED_STRING = "test update";

    @Test
    public void testSearchById() {
        try {
            User expectedUser = addTempUserToDataBase();
            User actualUser = userDao.searchById(expectedUser.getId());

            assertEquals(expectedUser, actualUser);

            clearTempDataFromDataBase(expectedUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByEmail() {
        try {
            User expectedUser = addTempUserToDataBase();
            User actualUser = userDao.searchByEmail(expectedUser.getEmail());

            assertEquals(expectedUser, actualUser);

            clearTempDataFromDataBase(expectedUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByName() {
        try {
            User expectedUser = addTempUserToDataBase();
            User actualUser = userDao.searchByName(expectedUser.getName());

            assertEquals(expectedUser, actualUser);

            clearTempDataFromDataBase(expectedUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetAll() {
        try {
            ArrayList<User> expectedUsersList = addTempUserListToDataBase();
            ArrayList<User> actualUserList = userDao.searchAll();
            boolean isActualContainSaved = actualUserList.containsAll(expectedUsersList);

            assertTrue(isActualContainSaved);

            clearTempDataFromDataBase(expectedUsersList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            User user = addTempUserToDataBase();
            assertNotEquals(NULL, user.getId());
            clearTempDataFromDataBase(user);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            User expectedUser = addTempUserToDataBase();
            expectedUser.setName(TEST_UPDATED_STRING);
            expectedUser.setEmail(TEST_UPDATED_STRING);
            expectedUser.setPassword(TEST_UPDATED_STRING);
            userDao.update(expectedUser);

            User actualUser = userDao.searchByEmail(expectedUser.getEmail());

            assertEquals(expectedUser, actualUser);

            clearTempDataFromDataBase(expectedUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            User expectedUser = addTempUserToDataBase();
            userDao.delete(expectedUser.getId());
            User actualUser = userDao.searchById(expectedUser.getId());

            assertEquals(NULL, actualUser.getId());

            clearTempDataFromDataBase(expectedUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private User addTempUserToDataBase() throws DalException {
        User tempUser = new User();

        tempUser.setName(TEST_NAME);
        tempUser.setEmail(TEST_STRING);
        tempUser.setPassword(TEST_STRING);
        tempUser = userDao.add(tempUser);

        return tempUser;
    }

    private ArrayList<User> addTempUserListToDataBase() throws DalException {
        ArrayList<User> tempUserList = new ArrayList<>();
        for (int i = NULL; i < 5; i++) {
            User tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser = userDao.add(tempUser);
            tempUserList.add(tempUser);
        }
        return tempUserList;
    }

    private void clearTempDataFromDataBase(User user) throws DalException {
        userDao.delete(user.getId());
    }

    private void clearTempDataFromDataBase(ArrayList<User> userList) throws DalException {
        for (User user : userList) {
            userDao.delete(user.getId());
        }
    }
}