package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.Qualification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.dal.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Hashtable;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlUserQualificationDaoTest {
    private static DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static UserQualificationDao userQualificationDao = daoFactory.getUserQualificationDao();
    private static final UserDao userDao = daoFactory.getUserDao();
    private static final QualificationsDao qualificationDao = daoFactory.getQualificationsDao();
    private static final SpecDao specDao = daoFactory.getSpecDao();

    private static final int NULL = 0;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";
    private static final String TEST_UPDATED_STRING = "test update";
    private static Qualification tempQualification;
    private static User tempUser;

    @Before
    public void createTempData() {
        try {
            tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser.setUserType(UserType.CUSTOMER);
            tempUser = userDao.add(tempUser);

            tempQualification = new Qualification();
            tempQualification.setName(TEST_NAME);
            tempQualification.setDescription(TEST_DESCRIPTION);
            tempQualification = qualificationDao.add(tempQualification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetUserIdList() {
        try {
            ArrayList<Integer> expectedUserIdList = addTempUserId();
            ArrayList<Integer> actualUserIdList =
                    userQualificationDao.searchUsersByQualificationId(tempQualification.getId());

            assertEquals(expectedUserIdList, actualUserIdList);

            clearTempDataFromDataBase(expectedUserIdList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetQualificationIdList() {
        try {
            ArrayList<Integer> expectedIdList = addTempQualificationId();
            ArrayList<Integer> actualIdList =
                    userQualificationDao.searchQualificationsByUserId(tempUser.getId());

            assertEquals(expectedIdList, actualIdList);

            clearTempDataFromDataBase(expectedIdList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testGetAll() {
        try {
            Hashtable<Integer, Integer> expectedIdTable = addTempIdTable();
            Hashtable<Integer, Integer> actualIdTable = userQualificationDao.searchAll();
            boolean isActualContainExpected = false;
            for (Integer userId : actualIdTable.keySet()) {
                if (userId.equals(tempUser.getId())) {
                    isActualContainExpected = true;
                }
            }
            assertEquals(true, isActualContainExpected);

            clearTempDataFromDataBase(expectedIdTable);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            userQualificationDao.add(tempUser.getId(), tempQualification.getId());
            int expectedId = tempUser.getId();
            ArrayList<Integer> actualIdList =
                    userQualificationDao.searchUsersByQualificationId(tempQualification.getId());
            int actualId = actualIdList.get(0);

            assertEquals(expectedId, actualId);

            clearTempDataFromDataBase(expectedId);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDeleteByUserId() {
        try {
            userQualificationDao.add(tempUser.getId(), tempQualification.getId());
            userQualificationDao.deleteByUserId(tempUser.getId());

            ArrayList<Integer> actualIdList =
                    userQualificationDao.searchQualificationsByUserId(tempUser.getId());
            boolean isActualIdListEmpty = actualIdList.isEmpty();

            assertTrue(isActualIdListEmpty);

            clearTempDataFromDataBase(tempUser.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDeleteByQualificationId() {
        try {
            userQualificationDao.add(tempUser.getId(), tempQualification.getId());
            userQualificationDao.deleteByQualificationId(tempQualification.getId());

            ArrayList<Integer> actualIdList =
                    userQualificationDao.searchUsersByQualificationId(tempQualification.getId());
            boolean isActualIdListEmpty = actualIdList.isEmpty();

            assertTrue(isActualIdListEmpty);

            clearTempDataFromDataBase(tempUser.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private Hashtable<Integer, Integer> addTempIdTable() throws DalException {
        Hashtable<Integer, Integer> tempIdTable = new Hashtable<>();
        userQualificationDao.add(tempUser.getId(), tempQualification.getId());
        tempIdTable.put(tempUser.getId(), tempQualification.getId());
        return tempIdTable;
    }

    private ArrayList<Integer> addTempUserId() throws DalException {
        ArrayList<Integer> tempUserId = new ArrayList<>();
        userQualificationDao.add(tempUser.getId(), tempQualification.getId());
        tempUserId.add(tempUser.getId());
        return tempUserId;
    }

    private ArrayList<Integer> addTempQualificationId() throws DalException {
        ArrayList<Integer> tempQualificationId = new ArrayList<>();
        userQualificationDao.add(tempUser.getId(), tempQualification.getId());
        tempQualificationId.add(tempQualification.getId());
        return tempQualificationId;
    }

    private void clearTempDataFromDataBase(ArrayList<Integer> userIdList) throws DalException {
        for (Integer id : userIdList) {
            userQualificationDao.deleteByUserId(id);
        }
    }

    private void clearTempDataFromDataBase(Hashtable<Integer, Integer> tempIdTable) throws DalException {
        for (Integer userId : tempIdTable.keySet()) {
            userQualificationDao.deleteByUserId(userId);
        }
    }

    private void clearTempDataFromDataBase(int tempUserId) throws DalException {
        userQualificationDao.deleteByUserId(tempUserId);
    }

    @After
    public void deleteTempData() {
        try {
            userDao.delete(tempUser.getId());
            qualificationDao.delete(tempQualification.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }
}