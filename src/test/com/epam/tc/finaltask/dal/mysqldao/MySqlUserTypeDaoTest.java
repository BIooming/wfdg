package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.bean.UserType;
import main.com.epam.tc.finaltask.dal.DalException;
import main.com.epam.tc.finaltask.dal.DaoFactory;
import main.com.epam.tc.finaltask.dal.UserDao;
import main.com.epam.tc.finaltask.dal.UserTypeDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Hashtable;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlUserTypeDaoTest {
    private static DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static UserTypeDao userTypeDao = daoFactory.getUserTypeDao();
    private static final UserDao userDao = daoFactory.getUserDao();

    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";

    private static User tempUser;

    @Before
    public void createTempData() {
        try {
            tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser.setUserType(UserType.CUSTOMER);
            tempUser = userDao.add(tempUser);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByUserId() {
        try {
            UserType expectedType = addTempUserType();
            UserType actualType = userTypeDao.searchByUserId(tempUser.getId());

            assertEquals(expectedType, actualType);

            clearTempData();
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchUserIdByType() {
        try {
            addTempUserType();
            int expectedId = tempUser.getId();
            ArrayList<Integer> actualId = userTypeDao.searchUserIdByType(tempUser.getUserType());
            boolean isActualIdContainExpected = actualId.contains(expectedId);

            assertTrue(isActualIdContainExpected);

            clearTempData();
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetAll() {
        try {
            UserType expectedType = addTempUserType();
            Hashtable<Integer, UserType> actualTable = userTypeDao.searchAll();

            boolean isActualTableContainsExpectedType = false;
            for (Integer userId : actualTable.keySet()) {
                if (actualTable.get(userId).equals(expectedType)) {
                    isActualTableContainsExpectedType = true;
                }
            }

            assertTrue(isActualTableContainsExpectedType);

            clearTempData();
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            UserType expectedType = addTempUserType();
            UserType actualType = userTypeDao.searchByUserId(tempUser.getId());

            assertEquals(expectedType, actualType);

            clearTempData();
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            UserType expectedType = addTempUserType();
            userTypeDao.delete(tempUser.getId());
            UserType actualType = userTypeDao.searchByUserId(tempUser.getId());

            assertNull(actualType);

            clearTempData();
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private UserType addTempUserType() throws DalException {
        userTypeDao.add(tempUser.getId(), tempUser.getUserType());
        return tempUser.getUserType();
    }

    private void clearTempData() throws DalException {
        userTypeDao.delete(tempUser.getId());
    }

    @After
    public void deleteTempUser() {
        try {
            userDao.delete(tempUser.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }
}