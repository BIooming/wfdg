package test.com.epam.tc.finaltask.dal.mysqldao;

import main.com.epam.tc.finaltask.bean.*;
import main.com.epam.tc.finaltask.dal.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by blooming on 2.3.16.
 */
public class MySqlWorkDaoTest {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final WorkDao workDao = daoFactory.getWorkDao();
    private static final UserDao userDao = daoFactory.getUserDao();
    private static final QualificationsDao qualificationDao = daoFactory.getQualificationsDao();
    private static final SpecDao specDao = daoFactory.getSpecDao();

    private static final int NULL = 0;
    private static final String TEST_DESCRIPTION = "test description";
    private static final String TEST_NAME = "test name";
    private static final String TEST_STRING = "test string";
    private static final String TEST_UPDATED_STRING = "test update";

    private static User tempUser;
    private static Specification tempSpec;
    private static Qualification tempQualification;

    @Before
    public void createTempData() {
        try {
            tempUser = new User();
            tempUser.setName(TEST_NAME);
            tempUser.setEmail(TEST_STRING);
            tempUser.setPassword(TEST_STRING);
            tempUser.setUserType(UserType.CUSTOMER);
            tempUser = userDao.add(tempUser);

            tempSpec = new Specification();
            tempSpec.setCustomer(tempUser);
            tempSpec.setName(TEST_NAME);
            tempSpec.setDescription(TEST_DESCRIPTION);
            tempSpec = specDao.add(tempSpec);

            tempQualification = new Qualification();
            tempQualification.setName(TEST_NAME);
            tempQualification.setDescription(TEST_DESCRIPTION);
            tempQualification = qualificationDao.add(tempQualification);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchById() {
        try {
            Work expectedWork = addTempWorkToDataBase();
            Work actualWork = workDao.searchById(expectedWork.getId());

            assertEquals(expectedWork.getId(), actualWork.getId());

            clearTempDataFromDataBase(expectedWork);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchByQualificationId() {
        try {
            ArrayList<Work> expectedWorkList = addTempWorkListToDataBase();
            ArrayList<Work> actualWorkList = workDao.searchByQualificationId(tempQualification.getId());

            ArrayList<Integer> expectedWorkIdList = new ArrayList<>();
            for(Work work:expectedWorkList){
                expectedWorkIdList.add(work.getId());
            }
            ArrayList<Integer> actualWorkIdList = new ArrayList<>();
            for(Work work:actualWorkList){
                actualWorkIdList.add(work.getId());
            }

            assertEquals(expectedWorkIdList, actualWorkIdList);

            clearTempDataFromDataBase(expectedWorkList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchBySpecId() {
        try {
            ArrayList<Work> expectedWorkList = addTempWorkListToDataBase();
            ArrayList<Work> actualWorkList = workDao.searchBySpecId(tempSpec.getId());

            ArrayList<Integer> expectedWorkIdList = new ArrayList<>();
            for(Work work:expectedWorkList){
                expectedWorkIdList.add(work.getId());
            }
            ArrayList<Integer> actualWorkIdList = new ArrayList<>();
            for(Work work:actualWorkList){
                actualWorkIdList.add(work.getId());
            }

            assertEquals(expectedWorkIdList, actualWorkIdList);

            clearTempDataFromDataBase(expectedWorkList);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAdd() {
        try {
            Work expectedWork = addTempWorkToDataBase();
            assertNotEquals(NULL, expectedWork.getId());
            clearTempDataFromDataBase(expectedWork);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdate() {
        try {
            Work expectedWork = addTempWorkToDataBase();
            expectedWork.setName(TEST_UPDATED_STRING);
            expectedWork.setDescription(TEST_UPDATED_STRING);
            workDao.update(expectedWork);
            Work actualWork = workDao.searchById(expectedWork.getId());

            assertEquals(expectedWork.getName(), actualWork.getName());

            clearTempDataFromDataBase(expectedWork);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {
            Work expectedWork = addTempWorkToDataBase();
            workDao.delete(expectedWork.getId());
            Work actualWork = workDao.searchById(expectedWork.getId());

            assertNull(actualWork);

            clearTempDataFromDataBase(expectedWork);
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

    private Work addTempWorkToDataBase() throws DalException {
        Work tempWork = new Work();
        tempWork.setName(TEST_NAME);
        tempWork.setDescription(TEST_DESCRIPTION);
        tempWork.setDevelopersNumber(NULL);
        tempWork.setQualification(tempQualification);
        tempWork.setSpecId(tempSpec.getId());
        tempWork = workDao.add(tempWork);
        return tempWork;
    }

    private ArrayList<Work> addTempWorkListToDataBase() throws DalException {
        ArrayList<Work> tempWorkList = new ArrayList<>();
        for (int i = NULL; i < 5; i++) {
            Work tempWork = new Work();
            tempWork.setName(TEST_NAME);
            tempWork.setDescription(TEST_DESCRIPTION);
            tempWork.setDevelopersNumber(NULL);
            tempWork.setQualification(tempQualification);
            tempWork.setSpecId(tempSpec.getId());
            tempWork = workDao.add(tempWork);
            tempWorkList.add(tempWork);
        }
        return tempWorkList;
    }

    private void clearTempDataFromDataBase(Work savedWork) throws DalException {
        workDao.delete(savedWork.getId());
    }

    private void clearTempDataFromDataBase(ArrayList<Work> savedWorks) throws DalException {
        for (Work work : savedWorks) {
            workDao.delete(work.getId());
        }
    }

    @After
    public void deleteTempData() {
        try {
            userDao.delete(tempUser.getId());
            specDao.delete(tempSpec.getId());
            qualificationDao.delete(tempQualification.getId());
        } catch (DalException e) {
            fail(e.getMessage());
        }
    }

}