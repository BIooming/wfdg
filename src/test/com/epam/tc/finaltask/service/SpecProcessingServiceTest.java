package test.com.epam.tc.finaltask.service;

import main.com.epam.tc.finaltask.bean.Specification;
import main.com.epam.tc.finaltask.bean.User;
import main.com.epam.tc.finaltask.service.SpecProcessingService;
import org.junit.Test;

/**
 * Created by blooming on 2.3.16.
 */
public class SpecProcessingServiceTest {
    private Specification spec;

    @Test
    public void testGetSpecList() throws Exception {
        User user = new User();
        user.setId(1);
        System.out.println(SpecProcessingService.getInstance().getSpecList(user));
    }

    @Test
    public void testUpdate() throws Exception {
        spec = new Specification();
        User user = new User();
        user.setId(1);
        spec.setCustomer(user);
        spec.setDescription("update test 2");
        spec.setName("update test 2");
        spec.setId(2);
        SpecProcessingService.getInstance().updateInDataBase(spec);
    }
}