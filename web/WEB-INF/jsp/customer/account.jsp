<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 1.14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content"
                   var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.account.headerSmall"
                 var="txtHeadingSmall"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.username"
                 var="txtUserName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.email"
                 var="txtUserEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.change_pass"
                 var="btnChangePass"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.newPassword"
                 var="txtNewPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.input.confirmPass"
                 var="txtConfirmPass"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.apply" var="btnApply"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longPassword"
                 var="msgLongPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.shortPassword"
                 var="msgShortPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.confirmNotMatch"
                 var="msgPassesNotMatch"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation"
                   var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.account" var="navAccount"/>

    <c:import url="customerhead.jsp" charEncoding="UTF-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="customernavigation.jsp" charEncoding="UTF-8"/>

    <!-- Content -->
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        ${sessionScope.user.name}
                        <small>${txtHeadingSmall}</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i> <a
                                href="Controller?command=GOTO_MAIN"> ${navHome} </a>
                        </li>
                        <li class="active">
                            <i class="fa fa-gear"></i> ${navAccount}
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-user"></span> ${txtUserName}
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            ${sessionScope.user.name}
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-envelope"></span>
                            ${txtUserEmail}
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            ${sessionScope.user.email}
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="fa fa-shield"></span>
                            ${btnChangePass}
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="Controller" method="post" class="form-inline"
                              id="changePasswordForm">
                            <input type="hidden" name="command" value="CHANGE_PASSWORD"/>
                            <div class="form-group">
                                <label for="passInput">
                                    ${txtNewPassword}</label>
                                <input type="password"
                                       class="form-control"
                                       placeholder="${txtNewPassword}" id="passInput">
                                <div id="passError"></div>
                            </div>
                            <div class="form-group">
                                <label for="confirmPassInput">
                                    ${txtConfirmPass}</label>
                                <input type="password"
                                       class="form-control"
                                       placeholder="${txtConfirmPass}"
                                       id="confirmPassInput">
                                <div id="confirmError"></div>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-default btn-block"
                                        onclick="changePassSubmit()">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>
                                    ${btnApply}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<script type="text/javascript" src="js/sha.js"></script>
<!-- Page-Level Script -->
<script>
    function changePassSubmit() {
        var passError = $('#passError');
        var confirmPassError = $('#confirmError');
        var form = $('#changePasswordForm');
        var pass = $('#passInput').val();
        var confirm = $('#confirmPassInput').val();
        passError.html("");
        confirmPassError.html("");
        if (pass.length > 32) {
            passError.append("<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgLongPassword} </div>");
            return false;
        }
        if (pass.length < 6) {
            passError.append("<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgShortPassword} </div>");
            return false;
        }
        if (pass !== confirm) {
            confirmPassError.append("<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgPassesNotMatch} </div>");
            return false;
        }
        passError.html("");
        confirmPassError.html("");
        var shaObj = new jsSHA("SHA-512", "TEXT");
        shaObj.update(pass);
        var hash = shaObj.getHash("HEX");
        form.append("<input type='hidden' name='password' value='" + hash + "'/>");
        form.append("<input type='hidden' name='confirmpassword' value='" + hash + "'/>");
        form.submit();
    }
</script>

</body>

</html>
