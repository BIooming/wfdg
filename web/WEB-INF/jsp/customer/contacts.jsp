<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 12.3.16
  Time: 16.42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content"
                   var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapLong" var="txtCapLong"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapShort" var="txtCapShort"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.contact" var="navContact"/>

    <c:import url="customerhead.jsp" charEncoding="UTF-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="customernavigation.jsp" charEncoding="UTF-8"/>

    <!-- Content -->
    <div id="page-wrapper">

        <!-- Container (Contact Section) -->
        <div id="contact" class="container-fluid bg-light slideanim">
            <h2 class="text-center">${navContact}</h2>
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6" style="text-align: center;">
                    <p>${txtCapShort}.</p>
                    <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
                    <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
                    <p><span class="glyphicon glyphicon-envelope"></span> contact@whiteflame.com
                    </p>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
        <div id="googleMap" style="height:400px;width:100%;"></div>

        <!-- Add Google Maps -->
        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script>
            var myCenter = new google.maps.LatLng(41.878114, -87.629798);

            function initialize() {
                var mapProp = {
                    center: myCenter,
                    zoom: 12,
                    scrollwheel: false,
                    draggable: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                var marker = new google.maps.Marker({
                    position: myCenter,
                });

                marker.setMap(map);
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

    </div>

</div>

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

</body>

</html>

