<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 27.3.16
  Time: 19.58
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
<fmt:message bundle="${navigation}" key="navigation.customer.logo" var="navLogo"/>
<fmt:message bundle="${navigation}" key="navigation.home" var="navHome"/>
<fmt:message bundle="${navigation}" key="navigation.customer.projects" var="navProjects"/>
<fmt:message bundle="${navigation}" key="navigation.customer.purchase" var="navPurchase"/>
<fmt:message bundle="${navigation}" key="navigation.customer.contact" var="navContacts"/>

<fmt:message bundle="${navigation}" key="navigation.profile" var="navProfile"/>
<fmt:message bundle="${navigation}" key="navigation.settings" var="navSettings"/>
<fmt:message bundle="${navigation}" key="navigation.logout" var="navLogOut"/>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><span
                class="glyphicon glyphicon-fire"></span> ${navLogo}</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                    class="fa fa-user"></i> ${sessionScope.user.name} <b
                    class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="Controller?command=GOTO_ACCOUNT"><i
                            class="fa fa-fw fa-user"></i> ${navProfile}</a>
                </li>
                <li>
                    <a href="Controller?command=GOTO_SETTINGS"><i
                            class="fa fa-fw fa-gear"></i> ${navSettings}</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="Controller?command=LOG_OUT"><i
                            class="fa fa-fw fa-power-off"></i> ${navLogOut}</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="Controller?command=GOTO_MAIN"><i
                        class="fa fa-fw fa-home"></i> ${navHome}</a>
            </li>
            <li>
                <a href="Controller?command=GOTO_NEWSPEC"><i
                        class="fa fa-fw fa-edit"></i> ${navPurchase}</a>
            </li>
            <li>
                <a href="Controller?command=GOTO_SPECLIST"><i
                        class="fa fa-fw fa-fire"></i> ${navProjects}</a>
            </li>
            <li>
                <a href="Controller?command=GOTO_CONTACTS"><i
                        class="fa fa-fw fa-info"></i> ${navContacts}</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
