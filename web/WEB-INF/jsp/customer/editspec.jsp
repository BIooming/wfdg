<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 3.3.16
  Time: 1.01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.text.editingSpecHeadingSmall" var="txtHeadingSmall"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.email" var="txtUserEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.text.specificationInfo" var="txtSpecInfo"/>
    <fmt:message bundle="${contentBundle}" key="content.text.specificationName" var="txtSpecName"/>
    <fmt:message bundle="${contentBundle}" key="content.text.specificationDescription" var="txtSpecDescript"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.text.addwork" var="txtWorks"/>
    <fmt:message bundle="${contentBundle}" key="content.text.devsCount" var="txtDevsCount"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.text.workName" var="txtWorkName"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.text.workDescription" var="txtWorkDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualification" var="txtQualification"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.back" var="btnBack"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.add" var="btnAdd"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.remove" var="btnRemove"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.scriptInput" var="msgScriptInput"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.emptyWorkName" var="msgEmptyWorkName"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.emptyWorkDescription" var="msgEmptyWorkDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyName" var="msgEmptySpecName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyDescription" var="msgEmptySpecDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.longWorkName" var="msgLongWorkName"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.longWorkDecsription" var="msgLongWorkDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longDescription" var="msgLongSpecDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longName" var="msgLongSpecName"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.noWorks" var="msgNoWorks"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.manyDevs" var="msgManyDevs"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.fewDevs" var="msgFewDevs"/>


    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.customer.projects" var="navProjects"/>
    <fmt:message bundle="${navBundle}" key="navigation.customer.editproject" var="navEditProject"/>
    <c:import url="customerhead.jsp" charEncoding="UTF-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="customernavigation.jsp" charEncoding="UTF-8"/>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        ${sessionScope.spec.name}
                        <small> ${txtHeadingSmall} </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i> <a
                                href="Controller?command=GOTO_MAIN">${navHome}</a>
                        </li>
                        <li>
                            <i class="fa fa-fire"></i> <a
                                href="Controller?command=GOTO_SPECLIST">${navProjects}</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-pencil-square-o"></i> ${navEditProject}
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="panel-group">
                    <form class="form-horizontal">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3>
                                    ${txtSpecInfo}
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="specName"
                                           class="col-sm-3 control-label">${txtSpecName}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control "
                                               id="specName" name="spec_name"
                                               placeholder="${txtSpecName}"
                                               form="submitSpec"
                                               value="${sessionScope.spec.name}"
                                               required/>
                                        <div id="specNameError"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="specDescription"
                                           class="col-sm-3 control-label">${txtSpecDescript}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="7"
                                                  id="specDescription"
                                                  name="spec_description"
                                                  form="submitSpec"
                                                  placeholder="${txtSpecDescript}"
                                                  required>${sessionScope.spec.description}</textarea>
                                        <div id="specDescriptionError"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3>
                                    ${txtWorks}
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="workName"
                                           class="col-sm-3 control-label">${txtWorkName}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control"
                                               id="workName" name="name"
                                               placeholder="${txtWorkName}"/>
                                        <div id="workNameError"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="workDescription"
                                           class="col-sm-3 control-label">${txtWorkDescription}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="5"
                                                  id="workDescription" name="description"
                                                  placeholder="${txtWorkDescription}"></textarea>
                                        <div id="workDescriptionError"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="qualification"
                                           class="col-sm-3 control-label">${txtQualification}</label>
                                    <div class="col-sm-3">
                                        <select property="qualification"
                                                id="qualification"
                                                class="form-control" name="qualification">
                                            <c:forEach var="qualification"
                                                       items="${sessionScope.qualifications}">
                                                <option value="${qualification.name}">
                                                        ${qualification.name}-${qualification.description}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <label for="countOfDevs"
                                           class="col-sm-3 control-label">${txtDevsCount}</label>
                                    <div class="col-sm-3">
                                        <input type="number" id="countOfDevs"
                                               onkeypress="return numbersonly(event)"
                                               class="form-control numeric"
                                               name="developers"
                                               value="1"/>
                                        <div id="countOfDevsError"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9"></div>
                                    <div class="col-sm-3">
                                        <a onclick="addRow()"
                                           class="btn btn-primary pull-right">${btnAdd}</a>
                                    </div>
                                </div>

                                <table class="table table-striped table-bordered table-hover display select"
                                       id="workTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>${txtWorkName}</th>
                                        <th>${txtWorkDescription}</th>
                                        <th>${txtQualification}</th>
                                        <th>${txtDevsCount}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="work" items="${sessionScope.works}"
                                               varStatus="index">
                                        <input type="hidden" name="index"
                                               value="${index.index}"/>
                                        <tr class="odd gradeX">
                                            <td>${index.count}</td>
                                            <td>${work.name}</td>
                                            <td>${work.description}</td>
                                            <td>${work.qualification.name} - ${work.qualification.description}</td>
                                            <td>${work.developersNumber}</td>
                                            <td>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div id="workTableError"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <form>
                        <input type="hidden" name="command" value="GOTO_SPECLIST">
                        <input type="submit" class="btn btn-danger" value="${btnBack}"/>
                    </form>
                </div>
                <div class="col-sm-6"></div>
                <div class="col-sm-3">
                    <form action="Controller" method="post" id="submitSpec">
                        <input type="hidden" name="command" value="EDIT_SPEC"/>
                        <a onclick="sendSpec()"
                           class="btn btn-success form-control">${btnCreate}</a>
                    </form>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<!-- Page-Level Script -->
<script>
    var i = 1;
    function addRow() {
        var rowCount = document.getElementById("workTable").rows.length;
        var workName = $('#workName').val();
        var workDescription = $('#workDescription').val();
        var qualification = $('#qualification').val();
        var countOfDevelopers = $('#countOfDevs').val();
        if (workName == "") {
            document.getElementById('workNameError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgEmptyWorkName} </div>";
            return false;
        }
        if (workName.length > 255) {
            document.getElementById('workNameError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgLongWorkName} </div>";
            return false;
        }
        if (workName.indexOf("<script") > -1) {
            document.getElementById('workNameError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgScriptInput} </div>";
            return false;
        }
        document.getElementById('workNameError').innerHTML = "";
        if (workDescription == "") {
            document.getElementById('workDescriptionError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgEmptyWorkDescription} </div>";
            return false;
        }
        if (workDescription.length > 510) {
            document.getElementById('workDescriptionError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgLongWorkDescription} </div>";
            return false;
        }
        if (workDescription.indexOf("<script") > -1) {
            document.getElementById('workDescriptionError').innerHTML =
                    "<div class='alert alert-dangersmall' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgScriptInput} </div>";
            return false;
        }
        document.getElementById('workDescriptionError').innerHTML = "";
        if (countOfDevelopers > 40) {
            document.getElementById('countOfDevsError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgManyDevs} </div>";
            return false;
        }
        if (countOfDevelopers < 1) {
            document.getElementById('countOfDevsError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgFewDevs} </div>";
            return false;
        }
        $('#workTable').append('<tr id="addr' + (i) + '" class="odd gradeX"></tr>');
        $('#addr' + i).html('<td>' + (rowCount) + "</td>" +
                "<td><input name='workName" + i + "' type='hidden' value='" + workName + "' form='submitSpec'/>" + workName + "</td>" +
                "<td><input name='workDescription" + i + "' type='hidden' value='" + workDescription + "' form='submitSpec'/>" + workDescription + "</td>" +
                "<td><input name='qualification" + i + "' type='hidden' value='" + qualification + "' form='submitSpec'/>" + qualification + "</td>" +
                "<td><input name='countOfDevelopers" + i + "' type='hidden' value='" + countOfDevelopers + "' form='submitSpec'/>" + countOfDevelopers + "</td>" +
                "<td><button onclick='deleteRow(this)' class='btn btn-danger'><span class='fa-remove fa'></span> ${btnRemove} </button></td>");

        document.getElementById("workName").value = "";
        document.getElementById("workDescription").value = "";
        document.getElementById('countOfDevsError').innerHTML = "";
        document.getElementById('workTableError').innerHTML = "";
        i++;
    }

    function deleteRow(r) {
        var row1 = r.parentNode.parentNode.rowIndex;
        document.getElementById("workTable").deleteRow(row1);
        var table = document.getElementById("workTable");
        for (var j = 1, row; row = table.rows[j]; j++) {
            row.cells[0].innerHTML = (j);
        }
    }

    function sendSpec() {
        var form = document.getElementById("submitSpec");
        var specName = $('#specName').val();
        var specDescription = $('#specDescription').val();
        if (specName == "") {
            document.getElementById('specNameError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgEmptySpecName} </div>";
            return false;
        }
        if (specName.length > 255) {
            document.getElementById('specNameError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgLongSpecName} </div>";
            return false;
        }
        if (specName.indexOf("<script") > -1) {
            document.getElementById('specNameError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgScriptInput} </div>";
            return false;
        }
        document.getElementById('specNameError').innerHTML ="";

        if (specDescription == "") {
            document.getElementById('specDescriptionError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgEmptySpecDescription} </div>";
            return false;
        }
        if (specDescription.length > 510) {
            document.getElementById('specDescriptionError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgLongSpecDescription} </div>";
            return false;
        }
        if (specDescription.indexOf("<script") > -1) {
            document.getElementById('specDescriptionError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgScriptInput} </div>";
            return false;
        }
        document.getElementById('specDescriptionError').innerHTML ="";

        if (document.getElementById('workTable').getElementsByTagName('tr').length < 2) {
            document.getElementById('workTableError').innerHTML =
                    "<div class='alert alert-danger small'  role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgNoWorks} </div>";
            return false;
        } else {
            form.submit();
        }
    }

    function deleteExistingWork(r, workId) {
        if (document.getElementById('workTable').getElementsByTagName('tr').length < 3) {
            document.getElementById('workTableError').innerHTML =
                    "<div class='alert alert-danger small' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> <span class='sr-only'>Error:</span> ${msgNoWorks} </div>";
            return false;
        }

        var row1 = r.parentNode.parentNode.rowIndex;
        document.getElementById("workTable").deleteRow(row1);
        var table = document.getElementById("workTable");
        for (var j = 1, row; row = table.rows[j]; j++) {
            row.cells[0].innerHTML = (j);
        }

        var form = document.getElementById("submitSpec");
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = "workToDelete";
        input.value = workId;
        form.appendChild(input);
    }

    function numbersonly(e) {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;

        keychar = String.fromCharCode(key);

        // control keys
        if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
            return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        else
            return false;
    }
</script>

</body>

</html>
