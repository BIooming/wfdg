<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 17.3.16
  Time: 2.07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.ru" var="btnRu"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.en" var="btnEn"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.apply" var="btnApply"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.settings.PageHeader" var="settingsHeader"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.settings.PageHeaderSmall" var="settingsSmall"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.settings.Locale" var="settingsLocale"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.settings.Language" var="settingsLanguage"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.settings" var="navSettings"/>

    <c:import url="customerhead.jsp" charEncoding="UTF-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="customernavigation.jsp" charEncoding="UTF-8"/>
    <!-- Content -->
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        ${settingsHeader}
                        <small>${settingsSmall}</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i> <a
                                href="Controller?command=GOTO_MAIN"> ${navHome} </a>
                        </li>
                        <li class="active">
                            <i class="fa fa-gear"></i> ${navSettings}
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ${settingsLocale}
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <form method="post" action="Controller"
                                  class="form-horizontal">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <label for="setLocale" class="col-sm-3 control-label">
                                    ${settingsLanguage}:
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="setLocale"
                                            name="locale">
                                        <option value="en">
                                            ${btnEn}
                                        </option>
                                        <option value="ru">
                                            ${btnRu}
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit"
                                            class="btn btn-primary pull-right form-control">
                                        ${btnApply}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

</body>

</html>
