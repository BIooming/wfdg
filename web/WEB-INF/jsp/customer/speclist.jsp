<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 25.2.16
  Time: 17.27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.id" var="txtId"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.text.allProjects" var="txtAllProjcets"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.projectStatus" var="txtProjectStatus"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnEdit"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.delete" var="btnDelete"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.cost" var="txtCost"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.name" var="txtName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.payAttention" var="txtAttention"/>
    <fmt:message bundle="${contentBundle}" key="content.customer.msg.projectCancelingWarning" var="txtCancelWorning"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.name" var="txtName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.id" var="txtId"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.description" var="txtdescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.projects" var="navProjects"/>

    <c:import url="customerhead.jsp" charEncoding="UTF-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="customernavigation.jsp" charEncoding="UTF-8"/>

    <!-- Content -->
    <div id="page-wrapper">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ${txtAllProjcets}
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>  <a href="Controller?command=GOTO_MAIN"> ${navHome} </a>
                    </li>
                    <li class="active">
                        <i class="fa fa-fire"></i> ${navProjects}
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ${txtAllProjcets}
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="alert alert-info">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>${txtAttention}!</strong>
                            ${txtCancelWorning}
                        </div>
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="specTable">
                                <thead>
                                <tr>
                                    <th>${txtId}</th>
                                    <th>${txtName}</th>
                                    <th>${txtdescription}</th>
                                    <th>${txtCost}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="work" items="${requestScope.specifications}">
                                    <tr class="odd gradeX">
                                        <td>${work.id}</td>
                                        <td>${work.name}</td>
                                        <td>${work.description}</td>
                                        <td>
                                            <c:if test="${not empty work.project.bill}">
                                                ${work.project.bill.cost}
                                            </c:if>
                                        </td>
                                        <td>
                                            <form action="/Controller" method="get" class="form-inline">
                                                <input type="hidden" name="command" value="GOTO_EDITSPEC"/>
                                                <input type="hidden" name="id" value="${work.id}"/>
                                                <input type="submit" class="btn btn-primary form-control" value="${btnEdit}"/>
                                            </form>
                                            <c:if test="${empty work.project.status}">
                                                <br>
                                                <form action="/Controller" method="get" class="form-inline">
                                                    <input type="hidden" name="command" value="REFUSE_SPEC"/>
                                                    <input type="hidden" name="id" value="${work.id}"/>
                                                    <input type="submit" class="btn btn-danger form-control" value="${btnDelete}"/>
                                                </form>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<!-- Page-Level scripts -->
<script>
    $(document).ready(function() {
        var locale = '${sessionScope.locale}';
        if(locale == "ru") {
            $('#specTable').DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                }
            });
        } else{
            $('#specTable').DataTable({
                responsive: true
            });
        }
    });
</script>

</body>

</html>