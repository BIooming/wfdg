<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 25.2.16
  Time: 13.29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="wft" uri="/WEB-INF/tld/wftaglib.tld" %>
<!DOCTYPE html>

<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation"
                   var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.language" var="navLanguage"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content"
                   var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.ru"
                 var="btnRu"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.en"
                 var="btnEn"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.logout"
                 var="btnLogOut"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.description"
                 var="txtDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.name" var="txtName"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.timeSpent"
                 var="txtTimespent"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.done"
                 var="txtDone"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.mainHeading"
                 var="txtMain"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.currentWorkHeading"
                 var="txtCurrentWork"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.workHistoryHeadong" var="txtHistory"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.noWorksAssigned" var="txtNoWorks"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.submitTimeHeading" var="txtSubmitTime"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.hours" var="txtHours"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.minutes" var="txtMinutes"/>
    <fmt:message bundle="${contentBundle}" key="content.developer.text.isDone" var="txtDone"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.apply" var="btnApply"/>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/wfbootstrap.css" type="text/css">
    <link rel="stylesheet" href="bower_components/bootstrap/fonts..css.css"
          type="text/css">

    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <title>White Flame</title>
    <link rel="shortcut icon" type="image/x-icon" href="css/img/favicon.ico" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <style>
        .panel {
            border: 1px solid #1b1b1b;
            border-radius: 0 !important;
            transition: box-shadow 0.5s;
            background-color: #ffffff;
        }

        .panel-body {
            background-color: #ffffff;
            color: #1b1b1b;
        }

        .panel:hover {
            box-shadow: 5px 0px 40px rgba(0, 0, 0, .2);
        }

        .panel-footer .btn:hover {
            border: 1px solid #1b1b1b;
            background-color: #fff !important;
            color: #1b1b1b;
        }

        .panel-heading {
            color: #fff !important;
            background-color: #1b1b1b !important;
            padding: 25px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }
        .panel-footer {
            background-color: white !important;
        }
        .panel-footer h3 {
            font-size: 25px;
        }
        .panel-footer h4 {
            color: #ffffff;
            font-size: 14px;
        }

        .panel-footer .btn {
            margin: 15px 0;
            background-color: #1b1b1b;
            color: #fff;
        }
    </style>

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">
                <span class="glyphicon glyphicon-fire"></span>
                ${sessionScope.user.name}
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-left">

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <button class=" btn-lg" type="button" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-cog"></span>
                        ${navLanguage}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <form action="/Controller">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <input type="hidden" name="locale" value="en"/>
                                <input type="submit" class="btn btn-default btn-nav"
                                       value="${btnEn}"/>
                            </form>
                        </li>
                        <li>
                            <form action="/Controller">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <input type="hidden" name="locale" value="ru"/>
                                <input type="submit" class="btn btn-default btn-nav"
                                       value="${btnRu}"/>
                            </form>
                        </li>
                    </ul>
                </li>
                <li>
                    <form action="Controller">
                        <input type="hidden" name="command" value="LOG_OUT"/>
                        <button type="submit" class="btn-lg">
                            <i class="fa fa-fw fa-power-off"></i> ${btnLogOut}
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron text-center">
    <img src="css/img/logo.png" width="250" height="250">
    <h1>White Flame</h1>
    <p>${txtMain}</p>
    <br>
</div>

<div class="row container-fluid bg-dark">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>${txtCurrentWork}</h3>
        </div>
        <div class="panel-body">
            <c:if test="${sessionScope.currentWork.id != 0}">
                <wft:workReport work="${sessionScope.currentWork}">
                    <h3>${txtSubmitTime}</h3><br>
                    <form id="submitTimeForm" class="form-inline" action="Controller">
                        <input type="hidden" name="command" value="SUBMIT_TIME">
                        <input type="hidden" name="id" value="${sessionScope.currentWork.id}">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="hours"> ${txtHours} </label>
                                    <input id="hours" type="number" class="form-control"
                                           onkeypress="return numbersonly(event)"
                                           name="hours" max="999" min="0" value="0" required/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="minutes"> ${txtMinutes} </label>
                                    <input id="minutes" type="number" class="form-control"
                                           onkeypress="return numbersonly(event)"
                                           name="minutes" max="60" min="0" value="0" required/>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="isDone"> ${txtDone} </label>
                                    <input id="isDone" type="checkbox"
                                           class="form-control" name="isDone" value="true"/>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <input type="submit" class="form-control"
                                           value="${btnApply}"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </wft:workReport>
            </c:if>
            <c:if test="${0 == sessionScope.currentWork.id}">
                ${txtNoWorks}
            </c:if>
        </div>
    </div>
</div>

<div class="row container-fluid bg-light">
    <div class="panel panel-default">
        <div class="panel-heading">
            ${txtHistory}
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover"
                   id="workHistory">
                <thead>
                <tr>
                    <th>${txtName}</th>
                    <th>${txtDescription}</th>
                    <th>${txtTimespent}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="work" items="${sessionScope.works}">
                    <tr class="odd gradeX">
                        <td>${work.name}</td>
                        <td>${work.description}</td>
                        <td>${work.spentTime[sessionScope.user.id]}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<footer class="wf-footer" style="background-color: #1b1b1b;height: 90px;padding: 10px;margin: 0;">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
    <p class="text-light">${txtFooter}.</p>
</footer>

<script>
    $(document).ready(function () {

        var locale = '${sessionScope.locale}';
        if (locale == "ru") {
            $('#workHistory').DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                }
            });
        } else {
            $('#workHistory').DataTable({
                responsive: true
            });
        }

        $("#logBtn").click(function () {
            $("#loginModal").modal();
        });
        $("#regBtn").click(function () {
            $("#regModal").modal();
        });

        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        });

        $(window).scroll(function () {
            $(".slideanim").each(function () {
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    });

    function numbersonly(e) {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;

        keychar = String.fromCharCode(key);

        // control keys
        if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
            return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        else
            return false;
    }
</script>

</body>

</html>