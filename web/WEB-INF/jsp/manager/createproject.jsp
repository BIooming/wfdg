<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 2.13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content"
                   var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.text.specificationInfo"
                 var="txtSpecInfo"/>
    <fmt:message bundle="${contentBundle}" key="content.text.specificationName"
                 var="txtSpecName"/>
    <fmt:message bundle="${contentBundle}" key="content.text.specificationDescription"
                 var="txtSpecDescript"/>
    <fmt:message bundle="${contentBundle}" key="content.text.projectInfo"
                 var="txtProjectInfo"/>
    <fmt:message bundle="${contentBundle}" key="content.text.projectName"
                 var="txtProjectName"/>
    <fmt:message bundle="${contentBundle}" key="content.text.projectDescritpion"
                 var="txtProjectDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.text.works" var="txtWorks"/>
    <fmt:message bundle="${contentBundle}" key="content.text.devsCount"
                 var="txtDevsCount"/>
    <fmt:message bundle="${contentBundle}" key="content.text.assignedDevs"
                 var="txtAssignedDevs"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.name" var="txtName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.description"
                 var="txtDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualification"
                 var="txtQualification"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.create"
                 var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.back" var="btnBack"/>

    <fmt:message bundle="${contentBundle}" key="content.common.msg.scriptInput"
                 var="msgScriptInput"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyName"
                 var="msgEmptySpecName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyDescription"
                 var="msgEmptySpecDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longDescription"
                 var="msgLongSpecDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longName"
                 var="msgLongSpecName"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation"
                   var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.projects"
                 var="navProjects"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.createProject"
                 var="navCreateProject"/>

    <title>WF Admin - Bootstrap Admin Template</title>
    <c:import url="managerhead.jsp" charEncoding="utf-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"></c:import>

    <div id="page-wrapper">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Projects
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i> <a
                            href="Controller?command=GOTO_MAIN"> ${navHome} </a>
                    </li>
                    <li>
                        <i class="fa fa-edit"></i> <a
                            href="Controller?command=GOTO_PROJECTS"> ${navProjects} </a>
                    </li>
                    <li class="active">
                        <i class="fa fa-gear"></i> ${navCreateProject}
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="panel-group">
                <form class="form-horizontal" action="Controller" id="createProject"
                      method="post">
                    <input type="hidden" name="command" value="CREATE_PROJECT"/>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>
                                ${txtSpecInfo}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="specName"
                                       class="col-sm-3 control-label">${txtSpecName}</label>
                                <div class="col-sm-9">
                                    <div id="specName">
                                        ${sessionScope.spec.name}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="specDescription"
                                       class="col-sm-3 control-label">${txtSpecDescript}</label>
                                <div class="col-sm-9">
                                    <div id="specDescription">
                                        ${sessionScope.spec.description}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>
                                ${txtProjectInfo}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="projectName"
                                       class="col-sm-3 control-label">${txtProjectName}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control "
                                           id="projectName" name="projectName"
                                           placeholder="${txtProjectName}"
                                           form="createProject"
                                           required/>
                                    <div id="projectNameError"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="projectDescription"
                                       class="col-sm-3 control-label">${txtProjectDescription}</label>
                                <div class="col-sm-9">
                                        <textarea class="form-control" rows="7"
                                                  id="projectDescription"
                                                  name="projectDescription"
                                                  form="createProject"
                                                  placeholder="${txtProjectDescription}"
                                                  required></textarea>
                                    <div id="projectDescriptionError"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>${txtWorks}</h2>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover"
                                   id="workTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>${txtName}</th>
                                    <th>${txtDescription}</th>
                                    <th>${txtQualification}</th>
                                    <th>${txtDevsCount}</th>
                                    <th>${txtAssignedDevs}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="work" items="${sessionScope.works}"
                                           varStatus="index">
                                    <tr class="odd gradeX">
                                        <td>${index.count}</td>
                                        <td id="${work.id}">${work.name}</td>
                                        <td>${work.description}</td>
                                        <td>${work.qualification.name}</td>
                                        <td id="need${index.count}">${work.developersNumber}</td>
                                        <td id="assigned${index.count}"></td>
                                        <td>
                                            <c:forEach var="assigned_dev"
                                                       items="${work.developers}">
                                                <br>
                                                ${assigned_dev.name}
                                                <a class="btn btn-danger"><span
                                                        class="fa fa-remove"></span> </a>
                                            </c:forEach>
                                            <select name="dev" id="select${index.count}">
                                                <c:forEach var="dev" items="${qualMap}">
                                                    <c:if test="${dev.key==work.qualification.id}">
                                                        <c:forEach var="rightDev"
                                                                   items="${dev.value}"
                                                                   varStatus="i">
                                                            <option id="${rightDev.name}">${rightDev.name}</option>
                                                        </c:forEach>
                                                    </c:if>
                                                </c:forEach>
                                            </select><br>
                                        </td>
                                        <td>
                                            <a class="btn btn-success"
                                               onclick="assignDev(${index.count}, ${work.id})">
                                                <span class="fa fa-plus"></span>
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-sm-1">
                <a href="/Controller?command=GOTO_PROJECTS"
                   class="btn btn-danger">
                    <span class="fa fa-arrow-left"></span> ${btnBack}
                </a>
            </div>
            <div class="col-sm-9"></div>
            <div class="col-sm-1">
                <a onclick="createProject()" class="btn btn-success">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    ${btnCreate}
                </a>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    var i = 0;

    $(document).ready(function () {
        var locale = '${sessionScope.locale}';
        if (locale == "ru") {
            $('#workTable').DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                }
            });
        } else {
            $('#workTable').DataTable({
                responsive: true
            });
        }
    });

    function assignDev(rowIndex, workId) {
        var devName = $('#select' + rowIndex).val();
        var need = $('#need' + rowIndex).text();
        var assigned = $('#assigned' + rowIndex + ' > div').length;
        if ((devName != null) && (need > assigned)) {
            $('#assigned' + rowIndex).append("<div id='dev" + i + "'>" +
                    devName +
                    "  <a class='btn btn-danger' onclick='removeDev(" + i + "," + rowIndex + ",\"" + devName + "\")'>" +
                    "<span class='fa fa-close'></span></a>" +
                    "</div>");
            i++;
            $('#createProject').append('<input id="' + devName + i + '" type="hidden" name="work' + i + ':' + workId + '" value="' + devName + '"/>');
            $('#' + devName).remove();
        }
    }

    function removeDev(devIndex, rowIndex, devName) {
        $('#dev' + devIndex).remove();
        $('#' + devIndex + devName).remove();
        $('#select' + rowIndex).append("<option id='" + devName + "'>" + devName + "</option>");
    }

    function createProject() {
        var form = $('#createProject');
        var projectName = $('#projectName').val();
        var projectDescription = $('#projectDescription').val();
        var nameError = $('#projectNameError');
        var descriptionError = $('#projectDescriptionError');
        if (projectName == "") {
            nameError.html("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgEmptySpecName} </div>");
            return false;
        }
        if (projectName.length > 255) {
            nameError.html("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongSpecName} </div>");
            return false;
        }
        if (projectName.indexOf("<script") > -1) {
            nameError.html("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgScriptInput} </div>");
            return false;
        }
        nameError.html("");

        if (projectDescription == "") {
            descriptionError.html("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgEmptySpecDescription} </div>");
            return false;
        }
        if (projectDescription.length > 510) {
            descriptionError.html("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongSpecDescription} </div>");
            return false;
        }
        if (projectDescription.indexOf("<script") > -1) {
            descriptionError.html("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgScriptInput} </div>");
            return false;
        }
        descriptionError.html("");
        form.submit();
    }
</script>

</body>

</html>
