<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 2.13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualificationCreatePageHeading" var="txtHeading"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualificationCreatePageHeadingSmall" var="txtHeadingSmall"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualificationName" var="txtQualificationName"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualificationDescription" var="txtQualificationDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyName" var="msgEmptyName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longName" var="msgLongName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyDescription" var="msgEmptyDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longDescription" var="msgLongDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.scriptInput" var="msgScriptInput"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.create" var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.back" var="btnBack"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.error" var="txtError"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.qualification" var="navQualifications"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.createQualification" var="navCreateQualification"/>

    <title>WF Admin - Bootstrap Admin Template</title>
    <c:import url="managerhead.jsp" charEncoding="utf-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"/>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                    <h1 class="page-header">
                        ${txtHeading} <small>${txtHeadingSmall}</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>  <a href="Controller?command=GOTO_MAIN">${navHome}</a>
                        </li>
                        <li>
                            <i class="fa fa-wrench"></i> <a href="Controller?command=GOTO_QUALIFICATIONS">${navQualifications}</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-pencil-square-o"></i> ${navCreateQualification}
                        </li>
                    </ol>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="Controller" id="createQualification"
                                  method="post">
                                <input type="hidden" name="command" value="CREATE_QUALIFICATION"/>

                                <div class="form-group">
                                    <label for="qualificationName" class="col-sm-3 control-label">${txtQualificationName}</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control "
                                               id="qualificationName" name="name"
                                               placeholder="${txtQualificationName}"
                                               required/>
                                    </div>
                                    <div class="col-sm-3" id="nameError"></div>
                                </div>
                                <div class="form-group">
                                    <label for="qualificationName" class="col-sm-3 control-label">${txtQualificationDescription}</label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control "
                                                  id="qualificationDescription" name="description"
                                                  placeholder="${txtQualificationDescription}"
                                                  required></textarea>
                                    </div>
                                    <div class="col-sm-3" id="descriptionError"></div>
                                </div>
                                <br>
                                <div class="col-sm-3">
                                    <a href="Controller?command=GOTO_QUALIFICATIONS" class="btn btn-danger form-control">
                                        <span class="fa fa-arrow-left"></span> ${btnBack}</a>
                                </div>
                                <div class="col-sm-6"></div>
                                <div class="col-sm-3">
                                    <a onclick="createQualification()" class="btn btn-success form-control">
                                        <span class="fa fa-floppy-o"></span> ${btnCreate}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<script>
    function createQualification() {
        var form = $('#createQualification');
        var qualificationName = $('#qualificationName').val();
        var qualificationDescription = $('#qualificationDescription').val();
        var nameError = $('#nameError');
        var descriptionError = $('#descriptionError');
        if ((qualificationName == "")||(!qualificationName.replace(/\s/g, '').length)) {
            nameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgEmptyName} </div>");
            return false;
        }
        if (qualificationName.length > 45) {
            nameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongName}</div>");
            return false;
        }
        if (qualificationName.indexOf("<script") > -1) {
            nameError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        nameError.html("");

        if ((qualificationDescription == "")||(!qualificationDescription.replace(/\s/g, '').length)) {
            descriptionError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgEmptyDescription}</div>");
            return false;
        }
        if (qualificationDescription.length > 255) {
            descriptionError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongDescription}</div>");
            return false;
        }
        if (qualificationDescription.indexOf("<script") > -1) {
            descriptionError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        descriptionError.html("");

        form.submit();
    }
</script>

</body>

</html>