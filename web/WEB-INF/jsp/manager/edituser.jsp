<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 2.14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Edit user</title>

    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.editDevAccount" var="txtEditAcc"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.email" var="txtEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.username" var="txtUserName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.input.password" var="txtPass"/>
    <fmt:message bundle="${contentBundle}" key="content.common.input.confirmPass" var="txtConfirmPass"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualification" var="txtQualification"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.back" var="btnBack"/>

    <fmt:message bundle="${contentBundle}" key="content.common.msg.longPassword"
                 var="msgLongPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.shortPassword"
                 var="msgShortPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.error" var="txtError"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.confirmNotMatch" var="msgPassesNotMatch"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.invalidEmail" var="msgInvalidEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longEmail" var="msgLongEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.scriptInput" var="msgScriptInput"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyUserName" var="msgEmptyName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longUserName" var="msgLongName"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.developers" var="navDevs"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.editUser" var="navEditDev"/>

    <c:import url="managerhead.jsp" charEncoding="utf-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"/>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <h1 class="page-header">
                    ${requestScope.user.name}
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i> <a
                            href="Controller?command=GOTO_MAIN">${navHome}</a>
                    </li>
                    <li>
                        <i class="fa fa-users"></i> <a
                            href="Controller?command=GOTO_USERS">${navDevs}</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-pencil-square-o">${navEditDev}</i>
                    </li>
                </ol>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="panel-group">
                    <div class="panel-default">
                        <div class="panel-heading">
                            <h2>${txtEditAcc}</h2>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form id="editUser" action="Controller" method="post"
                              class="form-horizontal">
                            <input type="hidden" name="command" value="EDIT_USER"/>
                            <input type="hidden" name="usertype" value="DEVELOPER">
                            <input type="hidden" name="id"
                                   value="${requestScope.user.id}">
                            <div class="form-group">
                                <label for="email" class="control-label col-sm-3">
                                    ${txtEmail}
                                    <span class="fa fa-envelope"></span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="email" name="email" class="form-control"
                                           id="email" value="${requestScope.user.email}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="control-label col-sm-3">
                                    ${txtUserName}
                                    <span class="fa fa-user"></span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" name="username"
                                           class="form-control" id="username"
                                           value="${requestScope.user.name}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="qualification" class="control-label col-sm-3">
                                    ${txtQualification}
                                    <span class="fa fa-gears"></span>
                                </label>
                                <div class="col-sm-6">
                                    <select name="qualification"
                                            class="form-control" id="qualification">
                                        <c:forEach var="qualification"
                                                   items="${sessionScope.qualifications}">
                                            <option value="${qualification.id}">
                                                    ${qualification.name}-${qualification.description}
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="psw" class="col-sm-3 control-label">
                                    ${txtPass}
                                    <span class="fa fa-shield"></span></label>
                                <div class="col-sm-6">
                                    <input type="password" name="password"
                                           class="form-control" id="psw"
                                    placeholder="${txtPass}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirmpsw" class="col-sm-3 control-label">
                                    ${txtConfirmPass}
                                    <span class="fa fa-shield"></span></label>
                                <div class="col-sm-6">
                                    <input type="password" name="confirmpassword"
                                           class="form-control" id="confirmpsw"
                                    placeholder="${txtConfirmPass}">
                                </div>
                            </div>
                            <br>
                            <div class="col-sm-3">
                                <a href="Controller?command=GOTO_USERS"
                                   class="btn btn-danger">
                                    <span class="fa fa-arrow-left"></span>
                                    ${btnBack} </a>
                            </div>
                            <div class="col-sm-5"></div>
                            <div class="col-sm-3">
                                <a onclick="editUser()" class="btn btn-success">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>
                                    ${btnCreate}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>


                ${requestScope.error.message}
            </div>
            <!-- /.row -->


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<script type="text/javascript" src="js/sha.js"></script>
<script>
    function editUser() {
        var form = document.getElementById("createUser");
        var username = $('#username');
        var email = $('#email');
        var pass = $('#password');
        var confirmpass = $('#confirmpass');
        var usernameError = $('#userNameError');
        var emailError = $('#emailError');
        var passError = $('#passwordError');
        var confirmPassError = $('#confirmPassError');

        if (!validateEmail(email)) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgInvalidEmail} </div>");
            return false;
        }
        if (email.length > 16) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongEmail}</div>");
            return false;
        }
        if (email.indexOf("<script") > -1) {
            emailError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        emailError.html("");

        if ((username == "") || (!username.replace(/\s/g, '').length)) {
            usernameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgEmptyName} </div>");
            return false;
        }
        if (username.length > 16) {
            usernameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongName}</div>");
            return false;
        }
        if (username.indexOf("<script") > -1) {
            usernameError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        usernameError.html("");

        if (pass.length > 32) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongPassword} </div>");
            return false;
        }
        if (pass.length < 6) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgShortPassword} </div>");
            return false;
        }
        if (pass !== confirm) {
            confirmPassError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgPassesNotMatch} </div>");
            return false;
        }
        passError.html("");
        confirmPassError.html("");
        var shaObj = new jsSHA("SHA-512", "TEXT");
        shaObj.update(pass);
        var hash = shaObj.getHash("HEX");
        form.append("<input type='hidden' name='password' value='" + hash + "'/>");
        form.append("<input type='hidden' name='confirmpassword' value='" + hash + "'/>");

        form.submit();
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
    }
</script>

</body>

</html>