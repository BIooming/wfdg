<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 25.2.16
  Time: 13.29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>WF Admin - Bootstrap Admin Template</title>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <c:import url="managerhead.jsp" charEncoding="utf-8"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapLong" var="txtCapLong"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapShort" var="txtCapShort"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapLong" var="txtCapLong"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapShort" var="txtCapShort"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.about" var="navAbout"/>
    <fmt:message bundle="${navigation}" key="navigation.services" var="navServices"/>
    <fmt:message bundle="${navigation}" key="navigation.portfolio" var="navPortfolio"/>

    <style>
        .jumbotron {
            padding: 120px 25px;
            margin-bottom: 0;
            font-family: "Nimbus Roman No9 L", sans-serif;
        }
        .container-fluid {
            margin-top: 0px;
            padding: 20px 10px;
            filter: Alpha(opacity=50); /* IE8 and earlier */
        }
        .bg-light {
            background-color: #eee;
        }
        .bg-dark{
            opacity: 0.9;
            color: #f6f6f6;
            background-color: #1b1b1b;
        }
        .logo-small {
            color: #f4511e;
            font-size: 50px;
        }
        .logo {
            padding-top: 40px;
            padding-left: 20px;
            color: #f4511e;
            font-size: 250px;
        }

        input {
            margin: 5px 10px;
            border: 1px solid #1b1b1b;
            border-radius: 3px;
        }

        input:hover {
            border: 1px solid #b8b8b8;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
        }

        table{
            border-spacing: 20px;
            width: 100%;
        }

        th, td {
            padding: 15px;
        }

        td{
            text-align: center;
        }

        textarea {
            max-width: 800px;
            min-width: 600px;
            min-height: 100%;
            border: solid #cccccc;
            border-radius: 7px;
        }

    </style>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"/>

    <!-- Content -->
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Container (About Section) -->
            <div id="about" class="container-fluid bg-dark">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>${navAbout}</h2><br>
                        <h4>${txtCapLong}</h4><br>
                        <p>${txtCapLong} ${txtCapLong} ${txtCapLong}</p>
                        <br>
                    </div>
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-signal logo"></span>
                    </div>
                </div>
            </div>

            <div class="container-fluid bg-light">
                <div class="row">
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-globe logo slideanim"></span>
                    </div>
                    <div class="col-sm-8">
                        <h2>${txtCapShort}</h2><br>
                        <h4>${txtCapLong} ${txtCapLong} ${txtCapLong}</h4><br>
                        <p> ${txtCapLong} ${txtCapLong}</p>
                    </div>
                </div>
            </div>

            <!-- Container (Services Section) -->
            <div id="services" class="container-fluid text-center bg-dark">
                <h2>${navServices}</h2>
                <h4>${txtCapShort}</h4>
                <br>
                <div class="row slideanim">
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-off logo-small"></span>
                        <h4>POWER</h4>
                        <p>${txtCapShort}</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-heart logo-small"></span>
                        <h4>LOVE</h4>
                        <p>${txtCapShort}</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-lock logo-small"></span>
                        <h4>JOB DONE</h4>
                        <p>${txtCapShort}</p>
                    </div>
                </div>
                <br><br>
                <div class="row slideanim">
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-leaf logo-small"></span>
                        <h4>GREEN</h4>
                        <p>${txtCapShort}</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-certificate logo-small"></span>
                        <h4>CERTIFIED</h4>
                        <p>${txtCapShort}</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="glyphicon glyphicon-wrench logo-small"></span>
                        <h4>HARD WORK</h4>
                        <p>${txtCapShort}</p>
                    </div>
                </div>
            </div>

            <!-- Container (Portfolio Section) -->
            <div id="portfolio" class="container-fluid text-center bg-light">
                <h2>${navPortfolio}</h2><br>
                <h4>${txtCapShort}</h4>
                <div class="row text-center slideanim">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <p><strong>Paris</strong></p>
                            <p>${txtCapShort}</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <p><strong>New York</strong></p>
                            <p>${txtCapShort}</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <p><strong>San Francisco</strong></p>
                            <p>${txtCapShort}</p>
                        </div>
                    </div>
                </div>
                <br>

                <h2>${txtCapShort}</h2>
                <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <h4>"${txtCapLong}!"<br><span
                                    style="font-style:normal;">Michael Roe, Vice President, Comment Box</span>
                            </h4>
                        </div>
                        <div class="item">
                            <h4>"${txtCapShort}!"<br><span style="font-style:normal;">John Doe, Salesman, Rep Inc</span>
                            </h4>
                        </div>
                        <div class="item">
                            <h4>"${txtCapShort}?"<br><span
                                    style="font-style:normal;">Chandler Bing, Actor, FriendsAlot</span>
                            </h4>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button"
                       data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button"
                       data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

</body>

</html>