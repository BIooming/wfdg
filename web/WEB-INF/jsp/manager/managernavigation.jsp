<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 25.3.16
  Time: 13.58
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
<fmt:message bundle="${navigation}" key="navigation.manager.logo" var="navLogo"/>
<fmt:message bundle="${navigation}" key="navigation.home" var="navHome"/>
<fmt:message bundle="${navigation}" key="navigation.manager.projects" var="navProjects"/>
<fmt:message bundle="${navigation}" key="navigation.manager.developers" var="navDevelopers"/>
<fmt:message bundle="${navigation}" key="navigation.manager.qualification" var="navQualifications"/>

<fmt:message bundle="${navigation}" key="navigation.profile" var="navProfile"/>
<fmt:message bundle="${navigation}" key="navigation.settings" var="navSettings"/>
<fmt:message bundle="${navigation}" key="navigation.logout" var="navLogOut"/>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-fire"></span> ${navLogo}</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${sessionScope.user.name} <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="Controller?command=GOTO_ACCOUNT"><i class="fa fa-fw fa-user"></i> ${navProfile}</a>
                </li>
                <li>
                    <a href="Controller?command=GOTO_SETTINGS"><i class="fa fa-fw fa-gear"></i> ${navSettings}</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="Controller?command=LOG_OUT"><i class="fa fa-fw fa-power-off"></i> ${navLogOut}</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="Controller?command=GOTO_MAIN"><i class="fa fa-fw fa-home"></i> ${navHome}</a>
            </li>
            <li>
                <a href="Controller?command=GOTO_PROJECTS"><i class="fa fa-fw fa-edit"></i> ${navProjects}</a>
            </li>
            <li>
                <a href="Controller?command=GOTO_USERS"><i class="fa fa-fw fa-users"></i> ${navDevelopers}</a>
            </li>
            <li>
                <a href="Controller?command=GOTO_QUALIFICATIONS"><i class="fa fa-fw fa-wrench"></i> ${navQualifications}</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>