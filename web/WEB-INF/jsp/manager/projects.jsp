<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 2.12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>WF Admin - Bootstrap Admin Template</title>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.id" var="txtId"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.newSpecs" var="txtNewSpecs"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.customer" var="txtCustomer"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.allProjects" var="txtAllProjcets"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.projectStatus" var="txtProjectStatus"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.btn.process" var="btnProcess"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.create" var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnEdit"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.name" var="txtName"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.projects" var="navProjects"/>

    <c:import url="managerhead.jsp" charEncoding="utf-8"></c:import>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"></c:import>

    <div id="page-wrapper">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ${navProjects}
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i> <a
                            href="Controller?command=GOTO_MAIN">${navHome}</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> ${navProjects}
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>${txtNewSpecs}</h2>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover"
                                   id="newSpecTable">
                                <thead>
                                <tr>
                                    <th>${txtId}</th>
                                    <th>${txtName}</th>
                                    <th>${txtCustomer}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                <c:forEach var="spec"
                                           items="${sessionScope.specifications}">
                                    <tr class="odd gradeX">
                                        <td>${spec.id}</td>
                                        <td>${spec.name}</td>
                                        <td>${spec.customer.name}</td>
                                        <td>
                                            <form action="/Controller" method="get">
                                                <input type="hidden" name="command"
                                                       value="GOTO_CREATE_PROJECT"/>
                                                <input type="hidden" name="id"
                                                       value="${spec.id}"/>
                                                <input type="submit" class="btn btn-success" value="${btnProcess}"/>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>${txtAllProjcets}</h2>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover"
                                   id="createdProjectsTable">
                                <thead>
                                <tr>
                                    <th>${txtId}</th>
                                    <th>${txtName}</th>
                                    <th>${txtCustomer}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="work" items="${sessionScope.projects}">
                                    <tr class="odd gradeX">
                                        <td>${work.id}</td>
                                        <td>${work.name}</td>
                                        <td>${work.customerName}</td>
                                        <td>
                                            <form action="/Controller" method="get">
                                                <input type="hidden" name="command"
                                                       value="GOTO_EDIT_PROJECT"/>
                                                <input type="hidden" name="id"
                                                       value="${work.id}"/>
                                                <input type="submit"
                                                       class="btn btn-primary"
                                                       value="${btnEdit}"/>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>


                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>

                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $(document).ready(function() {
            var locale = '${sessionScope.locale}';
            if(locale == "ru") {
                $('#newSpecTable').DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                    }
                });
            } else{
                $('#newSpecTable').DataTable({
                    responsive: true
                });
            }
        });
        $(document).ready(function() {
            var locale = '${sessionScope.locale}';
            if(locale == "ru") {
                $('#createdProjectsTable').DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                    }
                });
            } else{
                $('#createdProjectsTable').DataTable({
                    responsive: true
                });
            }
        });
    });
</script>

</body>

</html>
