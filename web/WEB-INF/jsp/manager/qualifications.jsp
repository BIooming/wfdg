<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 2.13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualificationList" var="txtQualificationList"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.id" var="txtId"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.name" var="txtName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.description" var="txtDescription"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.create" var="btnCreate"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnEdit"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.msg.qualCreated" var="msgCreated"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.msg.qualNotCreated" var="msgNotCreated"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.msg.qualRemoved" var="msgRemoved"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.msg.qualNotDeleted" var="msgNotRemoved"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.qualification" var="navQualifications"/>

    <title>WF Admin - Bootstrap Admin Template</title>
    <c:import url="managerhead.jsp" charEncoding="utf-8"/>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"/>

    <div id="page-wrapper">

        <!-- Page Heading -->
        <div class="row" style="margin-left: 0;margin-right: 0;">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ${navQualifications}
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i> <a
                            href="Controller?command=GOTO_MAIN">${navHome}</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-wrench"></i> ${navQualifications}
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div id="message">

        </div>
        <!-- /.row -->

        <div class="row" style="margin-left: 0;margin-right: 0;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ${txtQualificationList}
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover"
                                   id="qualificationsTable">
                                <thead>
                                <tr>
                                    <th>${txtId}</th>
                                    <th>${txtName}</th>
                                    <th>${txtDescription}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="qualification"
                                           items="${sessionScope.qualifications}">
                                    <tr class="odd gradeX">
                                        <td>${qualification.id}</td>
                                        <td>${qualification.name}</td>
                                        <td>${qualification.description}</td>
                                        <td>
                                            <form action="/Controller" method="post">
                                                <input type="hidden" name="command"
                                                       value="GOTO_EDIT_QUALIFICATION"/>
                                                <input type="hidden" name="id"
                                                       value="${qualification.id}"/>
                                                <input type="submit" class="btn btn-primary form-control" value="${btnEdit}">
                                            </form>
                                        </td>
                                        <td>
                                            <form action="/Controller" method="post">
                                                <input type="hidden" name="command"
                                                       value="DELETE_QUALIFICATION"/>
                                                <input type="hidden" name="id"
                                                       value="${qualification.id}"/>
                                                <button type="submit" class="btn btn-danger">
                                                    <span class="fa fa-remove"></span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>
                            <form action="/Controller" method="get">
                                <input type="hidden" name="command"
                                       value="GOTO_CREATE_QUALIFICATION"/>
                                <input type="submit" class="btn btn-success" value="${btnCreate}">
                            </form>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $(document).ready(function() {
            var locale = '${sessionScope.locale}';
            if(locale == "ru") {
                $('#qualificationsTable').DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                    }
                });
            } else{
                $('#qualificationsTable').DataTable({
                    responsive: true
                });
            }
        });
        var msg = "${requestScope.message}";
        if (msg == "1") {
            $('#message').append("<div class='alert alert-success'> " +
                    "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
                    "${msgCreated}</div>");
        }
        if (msg == "-1") {
            $('#message').append("<div class='alert alert-danger'> ${msgNotCreated}</div>");
        }
        if (msg == "2") {
            $('#message').append("<div class='alert alert-success'> " +
                    "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
                    "${msgRemoved}</div>");
        }
        if (msg == "-2") {
            $('#message').append("<div class='alert alert-danger'> " +
                    "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                    "${msgNotRemoved}</div>");
        }
    });
</script>

</body>

</html>