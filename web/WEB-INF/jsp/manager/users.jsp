<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 26.2.16
  Time: 2.13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>WF Admin - Bootstrap Admin Template</title>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.devList" var="txtDevList"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.id" var="txtId"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.userName" var="txtUserName"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.userEmail" var="txtUserEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.qualification" var="txtQualification"/>
    <fmt:message bundle="${contentBundle}" key="content.manager.text.addDevAccount" var="txtAddDev"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.edit" var="btnEdit"/>

    <fmt:message bundle="${contentBundle}" key="content.common.btn.delete" var="btnDelete"/>

    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navBundle"/>
    <fmt:message bundle="${navBundle}" key="navigation.home" var="navHome"/>
    <fmt:message bundle="${navBundle}" key="navigation.manager.developers" var="navDevs"/>
    <c:import url="managerhead.jsp" charEncoding="utf-8"></c:import>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <c:import url="managernavigation.jsp" charEncoding="utf-8"></c:import>

    <div id="page-wrapper">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ${navDevs}
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>  <a href="Controller?command=GOTO_MAIN">${navHome}</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-users"></i> ${navDevs}
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ${txtDevList}
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="developersTable">
                                <thead>
                                <tr>
                                    <th>${txtId}</th>
                                    <th>${txtUserName}</th>
                                    <th>${txtUserEmail}</th>
                                    <th>${txtQualification}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="dev" items="${sessionScope.devs}">
                                    <tr class="odd gradeX">
                                    <td>${dev.id}</td>
                                    <td>${dev.name}</td>
                                    <td>${dev.email}</td>
                                    <td>${dev.qualification.name}</td>
                                    <td>
                                        <form action="/Controller" method="get">
                                            <input type="hidden" name="command" value="GOTO_EDIT_USER"/>
                                            <input type="hidden" name="id" value="${dev.id}"/>
                                            <input type="submit" class="btn btn-primary" value="${btnEdit}">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="/Controller" method="get">
                                            <input type="hidden" name="command" value="DELETE_USER"/>
                                            <input type="hidden" name="id" value="${dev.id}"/>
                                            <button type="submit" class="btn btn-danger" value="${btnDelete}">
                                                <span class="fa fa-remove"></span>
                                            </button>
                                        </form>
                                    </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <form action="/Controller" method="get">
                                <input type="hidden" name="command" value="GOTO_CREATE_USER"/>
                                <button type="submit" class="btn btn-success">
                                    ${txtAddDev}
                                </button>
                            </form>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<footer class="site-footer">
    <p class="wf-footer">${txtFooter}</p>
</footer>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        var locale = '${sessionScope.locale}';
        if(locale == "ru") {
            $('#developersTable').DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
                }
            });
        } else{
            $('#developersTable').DataTable({
                responsive: true
            });
        }
    });
</script>

</body>

</html>
