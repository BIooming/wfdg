<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 24.2.16
  Time: 0.00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.language" var="navLanguage"/>
    <fmt:message bundle="${navigation}" key="navigation.login" var="navLogin"/>
    <fmt:message bundle="${navigation}" key="navigation.signup" var="navSignUp"/>
    <fmt:message bundle="${navigation}" key="navigation.about" var="navAbout"/>
    <fmt:message bundle="${navigation}" key="navigation.services" var="navServices"/>
    <fmt:message bundle="${navigation}" key="navigation.portfolio" var="navPortfolio"/>
    <fmt:message bundle="${navigation}" key="navigation.contact" var="navContact"/>

    <fmt:message bundle="${contentBundle}" key="content.common.msg.longPassword"
                 var="msgLongPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.shortPassword"
                 var="msgShortPassword"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.error" var="txtError"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.confirmNotMatch" var="msgPassesNotMatch"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.invalidEmail" var="msgInvalidEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longEmail" var="msgLongEmail"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.scriptInput" var="msgScriptInput"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.emptyUserName" var="msgEmptyName"/>
    <fmt:message bundle="${contentBundle}" key="content.common.msg.longUserName" var="msgLongName"/>

    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.ru" var="btnRu"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.en" var="btnEn"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text" var="txtFooter"/>

    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapLong" var="txtCapLong"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.textCapShort" var="txtCapShort"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/wfbootstrap.css" type="text/css">
    <link rel="stylesheet" href="bower_components/bootstrap/fonts..css.css" type="text/css">

    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <title>White Flame</title>
    <link rel="shortcut icon" type="image/x-icon" href="css/img/favicon.ico" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">
                <span class="glyphicon glyphicon-fire"></span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="#about">${navAbout}</a></li>
                <li><a href="#services">${navServices}</a></li>
                <li><a href="#portfolio">${navPortfolio}</a></li>
                <li><a href="#contact">${navContact}</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <button class="dropdown-default btn-lg" type="button" id="regBtn">
                        <span class="glyphicon glyphicon-user"></span>
                        ${navSignUp}
                    </button>
                </li>
                <li>
                    <button class="dropdown-default btn-lg" type="button" id="logBtn">
                        <span class="glyphicon glyphicon-log-in"></span>
                        ${navLogin}
                    </button>
                </li>
                <li>
                    <button class="dropdown-toggle btn-lg" type="button"
                            data-toggle="dropdown">
                        <span class="glyphicon glyphicon-cog"></span> ${navLanguage}<span
                            class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>
                            <form action="/Controller">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <input type="hidden" name="locale" value="en"/>
                                <input type="submit" class="btn btn-default btn-nav" value="${btnEn}"/>
                            </form>
                        </li>
                        <li>
                            <form action="/Controller">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <input type="hidden" name="locale" value="ru"/>
                                <input type="submit" class="btn btn-default btn-nav" value="${btnRu}"/>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron text-center">
    <img src="css/img/logo.png" width="250" height="250">
    <h1>White Flame</h1>
    <p>development group</p>
    <br>
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid bg-dark">
    <div class="row">
        <div class="col-sm-8">
            <h2>${navAbout}</h2><br>
            <h4>${txtCapLong}</h4><br>
            <p>${txtCapLong} ${txtCapLong} ${txtCapLong}</p>
            <br>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-signal logo"></span>
        </div>
    </div>
</div>

<div class="container-fluid bg-light">
    <div class="row">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-globe logo slideanim"></span>
        </div>
        <div class="col-sm-8">
            <h2>${txtCapShort}</h2><br>
            <h4>${txtCapLong} ${txtCapLong} ${txtCapLong}</h4><br>
            <p> ${txtCapLong} ${txtCapLong}</p>
        </div>
    </div>
</div>

<!-- Container (Services Section) -->
<div id="services" class="container-fluid text-center bg-dark">
    <h2>${navServices}</h2>
    <h4>${txtCapShort}</h4>
    <br>
    <div class="row slideanim">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-off logo-small"></span>
            <h4>POWER</h4>
            <p>${txtCapShort}</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-heart logo-small"></span>
            <h4>LOVE</h4>
            <p>${txtCapShort}</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-lock logo-small"></span>
            <h4>JOB DONE</h4>
            <p>${txtCapShort}</p>
        </div>
    </div>
    <br><br>
    <div class="row slideanim">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-leaf logo-small"></span>
            <h4>GREEN</h4>
            <p>${txtCapShort}</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-certificate logo-small"></span>
            <h4>CERTIFIED</h4>
            <p>${txtCapShort}</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-wrench logo-small"></span>
            <h4>HARD WORK</h4>
            <p>${txtCapShort}</p>
        </div>
    </div>
</div>

<!-- Container (Portfolio Section) -->
<div id="portfolio" class="container-fluid text-center bg-light">
    <h2>${navPortfolio}</h2><br>
    <h4>${txtCapShort}</h4>
    <div class="row text-center slideanim">
        <div class="col-sm-4">
            <div class="thumbnail">
                <p><strong>Paris</strong></p>
                <p>${txtCapShort}</p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="thumbnail">
                <p><strong>New York</strong></p>
                <p>${txtCapShort}</p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="thumbnail">
                <p><strong>San Francisco</strong></p>
                <p>${txtCapShort}</p>
            </div>
        </div>
    </div>
    <br>

    <h2>${txtCapShort}</h2>
    <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <h4>"${txtCapLong}!"<br><span
                        style="font-style:normal;">Michael Roe, Vice President, Comment Box</span>
                </h4>
            </div>
            <div class="item">
                <h4>"${txtCapShort}!"<br><span style="font-style:normal;">John Doe, Salesman, Rep Inc</span>
                </h4>
            </div>
            <div class="item">
                <h4>"${txtCapShort}?"<br><span
                        style="font-style:normal;">Chandler Bing, Actor, FriendsAlot</span>
                </h4>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button"
           data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button"
           data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-light">
    <h2 class="text-center">${navContact}</h2>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" style="text-align: center;">
            <p>${txtCapShort}.</p>
            <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
            <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
            <p><span class="glyphicon glyphicon-envelope"></span> contact@whiteflame.com
            </p>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>

<div id="googleMap" style="height:400px;width:100%;margin-bottom: 120px;"></div>

<!-- Add Google Maps -->
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    var myCenter = new google.maps.LatLng(41.878114, -87.629798);

    function initialize() {
        var mapProp = {
            center: myCenter,
            zoom: 12,
            scrollwheel: false,
            draggable: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        var marker = new google.maps.Marker({
            position: myCenter,
        });

        marker.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<footer class="wf-footer" style="background-color: #1b1b1b;height: 120px;padding: 10px;margin: 0;">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
    <p class="text-light">${txtFooter}.</p>
</footer>

<!-- Login modal -->
<div class="modal fade" id="loginModal" role="dialog" style="margin-top: 50px;">
    <div class="modal-dialog">
        <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="commonBundle"/>
        <fmt:message key="content.common.text.email" bundle="${commonBundle}" var="txtEmail"/>
        <fmt:message key="content.common.input.password" bundle="${commonBundle}" var="txtPassword"/>
        <fmt:message key="content.common.input.emailPlaceholder" bundle="${commonBundle}" var="txtEmailPlaceholder"/>
        <fmt:message key="content.common.btn.login" bundle="${commonBundle}" var="btnLogin"/>
        <fmt:message key="content.common.btn.cancel" bundle="${commonBundle}" var="btnCancel"/>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="padding:15px 30px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span> ${navLogin} </h4>
            </div>
            <div class="modal-body" style="padding:40px 50px;">
                <form role="form" action="Controller" method="post" id="loginForm">
                    <input type="hidden" name="command" value="LOGIN"/>
                    <div class="form-group">
                        <label for="username"><span
                                class="fa fa-fw fa-user"></span> ${txtEmail}</label>
                        <input type="email" class="form-control" id="username" name="email"
                               placeholder="${txtEmailPlaceholder}">
                        <div id="loginEmailError"></div>
                    </div>
                    <div class="form-group">
                        <label for="psw"><span
                                class="fa fa-fw fa-shield"></span>
                            ${txtPassword}</label>
                        <input type="password" class="form-control" id="psw"
                               placeholder="${txtPassword}">
                        <div id="loginPassError"></div>
                    </div>
                </form>
                <div class="form-group">
                    <button onclick="submitLogin()" class="btn btn-default form-control">
                        <span class="fa fa-floppy-o"></span>
                        ${btnLogin}
                    </button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default pull-left"
                        data-dismiss="modal"><span
                        class="fa fa-fw fa-remove"></span> ${btnCancel}
                </button>
            </div>
        </div>

    </div>
</div>

<!-- Register modal -->
<div class="modal fade" id="regModal" role="dialog" style="margin-top: 50px;">
    <div class="modal-dialog">
        <fmt:message key="content.common.input.confirmPass" bundle="${commonBundle}" var="txtConfirmPass"/>
        <fmt:message key="content.common.btn.apply" bundle="${commonBundle}" var="btnApply"/>
        <fmt:message key="content.common.text.username" bundle="${commonBundle}" var="txtUserName"/>
        <fmt:message key="content.common.text.registerDialogHeader" bundle="${commonBundle}" var="txtRegisterHeader"/>

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="padding:15px 30px;">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span>${txtRegisterHeader}</h4>
            </div>
            <div class="modal-body" style="padding:40px 50px;">
                    <form id="register" action="Controller" method="post">
                        <input type="hidden" name="command" value="CREATE_USER"/>
                        <input type="hidden" name="id" value="${requestScope.user.id}">
                        <div class="form-group">
                            <label for="newUserEmail">
                                <span class="fa fa-envelope"></span>
                                ${txtEmail}
                            </label>
                            <input type="email" name="email" class="form-control" placeholder="${txtEmailPlaceholder}"
                                   id="newUserEmail" value="${requestScope.user.email}">
                            <div id="emailError"></div>
                        </div>
                        <div class="form-group">
                            <label for="newUsername">
                                <span class="fa fa-user"></span>
                                ${txtUserName}
                            </label>
                            <input type="text" name="username" class="form-control" placeholder="${txtUserName}"
                                   id="newUsername" value="${requestScope.user.name}">
                            <div id="userNameError"></div>
                        </div>
                        <div class="form-group">
                            <label for="newPsw"><span class="fa fa-shield"></span>
                                ${txtPassword}</label>
                            <input type="password" class="form-control" placeholder="${txtPassword}"
                                   id="newPsw" value="${requestScope.user.password}">
                            <div id="newPassError"></div>
                        </div>
                        <div class="form-group">
                            <label for="confirmpsw"><span class="fa fa-shield"></span>
                                ${txtConfirmPass}</label>
                            <input type="password"
                                   class="form-control" id="confirmpsw" placeholder="${txtConfirmPass}"
                                   value="${requestScope.user.password}">
                            <div id="confirmPassError"></div>
                        </div>
                    </form>
                <button onclick="createUser()" class="btn btn-default form-control">
                    <span class="fa fa-floppy-o"></span>
                    ${btnApply}
                </button>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default pull-left"
                        data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span> ${btnCancel}
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="js/sha.js"></script>
<script>
    $(document).ready(function () {
        $("#logBtn").click(function () {
            $("#loginModal").modal();
        });
        $("#regBtn").click(function () {
            $("#regModal").modal();
        });

        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        });

        $(window).scroll(function () {
            $(".slideanim").each(function () {
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    });
</script>
<script type="text/javascript" src="js/sha.js"></script>
<script>
    function createUser() {
        var form = $('#register');
        var username = $('#newUsername').val();
        var email = $('#newUserEmail').val();
        var pass = $('#newPsw').val();
        var confirm= $('#confirmpsw').val();
        var usernameError = $('#userNameError');
        var emailError = $('#emailError');
        var passError = $('#newPassError');
        var confirmPassError = $('#confirmPassError');

        if (!validateEmail(email)) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgInvalidEmail} </div>");
            return false;
        }
        if (email.length > 255) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongEmail}</div>");
            return false;
        }
        if (email.indexOf("<script") > -1) {
            emailError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        emailError.html("");

        if ((username == "") || (!username.replace(/\s/g, '').length)) {
            usernameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgEmptyName} </div>");
            return false;
        }
        if (username.length > 16) {
            usernameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongName}</div>");
            return false;
        }
        if (username.indexOf("<script") > -1) {
            usernameError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        usernameError.html("");

        if (pass.length > 32) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongPassword} </div>");
            return false;
        }
        if (pass.length < 6) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgShortPassword} </div>");
            return false;
        }
        passError.html("");
        if (pass !== confirm) {
            confirmPassError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgPassesNotMatch} </div>");
            return false;
        }
        confirmPassError.html("");
        var shaObj = new jsSHA("SHA-1", "TEXT");
        shaObj.update(pass);
        var hash = shaObj.getHash("HEX");
        form.append("<input type='hidden' name='password' value='" + hash + "'/>");
        form.append("<input type='hidden' name='confirmpassword' value='" + hash + "'/>");

        form.submit();
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
    }

    function submitLogin(){
        var form = $('#loginForm');
        var email = $('#username').val();
        var emailError = $('#loginEmailError');
        var pass = $('#psw').val();
        var passError = $('#loginPassError');


        if (!validateEmail(email)) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgInvalidEmail} </div>");
            return false;
        }
        if (email.length > 255) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongEmail}</div>");
            return false;
        }
        if (email.indexOf("<script") > -1) {
            emailError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        emailError.html("");

        if (pass.length > 32) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongPassword} </div>");
            return false;
        }
        if (pass.length < 6) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgShortPassword} </div>");
            return false;
        }
        passError.html("");
        var shaObj = new jsSHA("SHA-1", "TEXT");
        shaObj.update(pass);
        var hash = shaObj.getHash("HEX");
        form.append("<input type='hidden' name='password' value='" + hash + "'/>");
        form.submit();
    }
</script>

</body>

</html>