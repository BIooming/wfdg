<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 19.3.16
  Time: 4.45
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="commonBundle"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.language" var="navLanguage"/>
    <fmt:message bundle="${navigation}" key="navigation.login" var="navLogin"/>
    <fmt:message key="content.common.text.email" bundle="${commonBundle}" var="txtEmail"/>
    <fmt:message key="content.common.input.password" bundle="${commonBundle}" var="txtPassword"/>
    <fmt:message key="content.common.input.emailPlaceholder" bundle="${commonBundle}" var="txtEmailPlaceholder"/>
    <fmt:message key="content.common.btn.login" bundle="${commonBundle}" var="btnLogin"/>
    <fmt:message key="content.common.btn.cancel" bundle="${commonBundle}" var="btnCancel"/>
    <fmt:message key="content.common.input.confirmPass" bundle="${commonBundle}" var="txtConfirmPass"/>
    <fmt:message key="content.common.btn.apply" bundle="${commonBundle}" var="btnApply"/>
    <fmt:message key="content.common.text.username" bundle="${commonBundle}" var="txtUserName"/>
    <fmt:message key="content.common.text.registerDialogHeader" bundle="${commonBundle}" var="txtRegisterHeader"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.longPassword"
                 var="msgLongPassword"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.shortPassword"
                 var="msgShortPassword"/>
    <fmt:message bundle="${commonBundle}" key="content.common.text.error" var="txtError"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.confirmNotMatch" var="msgPassesNotMatch"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.invalidEmail" var="msgInvalidEmail"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.longEmail" var="msgLongEmail"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.scriptInput" var="msgScriptInput"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.emptyUserName" var="msgEmptyName"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.longUserName" var="msgLongName"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/wfbootstrap.css" type="text/css">
    <title>White Flame</title>
    <link rel="shortcut icon" type="image/x-icon" href="css/img/favicon.ico" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body id="myPage" data-target=".navbar" data-offset="60">

<div class="modal fade" id="loginModal" style="margin-top: 50px;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="padding:15px 30px;">
                <h4><span class="glyphicon glyphicon-lock"></span> ${navLogin}</h4>
            </div>
            <div class="modal-body" style="padding:40px 50px;">
                <form role="form" action="Controller" method="post" id="loginForm">
                    <input type="hidden" name="command" value="LOGIN"/>
                    <div class="form-group">
                        <label for="username"><span
                                class="fa fa-fw fa-user"></span> ${txtEmail}</label>
                        <input type="email" class="form-control" id="username" name="email"
                               placeholder="${txtEmailPlaceholder}">
                        <div id="loginEmailError"></div>
                    </div>
                    <div class="form-group">
                        <label for="psw"><span
                                class="fa fa-fw fa-shield"></span>
                            ${txtPassword}</label>
                        <input type="password" class="form-control" id="psw"
                               placeholder="${txtPassword}">
                        <div id="loginPassError"></div>
                    </div>
                </form>
                <div class="form-group">
                    <button onclick="submitLogin()" class="btn btn-default form-control">
                        <span class="fa fa-floppy-o"></span>
                        ${btnLogin}
                    </button>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-9"></div>
                <div class="col-sm-3"><a href="index.jsp" class="btn btn-default form-control"><span class="fa fa-home"></span>${btnCancel}</a></div>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
            $("#loginModal").modal();

        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        });

        $(window).scroll(function () {
            $(".slideanim").each(function () {
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    });



    function submitLogin(){
        var form = $('#loginForm');
        var email = $('#username').val();
        var emailError = $('#loginEmailError');
        var pass = $('#psw').val();
        var passError = $('#loginPassError');


        if (!validateEmail(email)) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgInvalidEmail} </div>");
            return false;
        }
        if (email.length > 255) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongEmail}</div>");
            return false;
        }
        if (email.indexOf("<script") > -1) {
            emailError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        emailError.html("");

        if (pass.length > 32) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongPassword} </div>");
            return false;
        }
        if (pass.length < 6) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgShortPassword} </div>");
            return false;
        }
        passError.html("");
        var shaObj = new jsSHA("SHA-1", "TEXT");
        shaObj.update(pass);
        var hash = shaObj.getHash("HEX");
        form.append("<input type='hidden' name='password' value='" + hash + "'/>");
        form.submit();
    }
</script>

</body>

</html>
