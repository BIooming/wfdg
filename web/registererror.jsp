<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 3.4.16
  Time: 23.33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="commonBundle"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.language" var="navLanguage"/>
    <fmt:message bundle="${navigation}" key="navigation.login" var="navLogin"/>
    <fmt:message key="content.common.text.email" bundle="${commonBundle}" var="txtEmail"/>
    <fmt:message key="content.common.input.password" bundle="${commonBundle}" var="txtPassword"/>
    <fmt:message key="content.common.input.emailPlaceholder" bundle="${commonBundle}" var="txtEmailPlaceholder"/>
    <fmt:message key="content.common.btn.login" bundle="${commonBundle}" var="btnLogin"/>
    <fmt:message key="content.common.btn.cancel" bundle="${commonBundle}" var="btnCancel"/>
    <fmt:message key="content.common.input.confirmPass" bundle="${commonBundle}" var="txtConfirmPass"/>
    <fmt:message key="content.common.btn.apply" bundle="${commonBundle}" var="btnApply"/>
    <fmt:message key="content.common.text.username" bundle="${commonBundle}" var="txtUserName"/>
    <fmt:message key="content.common.text.registerDialogHeader" bundle="${commonBundle}" var="txtRegisterHeader"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.longPassword"
                 var="msgLongPassword"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.shortPassword"
                 var="msgShortPassword"/>
    <fmt:message bundle="${commonBundle}" key="content.common.text.error" var="txtError"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.confirmNotMatch" var="msgPassesNotMatch"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.invalidEmail" var="msgInvalidEmail"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.longEmail" var="msgLongEmail"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.scriptInput" var="msgScriptInput"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.emptyUserName" var="msgEmptyName"/>
    <fmt:message bundle="${commonBundle}" key="content.common.msg.longUserName" var="msgLongName"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/wfbootstrap.css" type="text/css">
    <title>White Flame</title>
    <link rel="shortcut icon" type="image/x-icon" href="css/img/favicon.ico" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body id="myPage" data-target=".navbar" data-offset="60">

<!-- Register modal -->
<div class="modal fade" id="regModal" role="dialog" style="margin-top: 50px;">
    <div class="modal-dialog">
        <fmt:message key="content.common.input.confirmPass" bundle="${commonBundle}" var="txtConfirmPass"/>
        <fmt:message key="content.common.btn.apply" bundle="${commonBundle}" var="btnApply"/>
        <fmt:message key="content.common.text.username" bundle="${commonBundle}" var="txtUserName"/>
        <fmt:message key="content.common.text.registerDialogHeader" bundle="${commonBundle}" var="txtRegisterHeader"/>

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="padding:15px 30px;">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span>${txtRegisterHeader}</h4>
            </div>
            <div class="modal-body" style="padding:40px 50px;">
                <form id="register" action="Controller" method="post">
                    <input type="hidden" name="command" value="CREATE_USER"/>
                    <input type="hidden" name="id" value="${requestScope.user.id}">
                    <div class="form-group">
                        <label for="newUserEmail">
                            <span class="fa fa-envelope"></span>
                            ${txtEmail}
                        </label>
                        <input type="email" name="email" class="form-control" placeholder="${txtEmailPlaceholder}"
                               id="newUserEmail" value="${requestScope.user.email}">
                        <div id="emailError"></div>
                    </div>
                    <div class="form-group">
                        <label for="newUsername">
                            <span class="fa fa-user"></span>
                            ${txtUserName}
                        </label>
                        <input type="text" name="username" class="form-control" placeholder="${txtUserName}"
                               id="newUsername" value="${requestScope.user.name}">
                        <div id="userNameError"></div>
                    </div>
                    <div class="form-group">
                        <label for="newPsw"><span class="fa fa-shield"></span>
                            ${txtPassword}</label>
                        <input type="password" class="form-control" placeholder="${txtPassword}"
                               id="newPsw" value="${requestScope.user.password}">
                        <div id="newPassError"></div>
                    </div>
                    <div class="form-group">
                        <label for="confirmpsw"><span class="fa fa-shield"></span>
                            ${txtConfirmPass}</label>
                        <input type="password"
                               class="form-control" id="confirmpsw" placeholder="${txtConfirmPass}"
                               value="${requestScope.user.password}">
                        <div id="confirmPassError"></div>
                    </div>
                </form>
                <button onclick="createUser()" class="btn btn-default form-control">
                    <span class="fa fa-floppy-o"></span>
                    ${btnApply}
                </button>
            </div>
            <div class="modal-footer">
                <div class="col-sm-9"></div>
                <div class="col-sm-3"><a href="index.jsp" class="btn btn-default form-control"><span class="fa fa-home"></span>${btnCancel}</a></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#regModal").modal();

        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        });

        $(window).scroll(function () {
            $(".slideanim").each(function () {
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    });
</script>
<script type="text/javascript" src="js/sha.js"></script>
<script>
    function createUser() {
        var form = $('#register');
        var username = $('#newUsername').val();
        var email = $('#newUserEmail').val();
        var pass = $('#newPsw').val();
        var confirm= $('#confirmpsw').val();
        var usernameError = $('#userNameError');
        var emailError = $('#emailError');
        var passError = $('#newPassError');
        var confirmPassError = $('#confirmPassError');

        if (!validateEmail(email)) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgInvalidEmail} </div>");
            return false;
        }
        if (email.length > 255) {
            emailError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongEmail}</div>");
            return false;
        }
        if (email.indexOf("<script") > -1) {
            emailError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        emailError.html("");

        if ((username == "") || (!username.replace(/\s/g, '').length)) {
            usernameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'> ${txtError}: </span> ${msgEmptyName} </div>");
            return false;
        }
        if (username.length > 16) {
            usernameError.html("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgLongName}</div>");
            return false;
        }
        if (username.indexOf("<script") > -1) {
            usernameError("<div class='alert alert-danger' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>" +
                    "<span class='sr-only'>${txtError}:</span> ${msgScriptInput}</div>");
            return false;
        }
        usernameError.html("");

        if (pass.length > 32) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgLongPassword} </div>");
            return false;
        }
        if (pass.length < 6) {
            passError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgShortPassword} </div>");
            return false;
        }
        passError.html("");
        if (pass !== confirm) {
            confirmPassError.append("<div class='alert alert-danger small' role='alert'>" +
                    "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> " +
                    "<span class='sr-only'>Error:</span> ${msgPassesNotMatch} </div>");
            return false;
        }
        confirmPassError.html("");
        var shaObj = new jsSHA("SHA-1", "TEXT");
        shaObj.update(pass);
        var hash = shaObj.getHash("HEX");
        form.append("<input type='hidden' name='password' value='" + hash + "'/>");

        form.submit();
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
    }
</script>

</body>

</html>
