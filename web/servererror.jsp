<%--
  Created by IntelliJ IDEA.
  User: blooming
  Date: 25.2.16
  Time: 13.21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.content" var="contentBundle"/>
    <fmt:message bundle="${contentBundle}" key="content.common.footer.text"
                 var="txtFooter"/>
    <fmt:setBundle basename="main.com.epam.tc.finaltask.localization.navigation" var="navigation"/>
    <fmt:message bundle="${navigation}" key="navigation.language" var="navLanguage"/>
    <fmt:message bundle="${navigation}" key="navigation.home" var="navHome"/>

    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.ru" var="btnRu"/>
    <fmt:message bundle="${contentBundle}" key="content.common.btn.locale.en" var="btnEn"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.serverError" var="txtServerErr"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.serverErrorHeading" var="txtErrHeading"/>
    <fmt:message bundle="${contentBundle}" key="content.common.text.serverErrorBody" var="txtBody"/>

    <link rel="shortcut icon" type="image/x-icon" href="css/img/favicon.ico" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/wfbootstrap.css" type="text/css">
    <title>White Flame</title>
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">
                <span class="glyphicon glyphicon-fire"></span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-left">
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <button class="dropdown-toggle btn-lg" type="button"
                            data-toggle="dropdown">
                        <span class="glyphicon glyphicon-cog"></span> ${navLanguage}<span
                            class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>
                            <form action="/Controller">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <input type="hidden" name="locale" value="en"/>
                                <input type="submit" class="btn btn-default btn-nav" value="${btnEn}"/>
                            </form>
                        </li>
                        <li>
                            <form action="/Controller">
                                <input type="hidden" name="command" value="SET_LOCALE"/>
                                <input type="hidden" name="locale" value="ru"/>
                                <input type="submit" class="btn btn-default btn-nav" value="${btnRu}"/>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row" style="background-color: #1b1b1b">
            <div class="col-lg-12">
                <h1 class="page-header" style="color: white">
                    ${txtServerErr}
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i> <a
                            href="Controller?command=GOTO_MAIN"> ${navHome} </a>
                    </li>
                </ol>
            </div>
        </div>

        <div id="about" class="container-fluid bg-dark">
            <div class="row">
                <div class="col-sm-4">
                    <span class="glyphicon glyphicon-info-sign logo" style="font-size: 200px"></span>
                </div>
                <div class="col-sm-8">
                    <h2>${txtErrHeading}</h2><br>
                    <h4>${txtBody}:</h4>
                    <p><span class="fa fa-envelope"></span> contact@whiteflame.com
                    </p>
                    <br>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>

<footer class="site-footer" style="background-color: #1b1b1b">
    <p class="wf-footer">${txtFooter}</p>
</footer>
</body>

</html>
